package com.june.quartz.jobs;


import com.june.quartz.feign.TicketFeignClient;
import jakarta.annotation.Resource;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.logging.Logger;

public class InitTicketJob extends QuartzJobBean {
    private final Logger log = Logger.getLogger("InitTicketJob");
    @Resource
    TicketFeignClient ticketFeignClient;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        log.info("----------上票任务进行中----------");
        ticketFeignClient.initTickets();
        log.info("----------上票任务完成----------");
    }
}
