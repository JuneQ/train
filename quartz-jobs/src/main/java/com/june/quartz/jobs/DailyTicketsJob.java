package com.june.quartz.jobs;


import com.june.quartz.feign.TicketFeignClient;
import jakarta.annotation.Resource;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.logging.Logger;

public class DailyTicketsJob extends QuartzJobBean {
    private final Logger log = Logger.getLogger("DailyTicketsJob");

    @Resource
    TicketFeignClient ticketFeignClient;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        log.info("--------------补票任务开始-------------");
        // 每3天上5天
        ticketFeignClient.addTicket(5);
        log.info("--------------补票任务完成-------------");
    }
}
