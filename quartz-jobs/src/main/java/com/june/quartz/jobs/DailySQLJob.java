package com.june.quartz.jobs;


import com.june.quartz.feign.TicketFeignClient;
import jakarta.annotation.Resource;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Logger;

public class DailySQLJob extends QuartzJobBean {
    private final Logger log = Logger.getLogger("DailySQLJob");

    @Resource
    JdbcTemplate jdbcTemplate;

    @Resource
    TicketFeignClient ticketFeignClient;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        log.info("--------------重置任务开始-------------");
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new ClassPathResource("daily.sql").getInputStream()))) {
            String line;
            StringBuilder sqlBuilder = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                line = line.trim();
                if (!line.startsWith("--") && !line.isEmpty()) { // 忽略注释和空行
                    sqlBuilder.append(line).append(" ");
                    if (line.endsWith(";")) { // SQL语句通常以分号结束
                        jdbcTemplate.execute(sqlBuilder.toString());
                        sqlBuilder.setLength(0); // 清空缓冲区，准备下一个SQL语句
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        log.info("--------------重置任务结束-------------");
        log.info("--------------继续重置数据-------------");
        ticketFeignClient.initTickets();
    }
}
