package com.june.quartz.feign;

import com.june.common.entity.R;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "ticket", path = "/ticket", contextId = "ticket312455213")
public interface TicketFeignClient {

    @Schema(description = "删除所有车票，然后重新上票")
    @GetMapping("/quartz/refresh-tickets/all")
    R<Void> initTickets();

    @Schema(defaultValue = "上票，参数为轮次")
    @PostMapping("/quartz/tickets/{cycle}")
    R<Void> addTicket(@PathVariable("cycle") Integer cycle);
}
