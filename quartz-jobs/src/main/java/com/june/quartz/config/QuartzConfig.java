package com.june.quartz.config;

import cn.hutool.core.date.DateUtil;
import com.june.quartz.jobs.DailySQLJob;
import com.june.quartz.jobs.DailyTicketsJob;
import com.june.quartz.jobs.InitTicketJob;
import org.apache.commons.lang3.time.DateUtils;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

/**
 * 定义任务描述和具体的执行时间
 */
@Configuration
public class QuartzConfig {
    @Bean
    public JobDetail dailyTicketsJob() {
        //指定任务描述具体的实现类
        return JobBuilder.newJob(DailyTicketsJob.class)
                // 指定任务的名称
                .withIdentity("daily-tickets")
                // 任务描述
                .withDescription("补票任务")
                // 每次任务执行后进行存储
                .storeDurably()
                .build();
    }

    @Bean
    public JobDetail dailySQLJob() {
        //指定任务描述具体的实现类
        return JobBuilder.newJob(DailySQLJob.class)
                // 指定任务的名称
                .withIdentity("daily-sql")
                // 任务描述
                .withDescription("SQL重置任务")
                // 每次任务执行后进行存储
                .storeDurably()
                .build();
    }

    @Bean
    public JobDetail initJob() {
        return JobBuilder.newJob(InitTicketJob.class)
                // 指定任务的名称
                .withIdentity("init-ticket")
                // 任务描述
                .withDescription("初始化上票")
                // 每次任务执行后进行存储
                .storeDurably()
                .build();
    }

    @Bean
    public Trigger addTicketTrigger() {
        //创建触发器
        return TriggerBuilder.newTrigger()
                .forJob(dailyTicketsJob())
                .withIdentity("AddTicketTrigger")
                // 项目启动后1个小时再执行
                .startAt(DateUtils.addHours(new Date(), 1))
                .withSchedule(SimpleScheduleBuilder.repeatHourlyForever(24 * 3))
                .build();
    }

    @Bean
    public Trigger onceTrigger() {
        //创建触发器
        return TriggerBuilder.newTrigger()
                .forJob(initJob()) // 关联到上面定义的JobDetail
                .withIdentity("OnceTrigger")
                .startNow() // 立即开始
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        .withIntervalInSeconds(0) // 表示立即执行
                        .withRepeatCount(0)) // 设置重复次数为0，表示只执行一次
                .build();

    }

    @Bean
    public Trigger everyDayTrigger() {
        //创建触发器
        return TriggerBuilder.newTrigger()
                .forJob(dailySQLJob()) // 关联到上面定义的JobDetail
                .withIdentity("EveryDayTrigger")
                .startAt(DateUtil.parse("2024-01-01 04:00:00", "yyyy-MM-dd HH:mm:ss"))
                .withSchedule(SimpleScheduleBuilder.repeatHourlyForever(24))
                .build();

    }


}
