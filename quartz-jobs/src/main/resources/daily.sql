SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `carriage`;
CREATE TABLE `carriage`
(
    `id`              bigint unsigned NOT NULL COMMENT 'ID',
    `train_id`        bigint unsigned                                               DEFAULT NULL COMMENT '列车ID',
    `carriage_number` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  DEFAULT NULL COMMENT '车厢号',
    `carriage_type`   enum ('0','1','2','3','4','5','6') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '车厢类型：0硬座车厢 1软座车厢  2硬卧车厢 3软卧车厢 4高级软卧车厢 5商务座车厢 6特等座车厢',
    `seat_count`      smallint unsigned                                             DEFAULT NULL COMMENT '座位数',
    `create_time`     datetime                                                      DEFAULT (now()) COMMENT '创建时间',
    `update_time`     datetime                                                      DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`        tinyint(1)                                                    DEFAULT '0' COMMENT '删除标识',
    PRIMARY KEY (`id`),
    KEY `idx_train_id` (`train_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT ='车厢表';



INSERT INTO `carriage` (`id`, `train_id`, `carriage_number`, `carriage_type`, `seat_count`, `create_time`,
                        `update_time`, `del_flag`)
VALUES (1763401646022377473, 1763401608764375041, '1', '0', 33, '2024-03-01 11:11:29', '2024-03-06 14:23:42', 1);
INSERT INTO `carriage` (`id`, `train_id`, `carriage_number`, `carriage_type`, `seat_count`, `create_time`,
                        `update_time`, `del_flag`)
VALUES (1763401697880752129, 1763401608764375041, '2', '1', 23, '2024-03-01 11:11:42', '2024-03-06 14:23:43', 1);
INSERT INTO `carriage` (`id`, `train_id`, `carriage_number`, `carriage_type`, `seat_count`, `create_time`,
                        `update_time`, `del_flag`)
VALUES (1763401820698361858, 1763401778583355393, '1', '1', 235, '2024-03-01 11:12:11', '2024-03-06 14:23:44', 1);
INSERT INTO `carriage` (`id`, `train_id`, `carriage_number`, `carriage_type`, `seat_count`, `create_time`,
                        `update_time`, `del_flag`)
VALUES (1763404109236453377, 1763403641835798529, '1', '1', 323, '2024-03-01 11:21:16', '2024-03-06 14:23:45', 1);
INSERT INTO `carriage` (`id`, `train_id`, `carriage_number`, `carriage_type`, `seat_count`, `create_time`,
                        `update_time`, `del_flag`)
VALUES (1765263539561332738, 1765262891990155265, '1', '0', 30, '2024-03-06 14:29:59', '2024-03-06 14:29:59', 0);
INSERT INTO `carriage` (`id`, `train_id`, `carriage_number`, `carriage_type`, `seat_count`, `create_time`,
                        `update_time`, `del_flag`)
VALUES (1765263566656536577, 1765262996428324866, '1', '0', 30, '2024-03-06 14:30:06', '2024-03-06 14:30:06', 0);
INSERT INTO `carriage` (`id`, `train_id`, `carriage_number`, `carriage_type`, `seat_count`, `create_time`,
                        `update_time`, `del_flag`)
VALUES (1765263598201896961, 1765263062421504002, '1', '0', 30, '2024-03-06 14:30:13', '2024-03-06 14:30:13', 0);
INSERT INTO `carriage` (`id`, `train_id`, `carriage_number`, `carriage_type`, `seat_count`, `create_time`,
                        `update_time`, `del_flag`)
VALUES (1765263622000377858, 1765263153815388161, '1', '0', 30, '2024-03-06 14:30:19', '2024-03-06 14:30:19', 0);
INSERT INTO `carriage` (`id`, `train_id`, `carriage_number`, `carriage_type`, `seat_count`, `create_time`,
                        `update_time`, `del_flag`)
VALUES (1765268806080430082, 1765262996428324866, '2', '0', 40, '2024-03-06 14:50:55', '2024-03-06 14:50:55', 0);
INSERT INTO `carriage` (`id`, `train_id`, `carriage_number`, `carriage_type`, `seat_count`, `create_time`,
                        `update_time`, `del_flag`)
VALUES (1765268884211924993, 1765262996428324866, '3', '1', 40, '2024-03-06 14:51:14', '2024-03-07 19:58:45', 0);
INSERT INTO `carriage` (`id`, `train_id`, `carriage_number`, `carriage_type`, `seat_count`, `create_time`,
                        `update_time`, `del_flag`)
VALUES (1765268921289572353, 1765263062421504002, '2', '2', 40, '2024-03-06 14:51:22', '2024-03-06 14:51:22', 0);
INSERT INTO `carriage` (`id`, `train_id`, `carriage_number`, `carriage_type`, `seat_count`, `create_time`,
                        `update_time`, `del_flag`)
VALUES (1765268946048548866, 1765263153815388161, '2', '1', 40, '2024-03-06 14:51:28', '2024-03-06 14:51:28', 0);
INSERT INTO `carriage` (`id`, `train_id`, `carriage_number`, `carriage_type`, `seat_count`, `create_time`,
                        `update_time`, `del_flag`)
VALUES (1766476180275433473, 1766476032442994690, '1', '2', 50, '2024-03-09 22:48:35', '2024-03-09 22:48:35', 0);



DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`
(
    `id`          bigint unsigned NOT NULL COMMENT 'ID',
    `order_sn`    varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单号',
    `user_id`     bigint unsigned                                              DEFAULT NULL COMMENT '用户ID',
    `source`      enum ('0','1','2') COLLATE utf8mb4_unicode_ci                DEFAULT NULL COMMENT '订单来源',
    `status`      char(1) COLLATE utf8mb4_unicode_ci                           DEFAULT NULL COMMENT '订单状态: 0已下单 1已付款 2已完成 3有退款 4已取消',
    `pay_time`    datetime                                                     DEFAULT NULL COMMENT '支付时间',
    `pay_type`    enum ('0','1','2','3','4') COLLATE utf8mb4_unicode_ci        DEFAULT NULL COMMENT '支付方式: 0支付宝 1微信 2银行卡 3现金 4其他',
    `create_time` datetime                                                     DEFAULT (now()) COMMENT '创建时间',
    `update_time` datetime                                                     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`    tinyint(1)                                                   DEFAULT '0' COMMENT '删除标识',
    `version`     int unsigned                                                 DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `idx_user_id` (`user_id`) USING BTREE,
    KEY `idx_order_sn` (`order_sn`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT ='订单表';



INSERT INTO `order` (`id`, `order_sn`, `user_id`, `source`, `status`, `pay_time`, `pay_type`, `create_time`,
                     `update_time`, `del_flag`, `version`)
VALUES (1766374258532974593, '1766374257962745856', 1757272649425256449, '2', '4', NULL, '4', '2024-03-09 16:03:35',
        '2024-03-09 16:18:36', 0, 1);
INSERT INTO `order` (`id`, `order_sn`, `user_id`, `source`, `status`, `pay_time`, `pay_type`, `create_time`,
                     `update_time`, `del_flag`, `version`)
VALUES (1766415291077746690, '1766415289857298432', 1757272649425256449, '2', '4', NULL, '4', '2024-03-09 18:46:38',
        '2024-03-09 19:01:39', 0, 1);
INSERT INTO `order` (`id`, `order_sn`, `user_id`, `source`, `status`, `pay_time`, `pay_type`, `create_time`,
                     `update_time`, `del_flag`, `version`)
VALUES (1766415516706136065, '1766415516056113152', 1757272649425256449, '2', '4', NULL, '4', '2024-03-09 18:47:32',
        '2024-03-09 19:02:33', 0, 1);
INSERT INTO `order` (`id`, `order_sn`, `user_id`, `source`, `status`, `pay_time`, `pay_type`, `create_time`,
                     `update_time`, `del_flag`, `version`)
VALUES (1766473800246919169, '1766473798611275776', 1757272649425256449, '2', '1', NULL, '4', '2024-03-09 22:39:08',
        '2024-03-09 22:39:57', 0, 0);
INSERT INTO `order` (`id`, `order_sn`, `user_id`, `source`, `status`, `pay_time`, `pay_type`, `create_time`,
                     `update_time`, `del_flag`, `version`)
VALUES (1766482100850036738, '1766482099700977664', 1757272649425256449, '2', '1', NULL, '4', '2024-03-09 23:12:07',
        '2024-03-09 23:12:41', 0, 0);
INSERT INTO `order` (`id`, `order_sn`, `user_id`, `source`, `status`, `pay_time`, `pay_type`, `create_time`,
                     `update_time`, `del_flag`, `version`)
VALUES (1766483376656650241, '1766483376065433600', 1757272649425256449, '2', '1', NULL, '4', '2024-03-09 23:17:11',
        '2024-03-09 23:18:02', 0, 0);
INSERT INTO `order` (`id`, `order_sn`, `user_id`, `source`, `status`, `pay_time`, `pay_type`, `create_time`,
                     `update_time`, `del_flag`, `version`)
VALUES (1766485165858656257, '1766485165342937088', 1757272649425256449, '2', '1', NULL, '4', '2024-03-09 23:24:17',
        '2024-03-09 23:24:43', 0, 0);

DROP TABLE IF EXISTS `order_item`;
CREATE TABLE `order_item`
(
    `id`                     bigint unsigned NOT NULL COMMENT 'ID',
    `order_id`               bigint unsigned                        DEFAULT NULL COMMENT '爹是谁',
    `train_id`               bigint unsigned                        DEFAULT NULL,
    `train_number`           varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `carriage_id`            bigint                                 DEFAULT NULL,
    `carriage_type`          char(1) COLLATE utf8mb4_unicode_ci     DEFAULT NULL,
    `carriage_number`        varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `departure_station_id`   bigint unsigned                        DEFAULT NULL,
    `departure_station_name` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `arrival_station_id`     bigint unsigned                        DEFAULT NULL,
    `arrival_station_name`   varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `seat_number`            smallint unsigned                      DEFAULT NULL,
    `passenger_id`           bigint unsigned                        DEFAULT NULL,
    `passenger_name`         varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `passenger_type`         char(1) COLLATE utf8mb4_unicode_ci     DEFAULT NULL,
    `passenger_phone`        varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `ticket_price`           decimal(10, 2)                         DEFAULT NULL,
    `departure_datetime`     datetime                               DEFAULT NULL,
    `arrival_datetime`       datetime                               DEFAULT NULL,
    `create_time`            datetime                               DEFAULT (now()) COMMENT '创建时间',
    `update_time`            datetime                               DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`               tinyint(1)                             DEFAULT '0' COMMENT '删除标识',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT ='订单项-车票明细表';



INSERT INTO `order_item` (`id`, `order_id`, `train_id`, `train_number`, `carriage_id`, `carriage_type`,
                          `carriage_number`, `departure_station_id`, `departure_station_name`, `arrival_station_id`,
                          `arrival_station_name`, `seat_number`, `passenger_id`, `passenger_name`, `passenger_type`,
                          `passenger_phone`, `ticket_price`, `departure_datetime`, `arrival_datetime`, `create_time`,
                          `update_time`, `del_flag`)
VALUES (1766374258902073345, 1766374258532974593, 1765262891990155265, 'G1', 1765263539561332738, '0', '1',
        1765262350547451906, '西安北站', 1765262432873250817, '北京西站', 29, 1760904615391428610, '张三', '1',
        '17364612480', 181.00, '2024-03-15 06:05:00', '2024-03-15 08:00:00', '2024-03-09 16:03:35',
        '2024-03-09 16:03:35', 0);
INSERT INTO `order_item` (`id`, `order_id`, `train_id`, `train_number`, `carriage_id`, `carriage_type`,
                          `carriage_number`, `departure_station_id`, `departure_station_name`, `arrival_station_id`,
                          `arrival_station_name`, `seat_number`, `passenger_id`, `passenger_name`, `passenger_type`,
                          `passenger_phone`, `ticket_price`, `departure_datetime`, `arrival_datetime`, `create_time`,
                          `update_time`, `del_flag`)
VALUES (1766415291883053057, 1766415291077746690, 1765262891990155265, 'G1', 1765263539561332738, '0', '1',
        1765262350547451906, '西安北站', 1765262432873250817, '北京西站', 27, 1760904615391428610, '张三', '1',
        '17364612480', 253.07, '2024-03-10 06:05:00', '2024-03-10 08:00:00', '2024-03-09 18:46:38',
        '2024-03-09 18:46:38', 0);
INSERT INTO `order_item` (`id`, `order_id`, `train_id`, `train_number`, `carriage_id`, `carriage_type`,
                          `carriage_number`, `departure_station_id`, `departure_station_name`, `arrival_station_id`,
                          `arrival_station_name`, `seat_number`, `passenger_id`, `passenger_name`, `passenger_type`,
                          `passenger_phone`, `ticket_price`, `departure_datetime`, `arrival_datetime`, `create_time`,
                          `update_time`, `del_flag`)
VALUES (1766415291887247362, 1766415291077746690, 1765262891990155265, 'G1', 1765263539561332738, '0', '1',
        1765262350547451906, '西安北站', 1765262432873250817, '北京西站', 7, 1765645384924860418, '李四', '1',
        '17364612480', 253.07, '2024-03-10 06:05:00', '2024-03-10 08:00:00', '2024-03-09 18:46:38',
        '2024-03-09 18:46:38', 0);
INSERT INTO `order_item` (`id`, `order_id`, `train_id`, `train_number`, `carriage_id`, `carriage_type`,
                          `carriage_number`, `departure_station_id`, `departure_station_name`, `arrival_station_id`,
                          `arrival_station_name`, `seat_number`, `passenger_id`, `passenger_name`, `passenger_type`,
                          `passenger_phone`, `ticket_price`, `departure_datetime`, `arrival_datetime`, `create_time`,
                          `update_time`, `del_flag`)
VALUES (1766415517159120897, 1766415516706136065, 1765262891990155265, 'G1', 1765263539561332738, '0', '1',
        1765262350547451906, '西安北站', 1765262432873250817, '北京西站', 3, 1760904615391428610, '张三', '1',
        '17364612480', 253.07, '2024-03-10 06:05:00', '2024-03-10 08:00:00', '2024-03-09 18:47:32',
        '2024-03-09 18:47:32', 0);
INSERT INTO `order_item` (`id`, `order_id`, `train_id`, `train_number`, `carriage_id`, `carriage_type`,
                          `carriage_number`, `departure_station_id`, `departure_station_name`, `arrival_station_id`,
                          `arrival_station_name`, `seat_number`, `passenger_id`, `passenger_name`, `passenger_type`,
                          `passenger_phone`, `ticket_price`, `departure_datetime`, `arrival_datetime`, `create_time`,
                          `update_time`, `del_flag`)
VALUES (1766415517159120898, 1766415516706136065, 1765262891990155265, 'G1', 1765263539561332738, '0', '1',
        1765262350547451906, '西安北站', 1765262432873250817, '北京西站', 2, 1765645384924860418, '李四', '1',
        '17364612480', 253.07, '2024-03-10 06:05:00', '2024-03-10 08:00:00', '2024-03-09 18:47:32',
        '2024-03-09 18:47:32', 0);
INSERT INTO `order_item` (`id`, `order_id`, `train_id`, `train_number`, `carriage_id`, `carriage_type`,
                          `carriage_number`, `departure_station_id`, `departure_station_name`, `arrival_station_id`,
                          `arrival_station_name`, `seat_number`, `passenger_id`, `passenger_name`, `passenger_type`,
                          `passenger_phone`, `ticket_price`, `departure_datetime`, `arrival_datetime`, `create_time`,
                          `update_time`, `del_flag`)
VALUES (1766473801136111617, 1766473800246919169, 1765262996428324866, 'G2', 1765263566656536577, '0', '1',
        1765262396345057282, '西安南站', 1765262464259227649, '北京东站', 13, 1760904615391428610, '张三', '1',
        '17364612480', 209.86, '2024-03-16 11:00:00', '2024-03-16 13:00:05', '2024-03-09 22:39:07',
        '2024-03-09 22:39:08', 0);
INSERT INTO `order_item` (`id`, `order_id`, `train_id`, `train_number`, `carriage_id`, `carriage_type`,
                          `carriage_number`, `departure_station_id`, `departure_station_name`, `arrival_station_id`,
                          `arrival_station_name`, `seat_number`, `passenger_id`, `passenger_name`, `passenger_type`,
                          `passenger_phone`, `ticket_price`, `departure_datetime`, `arrival_datetime`, `create_time`,
                          `update_time`, `del_flag`)
VALUES (1766482101600817154, 1766482100850036738, 1766476032442994690, 'G5', 1766476180275433473, '2', '1',
        1765262528792788993, '新疆站', 1765262495926222849, '深圳北站', 47, 1760904615391428610, '张三', '1',
        '17364612480', 94.22, '2024-03-17 01:00:00', NULL, '2024-03-09 23:12:06', '2024-03-09 23:12:07', 0);
INSERT INTO `order_item` (`id`, `order_id`, `train_id`, `train_number`, `carriage_id`, `carriage_type`,
                          `carriage_number`, `departure_station_id`, `departure_station_name`, `arrival_station_id`,
                          `arrival_station_name`, `seat_number`, `passenger_id`, `passenger_name`, `passenger_type`,
                          `passenger_phone`, `ticket_price`, `departure_datetime`, `arrival_datetime`, `create_time`,
                          `update_time`, `del_flag`)
VALUES (1766483377088663553, 1766483376656650241, 1765262891990155265, 'G1', 1765263539561332738, '0', '1',
        1765262350547451906, '西安北站', 1765262432873250817, '北京西站', 27, 1760904615391428610, '张三', '1',
        '17364612480', 153.65, '2024-03-10 06:05:00', '2024-03-10 08:00:00', '2024-03-09 23:17:11',
        '2024-03-09 23:17:11', 0);
INSERT INTO `order_item` (`id`, `order_id`, `train_id`, `train_number`, `carriage_id`, `carriage_type`,
                          `carriage_number`, `departure_station_id`, `departure_station_name`, `arrival_station_id`,
                          `arrival_station_name`, `seat_number`, `passenger_id`, `passenger_name`, `passenger_type`,
                          `passenger_phone`, `ticket_price`, `departure_datetime`, `arrival_datetime`, `create_time`,
                          `update_time`, `del_flag`)
VALUES (1766485166156451841, 1766485165858656257, 1765262996428324866, 'G2', 1765263566656536577, '0', '1',
        1765262396345057282, '西安南站', 1765262464259227649, '北京东站', 31, 1760904615391428610, '张三', '1',
        '17364612480', 346.29, '2024-03-10 11:00:00', '2024-03-10 13:00:05', '2024-03-09 23:24:17',
        '2024-03-09 23:24:18', 0);



DROP TABLE IF EXISTS `passenger`;
CREATE TABLE `passenger`
(
    `id`            bigint             NOT NULL COMMENT 'id',
    `user_id`       bigint             NOT NULL COMMENT '会员id',
    `name`          varchar(20)        NOT NULL COMMENT '姓名',
    `verify_status` enum ('0','1','2') NOT NULL DEFAULT '0' COMMENT '0已通过 1审核中 2未通过',
    `id_card`       varchar(18)        NOT NULL COMMENT '身份证',
    `id_type`       char(1)            NOT NULL COMMENT '旅客类型 0身份证 1护照 2港澳通行证 3台湾通行证 4军官证',
    `phone`         varchar(15)                 DEFAULT NULL COMMENT '手机号',
    `type`          char(1)                     DEFAULT NULL COMMENT '优惠类型 0儿童 1学生 2成人',
    `create_time`   datetime                    DEFAULT (now()) COMMENT '新增时间',
    `update_time`   datetime                    DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`      tinyint(1)                  DEFAULT '0' COMMENT '删除标识',
    PRIMARY KEY (`id`),
    KEY `member_id_index` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci COMMENT ='乘车人';



INSERT INTO `passenger` (`id`, `user_id`, `name`, `verify_status`, `id_card`, `id_type`, `phone`, `type`, `create_time`,
                         `update_time`, `del_flag`)
VALUES (1760904615391428610, 1757272649425256449, '张三', '0', '411123200107208012', '3', '17364612480', '1',
        '2024-02-23 13:49:11', '2024-02-23 13:49:11', 0);
INSERT INTO `passenger` (`id`, `user_id`, `name`, `verify_status`, `id_card`, `id_type`, `phone`, `type`, `create_time`,
                         `update_time`, `del_flag`)
VALUES (1765645384924860418, 1757272649425256449, '李四', '0', '411123200107208017', '0', '17364612480', '1',
        '2024-03-07 15:47:18', '2024-03-07 15:48:18', 0);



DROP TABLE IF EXISTS `region`;
CREATE TABLE `region`
(
    `id`          bigint unsigned NOT NULL COMMENT 'ID',
    `name`        varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '地区名称',
    `full_name`   varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '地区全名',
    `code`        varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '地区编码',
    `initial`     varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  DEFAULT NULL COMMENT '地区首字母',
    `pinyin`      varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '拼音',
    `hot_flag`    tinyint(1)                                                   DEFAULT '0' COMMENT '热门标识',
    `create_time` datetime                                                     DEFAULT (now()) COMMENT '创建时间',
    `update_time` datetime                                                     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`    tinyint(1)                                                   DEFAULT '0' COMMENT '删除标识',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT ='地区表';



INSERT INTO `region` (`id`, `name`, `full_name`, `code`, `initial`, `pinyin`, `hot_flag`, `create_time`, `update_time`,
                      `del_flag`)
VALUES (1, '西安', '西安', '123', 'S', 'xian', 1, '2024-02-19 21:52:39', '2024-02-19 21:52:39', 0);
INSERT INTO `region` (`id`, `name`, `full_name`, `code`, `initial`, `pinyin`, `hot_flag`, `create_time`, `update_time`,
                      `del_flag`)
VALUES (2, '深圳', '深圳', '234', 'G', 'shenzhen', 1, '2024-02-19 21:53:30', '2024-02-19 21:53:38', 0);
INSERT INTO `region` (`id`, `name`, `full_name`, `code`, `initial`, `pinyin`, `hot_flag`, `create_time`, `update_time`,
                      `del_flag`)
VALUES (3, '济南站', '济南站', '5125', 'j', 'jinanzhan', 0, '2024-02-22 11:51:58', '2024-03-06 14:24:37', 1);
INSERT INTO `region` (`id`, `name`, `full_name`, `code`, `initial`, `pinyin`, `hot_flag`, `create_time`, `update_time`,
                      `del_flag`)
VALUES (4, '北京', '北京', '123', 'b', 'beijing', 1, '2024-02-22 13:18:01', '2024-03-06 14:24:45', 0);
INSERT INTO `region` (`id`, `name`, `full_name`, `code`, `initial`, `pinyin`, `hot_flag`, `create_time`, `update_time`,
                      `del_flag`)
VALUES (5, '郑州', '郑州', '233213', 'z', 'zhengzhou', 1, '2024-03-01 00:12:56', '2024-03-06 14:24:45', 0);
INSERT INTO `region` (`id`, `name`, `full_name`, `code`, `initial`, `pinyin`, `hot_flag`, `create_time`, `update_time`,
                      `del_flag`)
VALUES (6, '济南', '济南', '224', 'j', 'jinan', 1, '2024-03-01 00:13:17', '2024-03-06 14:24:45', 0);
INSERT INTO `region` (`id`, `name`, `full_name`, `code`, `initial`, `pinyin`, `hot_flag`, `create_time`, `update_time`,
                      `del_flag`)
VALUES (7, '新疆', '新疆', '42', 'x', 'xinjiang', 1, '2024-03-01 00:13:24', '2024-03-06 14:24:45', 0);
INSERT INTO `region` (`id`, `name`, `full_name`, `code`, `initial`, `pinyin`, `hot_flag`, `create_time`, `update_time`,
                      `del_flag`)
VALUES (8, '湖北', '湖北', '236', 'h', 'hubei', 1, '2024-03-01 00:13:30', '2024-03-06 14:24:45', 1);



DROP TABLE IF EXISTS `remaining_tickets`;
CREATE TABLE `remaining_tickets`
(
    `id`                           bigint unsigned NOT NULL COMMENT 'ID',
    `carriage_id`                  bigint unsigned NOT NULL COMMENT '车厢号',
    `train_id`                     bigint unsigned   DEFAULT NULL,
    `departure_station_id`         bigint unsigned   DEFAULT NULL COMMENT '仅保存出发站，因为默认下一站为此列车的行驶的紧挨的下一站',
    `first_station_departure_date` date              DEFAULT NULL COMMENT '始发站发车时间，用于记录发车周期相关信息',
    `sequence`                     tinyint unsigned  DEFAULT NULL COMMENT '出发站的站序，从1计数',
    `departure_datetime`           datetime          DEFAULT NULL COMMENT '出发时间',
    `arrival_datetime`             datetime          DEFAULT NULL COMMENT '到达最近下一站的时间',
    `remaining_tickets`            smallint unsigned DEFAULT NULL COMMENT '从此站到下一站的余票数',
    `price`                        decimal(10, 2)    DEFAULT NULL COMMENT '相邻站车票售价',
    `create_time`                  datetime          DEFAULT (now()) COMMENT '创建时间',
    `update_time`                  datetime          DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`                     tinyint(1)        DEFAULT '0' COMMENT '删除标识',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT ='最近下一站-余票信息表';



INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481646963425281, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-10', 1,
        '2024-03-10 06:05:00', '2024-03-10 07:00:00', 29, 115.27, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481647269609473, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-10', 1,
        '2024-03-10 11:00:00', '2024-03-10 12:00:00', 30, 116.52, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481647441575938, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-11', 1,
        '2024-03-11 06:05:00', '2024-03-11 07:00:00', 30, 104.69, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481647642902529, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-10', 1,
        '2024-03-10 11:00:00', '2024-03-10 12:00:00', 40, 184.27, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481647672262658, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-11', 1,
        '2024-03-11 11:00:00', '2024-03-11 12:00:00', 30, 89.26, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481647961669634, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-12', 1,
        '2024-03-12 06:05:00', '2024-03-12 07:00:00', 30, 10.54, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481647978446849, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-10', 1,
        '2024-03-10 11:00:00', '2024-03-10 12:00:00', 40, 69.02, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481648100081665, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-12', 1,
        '2024-03-12 11:00:00', '2024-03-12 12:00:00', 30, 155.23, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481648112664578, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-11', 1,
        '2024-03-11 11:00:00', '2024-03-11 12:00:00', 40, 128.64, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481648368517122, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-11', 1,
        '2024-03-11 11:00:00', '2024-03-11 12:00:00', 40, 136.40, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481648439820290, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-13', 1,
        '2024-03-13 06:05:00', '2024-03-13 07:00:00', 30, 104.76, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481648444014594, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-10', 1,
        '2024-03-10 15:00:00', '2024-03-10 17:00:00', 30, 98.03, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481648506929153, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-13', 1,
        '2024-03-13 11:00:00', '2024-03-13 12:00:00', 30, 44.58, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481648544677889, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-12', 1,
        '2024-03-12 11:00:00', '2024-03-12 12:00:00', 40, 157.39, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481648758587394, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-12', 1,
        '2024-03-12 11:00:00', '2024-03-12 12:00:00', 40, 141.71, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481648855056385, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-11', 1,
        '2024-03-11 15:00:00', '2024-03-11 17:00:00', 30, 72.78, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481648913776641, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-10', 2,
        '2024-03-10 07:00:00', '2024-03-10 08:00:00', 29, 38.38, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481648913776642, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-10', 2,
        '2024-03-10 12:00:00', '2024-03-10 13:00:05', 30, 22.04, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481648964108289, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-13', 1,
        '2024-03-13 11:00:00', '2024-03-13 12:00:00', 40, 187.06, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481649136074754, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-13', 1,
        '2024-03-13 11:00:00', '2024-03-13 12:00:00', 40, 66.71, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481649261903873, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-12', 1,
        '2024-03-12 15:00:00', '2024-03-12 17:00:00', 30, 101.07, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481649324818433, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-11', 2,
        '2024-03-11 12:00:00', '2024-03-11 13:00:05', 30, 29.23, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481649383538690, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-11', 2,
        '2024-03-11 07:00:00', '2024-03-11 08:00:00', 30, 50.00, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481649383538691, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-10', 2,
        '2024-03-10 12:00:00', '2024-03-10 13:00:05', 40, 81.83, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481649509367810, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-10', 2,
        '2024-03-10 12:00:00', '2024-03-10 13:00:05', 40, 151.85, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481649677139970, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-13', 1,
        '2024-03-13 15:00:00', '2024-03-13 17:00:00', 30, 87.61, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481649740054529, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-12', 2,
        '2024-03-12 12:00:00', '2024-03-12 13:00:05', 30, 168.46, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481649811357698, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-11', 2,
        '2024-03-11 12:00:00', '2024-03-11 13:00:05', 40, 131.30, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481649849106434, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-12', 2,
        '2024-03-12 07:00:00', '2024-03-12 08:00:00', 30, 199.65, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481649903632385, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-11', 2,
        '2024-03-11 12:00:00', '2024-03-11 13:00:05', 40, 104.36, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481650142707713, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-13', 2,
        '2024-03-13 12:00:00', '2024-03-13 13:00:05', 30, 56.46, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481650239176706, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-12', 2,
        '2024-03-12 12:00:00', '2024-03-12 13:00:05', 40, 125.04, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481650281119745, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-12', 2,
        '2024-03-12 12:00:00', '2024-03-12 13:00:05', 40, 55.35, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481650327257090, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-13', 2,
        '2024-03-13 07:00:00', '2024-03-13 08:00:00', 30, 161.89, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481650553749505, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-10', 1,
        '2024-03-10 15:00:00', '2024-03-10 17:00:00', 40, 172.60, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481650662801409, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-13', 2,
        '2024-03-13 12:00:00', '2024-03-13 13:00:05', 40, 132.15, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481650671190018, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-13', 2,
        '2024-03-13 12:00:00', '2024-03-13 13:00:05', 40, 61.03, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481650968985601, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-11', 1,
        '2024-03-11 15:00:00', '2024-03-11 17:00:00', 40, 166.34, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481651057065986, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-10', 1,
        '2024-03-10 17:00:00', '2024-03-10 18:00:00', 30, 71.47, '2024-03-09 23:10:19', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481651375833089, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-12', 1,
        '2024-03-12 15:00:00', '2024-03-12 17:00:00', 40, 85.28, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481651447136258, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-11', 1,
        '2024-03-11 17:00:00', '2024-03-11 18:00:00', 30, 71.44, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481651791069185, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-13', 1,
        '2024-03-13 15:00:00', '2024-03-13 17:00:00', 40, 58.03, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481651837206530, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-12', 1,
        '2024-03-12 17:00:00', '2024-03-12 18:00:00', 30, 68.40, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481652218888194, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-10', 1,
        '2024-03-10 17:00:00', '2024-03-10 18:00:00', 40, 184.95, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481652231471105, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-13', 1,
        '2024-03-13 17:00:00', '2024-03-13 18:00:00', 30, 92.04, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481652621541378, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-10', 2,
        '2024-03-10 18:00:00', '2024-03-10 19:00:00', 30, 159.89, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481652629929986, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-11', 1,
        '2024-03-11 17:00:00', '2024-03-11 18:00:00', 40, 38.17, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481653011611649, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-11', 2,
        '2024-03-11 18:00:00', '2024-03-11 19:00:00', 30, 131.34, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481653045166081, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-12', 1,
        '2024-03-12 17:00:00', '2024-03-12 18:00:00', 40, 62.46, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481653410070530, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-12', 2,
        '2024-03-12 18:00:00', '2024-03-12 19:00:00', 30, 110.92, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481653460402178, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-13', 1,
        '2024-03-13 17:00:00', '2024-03-13 18:00:00', 40, 194.51, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481653800140801, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-13', 2,
        '2024-03-13 18:00:00', '2024-03-13 19:00:00', 30, 161.87, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481653871443969, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-10', 2,
        '2024-03-10 18:00:00', '2024-03-10 19:00:00', 40, 107.88, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481654282485761, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-11', 2,
        '2024-03-11 18:00:00', '2024-03-11 19:00:00', 40, 138.60, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481654693527554, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-12', 2,
        '2024-03-12 18:00:00', '2024-03-12 19:00:00', 40, 168.73, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481655104569345, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-13', 2,
        '2024-03-13 18:00:00', '2024-03-13 19:00:00', 40, 29.81, '2024-03-09 23:10:20', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481655519805442, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-11', 1,
        '2024-03-11 01:00:00', '2024-03-12 04:00:00', 50, 82.19, '2024-03-09 23:10:21', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481655935041538, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-13', 1,
        '2024-03-13 01:00:00', '2024-03-14 04:00:00', 50, 141.30, '2024-03-09 23:10:21', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481656346083330, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-15', 1,
        '2024-03-15 01:00:00', '2024-03-16 04:00:00', 50, 42.07, '2024-03-09 23:10:21', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766481656761319425, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-17', 1,
        '2024-03-17 01:00:00', '2024-03-18 04:00:00', 49, 94.22, '2024-03-09 23:10:21', '2024-03-09 23:22:12', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484642002665473, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-10', 1,
        '2024-03-10 06:05:00', '2024-03-10 07:00:00', 30, 80.21, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484642141077505, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-10', 1,
        '2024-03-10 06:05:00', '2024-03-10 07:00:00', 30, 195.85, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484642405318658, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-10', 1,
        '2024-03-10 11:00:00', '2024-03-10 12:00:00', 29, 87.11, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484642485010434, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-11', 1,
        '2024-03-11 06:05:00', '2024-03-11 07:00:00', 30, 178.44, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484642547924993, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-11', 1,
        '2024-03-11 06:05:00', '2024-03-11 07:00:00', 30, 14.22, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484642824749058, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-11', 1,
        '2024-03-11 11:00:00', '2024-03-11 12:00:00', 30, 168.49, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484642954772482, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-12', 1,
        '2024-03-12 06:05:00', '2024-03-12 07:00:00', 30, 139.13, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484642954772483, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-12', 1,
        '2024-03-12 06:05:00', '2024-03-12 07:00:00', 30, 178.95, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484643256762370, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-12', 1,
        '2024-03-12 11:00:00', '2024-03-12 12:00:00', 30, 67.69, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484643357425666, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-13', 1,
        '2024-03-13 06:05:00', '2024-03-13 07:00:00', 30, 157.56, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484643428728833, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-13', 1,
        '2024-03-13 06:05:00', '2024-03-13 07:00:00', 30, 73.04, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484643680387073, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-13', 1,
        '2024-03-13 11:00:00', '2024-03-13 12:00:00', 30, 127.09, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484643772661761, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-14', 1,
        '2024-03-14 06:05:00', '2024-03-14 07:00:00', 30, 183.77, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484643906879490, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-14', 1,
        '2024-03-14 06:05:00', '2024-03-14 07:00:00', 30, 154.60, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484644112400386, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-14', 1,
        '2024-03-14 11:00:00', '2024-03-14 12:00:00', 30, 114.05, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484644179509249, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-15', 1,
        '2024-03-15 06:05:00', '2024-03-15 07:00:00', 30, 85.25, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484644376641538, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-15', 1,
        '2024-03-15 06:05:00', '2024-03-15 07:00:00', 30, 95.82, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484644540219394, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-15', 1,
        '2024-03-15 11:00:00', '2024-03-15 12:00:00', 30, 152.20, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484644586356738, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-16', 1,
        '2024-03-16 06:05:00', '2024-03-16 07:00:00', 30, 15.12, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484644947066881, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-16', 1,
        '2024-03-16 06:05:00', '2024-03-16 07:00:00', 30, 110.22, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484645064507394, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-17', 1,
        '2024-03-17 06:05:00', '2024-03-17 07:00:00', 30, 41.91, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484645068701697, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-16', 1,
        '2024-03-16 11:00:00', '2024-03-16 12:00:00', 30, 159.53, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484645475549185, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-18', 1,
        '2024-03-18 06:05:00', '2024-03-18 07:00:00', 30, 182.83, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484645492326401, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-17', 1,
        '2024-03-17 11:00:00', '2024-03-17 12:00:00', 30, 25.50, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484645584601090, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-18', 1,
        '2024-03-18 06:05:00', '2024-03-18 07:00:00', 30, 45.59, '2024-03-09 23:22:13', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484645894979586, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-19', 1,
        '2024-03-19 06:05:00', '2024-03-19 07:00:00', 30, 52.40, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484645920145410, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-18', 1,
        '2024-03-18 11:00:00', '2024-03-18 12:00:00', 30, 180.93, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484646054363138, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-19', 1,
        '2024-03-19 06:05:00', '2024-03-19 07:00:00', 30, 54.72, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484646310215682, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-20', 1,
        '2024-03-20 06:05:00', '2024-03-20 07:00:00', 30, 126.44, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484646343770114, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-19', 1,
        '2024-03-19 11:00:00', '2024-03-19 12:00:00', 30, 61.09, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484646524125186, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-20', 1,
        '2024-03-20 06:05:00', '2024-03-20 07:00:00', 30, 194.95, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484646721257473, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-21', 1,
        '2024-03-21 06:05:00', '2024-03-21 07:00:00', 30, 58.30, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484646771589122, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-20', 1,
        '2024-03-20 11:00:00', '2024-03-20 12:00:00', 30, 176.86, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484647006470145, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-21', 1,
        '2024-03-21 06:05:00', '2024-03-21 07:00:00', 30, 97.87, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484647136493569, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-22', 1,
        '2024-03-22 06:05:00', '2024-03-22 07:00:00', 30, 149.66, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484647207796737, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-21', 1,
        '2024-03-21 11:00:00', '2024-03-21 12:00:00', 30, 27.40, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484647547535362, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-23', 1,
        '2024-03-23 06:05:00', '2024-03-23 07:00:00', 30, 191.89, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484647639810050, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-23', 1,
        '2024-03-23 06:05:00', '2024-03-23 07:00:00', 30, 176.11, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484647639810051, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-22', 1,
        '2024-03-22 11:00:00', '2024-03-22 12:00:00', 30, 60.56, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484647954382849, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-24', 1,
        '2024-03-24 06:05:00', '2024-03-24 07:00:00', 30, 180.14, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484648063434754, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-23', 1,
        '2024-03-23 11:00:00', '2024-03-23 12:00:00', 30, 105.37, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484648101183489, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-24', 1,
        '2024-03-24 06:05:00', '2024-03-24 07:00:00', 30, 20.18, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484648365424642, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-25', 1,
        '2024-03-25 06:05:00', '2024-03-25 07:00:00', 30, 133.46, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484648491253762, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-24', 1,
        '2024-03-24 11:00:00', '2024-03-24 12:00:00', 30, 177.61, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484648570945538, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-25', 1,
        '2024-03-25 06:05:00', '2024-03-25 07:00:00', 30, 122.88, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484648776466433, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-10', 2,
        '2024-03-10 07:00:00', '2024-03-10 08:00:00', 30, 33.05, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484648919072769, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-25', 1,
        '2024-03-25 11:00:00', '2024-03-25 12:00:00', 30, 133.19, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484649086844929, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-10', 2,
        '2024-03-10 07:00:00', '2024-03-10 08:00:00', 30, 155.39, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484649225256961, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-11', 2,
        '2024-03-11 07:00:00', '2024-03-11 08:00:00', 30, 19.89, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484649367863298, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-10', 2,
        '2024-03-10 12:00:00', '2024-03-10 13:00:05', 29, 120.62, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484649640493058, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-12', 2,
        '2024-03-12 07:00:00', '2024-03-12 08:00:00', 30, 38.79, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484649724379138, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-12', 2,
        '2024-03-12 07:00:00', '2024-03-12 08:00:00', 30, 176.49, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484649795682306, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-11', 2,
        '2024-03-11 12:00:00', '2024-03-11 13:00:05', 30, 95.76, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484650047340545, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-13', 2,
        '2024-03-13 07:00:00', '2024-03-13 08:00:00', 30, 21.88, '2024-03-09 23:22:14', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484650194141185, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-13', 2,
        '2024-03-13 07:00:00', '2024-03-13 08:00:00', 30, 29.11, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484650223501314, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-12', 2,
        '2024-03-12 12:00:00', '2024-03-12 13:00:05', 30, 180.16, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484650462576641, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-14', 2,
        '2024-03-14 07:00:00', '2024-03-14 08:00:00', 30, 185.77, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484650655514625, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-13', 2,
        '2024-03-13 12:00:00', '2024-03-13 13:00:05', 30, 12.78, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484650659708929, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-14', 2,
        '2024-03-14 07:00:00', '2024-03-14 08:00:00', 30, 34.39, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484650873618433, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-15', 2,
        '2024-03-15 07:00:00', '2024-03-15 08:00:00', 30, 168.12, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484651083333634, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-14', 2,
        '2024-03-14 12:00:00', '2024-03-14 13:00:05', 30, 164.09, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484651142053890, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-15', 2,
        '2024-03-15 07:00:00', '2024-03-15 08:00:00', 30, 109.51, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484651288854530, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-16', 2,
        '2024-03-16 07:00:00', '2024-03-16 08:00:00', 30, 31.41, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484651519541250, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-15', 2,
        '2024-03-15 12:00:00', '2024-03-15 13:00:05', 30, 93.58, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484651695702017, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-17', 2,
        '2024-03-17 07:00:00', '2024-03-17 08:00:00', 30, 56.47, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484651829919745, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-17', 2,
        '2024-03-17 07:00:00', '2024-03-17 08:00:00', 30, 21.99, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484651951554562, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-16', 2,
        '2024-03-16 12:00:00', '2024-03-16 13:00:05', 30, 189.66, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484652106743810, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-18', 2,
        '2024-03-18 07:00:00', '2024-03-18 08:00:00', 30, 124.22, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484652312264706, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-18', 2,
        '2024-03-18 07:00:00', '2024-03-18 08:00:00', 30, 55.80, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484652396150786, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-17', 2,
        '2024-03-17 12:00:00', '2024-03-17 13:00:05', 30, 146.05, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484652534562818, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-19', 2,
        '2024-03-19 07:00:00', '2024-03-19 08:00:00', 30, 116.35, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484652790415362, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-19', 2,
        '2024-03-19 07:00:00', '2024-03-19 08:00:00', 30, 172.02, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484652815581185, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-18', 2,
        '2024-03-18 12:00:00', '2024-03-18 13:00:05', 30, 191.37, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484652945604609, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-20', 2,
        '2024-03-20 07:00:00', '2024-03-20 08:00:00', 30, 189.99, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484653230817281, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-19', 2,
        '2024-03-19 12:00:00', '2024-03-19 13:00:05', 30, 161.22, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484653352452097, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-21', 2,
        '2024-03-21 07:00:00', '2024-03-21 08:00:00', 30, 50.21, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484653423755265, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-21', 2,
        '2024-03-21 07:00:00', '2024-03-21 08:00:00', 30, 31.72, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484653658636290, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-20', 2,
        '2024-03-20 12:00:00', '2024-03-20 13:00:05', 30, 175.83, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484653763493889, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-22', 2,
        '2024-03-22 07:00:00', '2024-03-22 08:00:00', 30, 129.46, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484653897711618, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-22', 2,
        '2024-03-22 07:00:00', '2024-03-22 08:00:00', 30, 177.75, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484654082260994, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-21', 2,
        '2024-03-21 12:00:00', '2024-03-21 13:00:05', 30, 38.25, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484654161952769, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-23', 2,
        '2024-03-23 07:00:00', '2024-03-23 08:00:00', 30, 31.59, '2024-03-09 23:22:15', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484654371667970, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-23', 2,
        '2024-03-23 07:00:00', '2024-03-23 08:00:00', 30, 39.29, '2024-03-09 23:22:16', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484654514274306, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-22', 2,
        '2024-03-22 12:00:00', '2024-03-22 13:00:05', 30, 157.73, '2024-03-09 23:22:16', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484654572994561, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-24', 2,
        '2024-03-24 07:00:00', '2024-03-24 08:00:00', 30, 74.52, '2024-03-09 23:22:16', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484654845624322, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-24', 2,
        '2024-03-24 07:00:00', '2024-03-24 08:00:00', 30, 163.95, '2024-03-09 23:22:16', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484654946287617, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-23', 2,
        '2024-03-23 12:00:00', '2024-03-23 13:00:05', 30, 114.28, '2024-03-09 23:22:16', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484654979842049, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-25', 2,
        '2024-03-25 07:00:00', '2024-03-25 08:00:00', 30, 195.11, '2024-03-09 23:22:16', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484655369912322, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-24', 2,
        '2024-03-24 12:00:00', '2024-03-24 13:00:05', 30, 151.53, '2024-03-09 23:22:16', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484655806119938, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-25', 2,
        '2024-03-25 12:00:00', '2024-03-25 13:00:05', 30, 101.57, '2024-03-09 23:22:16', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484656225550337, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-10', 1,
        '2024-03-10 11:00:00', '2024-03-10 12:00:00', 40, 66.94, '2024-03-09 23:22:16', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484656653369346, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-11', 1,
        '2024-03-11 11:00:00', '2024-03-11 12:00:00', 40, 73.06, '2024-03-09 23:22:16', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484657085382657, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-12', 1,
        '2024-03-12 11:00:00', '2024-03-12 12:00:00', 40, 171.87, '2024-03-09 23:22:16', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484657513201666, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-13', 1,
        '2024-03-13 11:00:00', '2024-03-13 12:00:00', 40, 151.25, '2024-03-09 23:22:16', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484657936826370, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-14', 1,
        '2024-03-14 11:00:00', '2024-03-14 12:00:00', 40, 16.73, '2024-03-09 23:22:16', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484658364645377, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-15', 1,
        '2024-03-15 11:00:00', '2024-03-15 12:00:00', 40, 142.24, '2024-03-09 23:22:16', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484658796658689, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-16', 1,
        '2024-03-16 11:00:00', '2024-03-16 12:00:00', 40, 47.67, '2024-03-09 23:22:17', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484659228672002, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-17', 1,
        '2024-03-17 11:00:00', '2024-03-17 12:00:00', 40, 194.04, '2024-03-09 23:22:17', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484659652296705, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-18', 1,
        '2024-03-18 11:00:00', '2024-03-18 12:00:00', 40, 186.50, '2024-03-09 23:22:17', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484659966869505, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-10', 1,
        '2024-03-10 11:00:00', '2024-03-10 12:00:00', 40, 30.29, '2024-03-09 23:22:17', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484660088504322, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-19', 1,
        '2024-03-19 11:00:00', '2024-03-19 12:00:00', 40, 25.73, '2024-03-09 23:22:17', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484660449214466, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-11', 1,
        '2024-03-11 11:00:00', '2024-03-11 12:00:00', 40, 121.26, '2024-03-09 23:22:17', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484660520517634, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-20', 1,
        '2024-03-20 11:00:00', '2024-03-20 12:00:00', 40, 100.31, '2024-03-09 23:22:17', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484660931559426, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-12', 1,
        '2024-03-12 11:00:00', '2024-03-12 12:00:00', 40, 116.42, '2024-03-09 23:22:17', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484660952530945, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-21', 1,
        '2024-03-21 11:00:00', '2024-03-21 12:00:00', 40, 96.48, '2024-03-09 23:22:17', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484661380349954, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-22', 1,
        '2024-03-22 11:00:00', '2024-03-22 12:00:00', 40, 168.32, '2024-03-09 23:22:17', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484661409710081, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-13', 1,
        '2024-03-13 11:00:00', '2024-03-13 12:00:00', 40, 67.64, '2024-03-09 23:22:17', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484661816557569, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-23', 1,
        '2024-03-23 11:00:00', '2024-03-23 12:00:00', 40, 127.14, '2024-03-09 23:22:17', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484661883666434, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-14', 1,
        '2024-03-14 11:00:00', '2024-03-14 12:00:00', 40, 52.67, '2024-03-09 23:22:17', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484662244376577, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-24', 1,
        '2024-03-24 11:00:00', '2024-03-24 12:00:00', 40, 145.13, '2024-03-09 23:22:17', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484662366011393, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-15', 1,
        '2024-03-15 11:00:00', '2024-03-15 12:00:00', 40, 103.45, '2024-03-09 23:22:17', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484662672195585, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-25', 1,
        '2024-03-25 11:00:00', '2024-03-25 12:00:00', 40, 197.10, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484662844162049, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-16', 1,
        '2024-03-16 11:00:00', '2024-03-16 12:00:00', 40, 89.95, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484663100014593, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-10', 2,
        '2024-03-10 12:00:00', '2024-03-10 13:00:05', 40, 146.50, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484663322312706, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-17', 1,
        '2024-03-17 11:00:00', '2024-03-17 12:00:00', 40, 121.74, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484663536222210, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-11', 2,
        '2024-03-11 12:00:00', '2024-03-11 13:00:05', 40, 53.63, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484663804657665, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-18', 1,
        '2024-03-18 11:00:00', '2024-03-18 12:00:00', 40, 189.24, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484663964041218, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-12', 2,
        '2024-03-12 12:00:00', '2024-03-12 13:00:05', 40, 179.73, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484664282808322, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-19', 1,
        '2024-03-19 11:00:00', '2024-03-19 12:00:00', 40, 199.23, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484664391860225, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-13', 2,
        '2024-03-13 12:00:00', '2024-03-13 13:00:05', 40, 199.18, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484664760958977, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-20', 1,
        '2024-03-20 11:00:00', '2024-03-20 12:00:00', 40, 153.39, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484664836456449, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-14', 2,
        '2024-03-14 12:00:00', '2024-03-14 13:00:05', 40, 31.69, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484665247498241, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-21', 1,
        '2024-03-21 11:00:00', '2024-03-21 12:00:00', 40, 128.99, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484665264275458, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-15', 2,
        '2024-03-15 12:00:00', '2024-03-15 13:00:05', 40, 33.51, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484665687900161, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-16', 2,
        '2024-03-16 12:00:00', '2024-03-16 13:00:05', 40, 92.42, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484665721454594, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-22', 1,
        '2024-03-22 11:00:00', '2024-03-22 12:00:00', 40, 148.10, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484666119913474, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-17', 2,
        '2024-03-17 12:00:00', '2024-03-17 13:00:05', 40, 155.51, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484666195410946, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-23', 1,
        '2024-03-23 11:00:00', '2024-03-23 12:00:00', 40, 165.60, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484666551926786, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-18', 2,
        '2024-03-18 12:00:00', '2024-03-18 13:00:05', 40, 77.94, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484666669367297, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-24', 1,
        '2024-03-24 11:00:00', '2024-03-24 12:00:00', 40, 172.99, '2024-03-09 23:22:18', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484666975551489, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-19', 2,
        '2024-03-19 12:00:00', '2024-03-19 13:00:05', 40, 98.24, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484667143323649, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-25', 1,
        '2024-03-25 11:00:00', '2024-03-25 12:00:00', 40, 19.54, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484667411759106, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-20', 2,
        '2024-03-20 12:00:00', '2024-03-20 13:00:05', 40, 197.81, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484667621474305, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-10', 2,
        '2024-03-10 12:00:00', '2024-03-10 13:00:05', 40, 80.71, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484667843772417, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-21', 2,
        '2024-03-21 12:00:00', '2024-03-21 13:00:05', 40, 92.36, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484668099624961, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-11', 2,
        '2024-03-11 12:00:00', '2024-03-11 13:00:05', 40, 62.82, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484668267397122, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-22', 2,
        '2024-03-22 12:00:00', '2024-03-22 13:00:05', 40, 50.08, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484668581969922, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-12', 2,
        '2024-03-12 12:00:00', '2024-03-12 13:00:05', 40, 144.65, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484668695216129, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-23', 2,
        '2024-03-23 12:00:00', '2024-03-23 13:00:05', 40, 52.11, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484669060120578, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-13', 2,
        '2024-03-13 12:00:00', '2024-03-13 13:00:05', 40, 129.36, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484669123035138, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-24', 2,
        '2024-03-24 12:00:00', '2024-03-24 13:00:05', 40, 165.26, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484669538271234, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-14', 2,
        '2024-03-14 12:00:00', '2024-03-14 13:00:05', 40, 103.05, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484669550854145, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-25', 2,
        '2024-03-25 12:00:00', '2024-03-25 13:00:05', 40, 51.92, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484670016421889, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-15', 2,
        '2024-03-15 12:00:00', '2024-03-15 13:00:05', 40, 190.03, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484670481989634, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-16', 2,
        '2024-03-16 12:00:00', '2024-03-16 13:00:05', 40, 53.97, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484670960140289, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-17', 2,
        '2024-03-17 12:00:00', '2024-03-17 13:00:05', 40, 95.14, '2024-03-09 23:22:19', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484671438290946, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-18', 2,
        '2024-03-18 12:00:00', '2024-03-18 13:00:05', 40, 13.35, '2024-03-09 23:22:20', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484672075825154, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-19', 2,
        '2024-03-19 12:00:00', '2024-03-19 13:00:05', 40, 43.06, '2024-03-09 23:22:20', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484672553975810, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-20', 2,
        '2024-03-20 12:00:00', '2024-03-20 13:00:05', 40, 23.69, '2024-03-09 23:22:20', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484673032126466, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-21', 2,
        '2024-03-21 12:00:00', '2024-03-21 13:00:05', 40, 36.42, '2024-03-09 23:22:20', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484673510277122, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-22', 2,
        '2024-03-22 12:00:00', '2024-03-22 13:00:05', 40, 123.05, '2024-03-09 23:22:20', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484673975844865, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-23', 2,
        '2024-03-23 12:00:00', '2024-03-23 13:00:05', 40, 187.00, '2024-03-09 23:22:20', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484674449801217, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-24', 2,
        '2024-03-24 12:00:00', '2024-03-24 13:00:05', 40, 122.46, '2024-03-09 23:22:20', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484674927951873, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-25', 2,
        '2024-03-25 12:00:00', '2024-03-25 13:00:05', 40, 99.39, '2024-03-09 23:22:20', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484675410296833, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-10', 1,
        '2024-03-10 15:00:00', '2024-03-10 17:00:00', 30, 178.98, '2024-03-09 23:22:21', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484675892641794, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-11', 1,
        '2024-03-11 15:00:00', '2024-03-11 17:00:00', 30, 47.14, '2024-03-09 23:22:21', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484676374986754, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-12', 1,
        '2024-03-12 15:00:00', '2024-03-12 17:00:00', 30, 64.37, '2024-03-09 23:22:21', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484676857331713, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-13', 1,
        '2024-03-13 15:00:00', '2024-03-13 17:00:00', 30, 141.54, '2024-03-09 23:22:21', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484677331288065, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-14', 1,
        '2024-03-14 15:00:00', '2024-03-14 17:00:00', 30, 138.34, '2024-03-09 23:22:21', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484677817827330, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-15', 1,
        '2024-03-15 15:00:00', '2024-03-15 17:00:00', 30, 68.79, '2024-03-09 23:22:21', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484678308560898, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-16', 1,
        '2024-03-16 15:00:00', '2024-03-16 17:00:00', 30, 112.35, '2024-03-09 23:22:21', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484678786711553, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-17', 1,
        '2024-03-17 15:00:00', '2024-03-17 17:00:00', 30, 34.44, '2024-03-09 23:22:21', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484679092895746, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-10', 1,
        '2024-03-10 15:00:00', '2024-03-10 17:00:00', 40, 55.03, '2024-03-09 23:22:21', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484679260667905, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-18', 1,
        '2024-03-18 15:00:00', '2024-03-18 17:00:00', 30, 81.91, '2024-03-09 23:22:21', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484679545880577, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-11', 1,
        '2024-03-11 15:00:00', '2024-03-11 17:00:00', 40, 21.84, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484679734624258, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-19', 1,
        '2024-03-19 15:00:00', '2024-03-19 17:00:00', 30, 104.10, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484679956922370, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-12', 1,
        '2024-03-12 15:00:00', '2024-03-12 17:00:00', 40, 161.33, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484680216969218, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-20', 1,
        '2024-03-20 15:00:00', '2024-03-20 17:00:00', 30, 192.42, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484680372158465, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-13', 1,
        '2024-03-13 15:00:00', '2024-03-13 17:00:00', 40, 155.28, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484680690925569, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-21', 1,
        '2024-03-21 15:00:00', '2024-03-21 17:00:00', 30, 60.97, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484680779005953, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-14', 1,
        '2024-03-14 15:00:00', '2024-03-14 17:00:00', 40, 193.14, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484681164881921, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-22', 1,
        '2024-03-22 15:00:00', '2024-03-22 17:00:00', 30, 114.14, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484681194242049, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-15', 1,
        '2024-03-15 15:00:00', '2024-03-15 17:00:00', 40, 30.86, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484681622061058, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-16', 1,
        '2024-03-16 15:00:00', '2024-03-16 17:00:00', 40, 77.21, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484681638838274, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-23', 1,
        '2024-03-23 15:00:00', '2024-03-23 17:00:00', 30, 190.93, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484682033102850, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-17', 1,
        '2024-03-17 15:00:00', '2024-03-17 17:00:00', 40, 25.97, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484682112794625, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-24', 1,
        '2024-03-24 15:00:00', '2024-03-24 17:00:00', 30, 32.00, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484682452533250, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-18', 1,
        '2024-03-18 15:00:00', '2024-03-18 17:00:00', 40, 196.99, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484682590945281, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-25', 1,
        '2024-03-25 15:00:00', '2024-03-25 17:00:00', 30, 40.06, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484682871963650, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-19', 1,
        '2024-03-19 15:00:00', '2024-03-19 17:00:00', 40, 104.43, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484683111038978, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-10', 1,
        '2024-03-10 17:00:00', '2024-03-10 18:00:00', 30, 132.55, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484683291394050, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-20', 1,
        '2024-03-20 15:00:00', '2024-03-20 17:00:00', 40, 25.66, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484683593383938, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-11', 1,
        '2024-03-11 17:00:00', '2024-03-11 18:00:00', 30, 44.54, '2024-03-09 23:22:22', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484683710824449, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-21', 1,
        '2024-03-21 15:00:00', '2024-03-21 17:00:00', 40, 99.21, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484684079923202, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-12', 1,
        '2024-03-12 17:00:00', '2024-03-12 18:00:00', 30, 95.48, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484684130254849, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-22', 1,
        '2024-03-22 15:00:00', '2024-03-22 17:00:00', 40, 176.96, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484684553879554, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-13', 1,
        '2024-03-13 17:00:00', '2024-03-13 18:00:00', 30, 74.26, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484684558073857, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-23', 1,
        '2024-03-23 15:00:00', '2024-03-23 17:00:00', 40, 56.75, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484684973309953, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-24', 1,
        '2024-03-24 15:00:00', '2024-03-24 17:00:00', 40, 27.37, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484685036224514, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-14', 1,
        '2024-03-14 17:00:00', '2024-03-14 18:00:00', 30, 183.55, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484685388546049, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-25', 1,
        '2024-03-25 15:00:00', '2024-03-25 17:00:00', 40, 140.10, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484685514375170, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-15', 1,
        '2024-03-15 17:00:00', '2024-03-15 18:00:00', 30, 90.23, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484685803782145, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-10', 1,
        '2024-03-10 17:00:00', '2024-03-10 18:00:00', 40, 29.05, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484685984137217, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-16', 1,
        '2024-03-16 17:00:00', '2024-03-16 18:00:00', 30, 112.69, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484686223212545, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-11', 1,
        '2024-03-11 17:00:00', '2024-03-11 18:00:00', 40, 197.76, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484686470676481, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-17', 1,
        '2024-03-17 17:00:00', '2024-03-17 18:00:00', 30, 89.38, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484686638448642, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-12', 1,
        '2024-03-12 17:00:00', '2024-03-12 18:00:00', 40, 129.14, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484686957215745, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-18', 1,
        '2024-03-18 17:00:00', '2024-03-18 18:00:00', 30, 119.14, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484687053684737, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-13', 1,
        '2024-03-13 17:00:00', '2024-03-13 18:00:00', 40, 186.77, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484687431172098, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-19', 1,
        '2024-03-19 17:00:00', '2024-03-19 18:00:00', 30, 38.48, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484687473115137, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-14', 1,
        '2024-03-14 17:00:00', '2024-03-14 18:00:00', 40, 136.64, '2024-03-09 23:22:23', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484687879962625, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-15', 1,
        '2024-03-15 17:00:00', '2024-03-15 18:00:00', 40, 190.24, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484687900934145, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-20', 1,
        '2024-03-20 17:00:00', '2024-03-20 18:00:00', 30, 190.57, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484688291004418, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-16', 1,
        '2024-03-16 17:00:00', '2024-03-16 18:00:00', 40, 92.29, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484688370696194, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-21', 1,
        '2024-03-21 17:00:00', '2024-03-21 18:00:00', 30, 62.50, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484688697851905, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-17', 1,
        '2024-03-17 17:00:00', '2024-03-17 18:00:00', 40, 124.63, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484688848846849, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-22', 1,
        '2024-03-22 17:00:00', '2024-03-22 18:00:00', 30, 24.18, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484689113088002, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-18', 1,
        '2024-03-18 17:00:00', '2024-03-18 18:00:00', 40, 121.44, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484689331191810, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-23', 1,
        '2024-03-23 17:00:00', '2024-03-23 18:00:00', 30, 160.40, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484689519935490, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-19', 1,
        '2024-03-19 17:00:00', '2024-03-19 18:00:00', 40, 74.58, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484689800953857, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-24', 1,
        '2024-03-24 17:00:00', '2024-03-24 18:00:00', 30, 27.76, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484689930977282, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-20', 1,
        '2024-03-20 17:00:00', '2024-03-20 18:00:00', 40, 50.95, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484690274910209, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-25', 1,
        '2024-03-25 17:00:00', '2024-03-25 18:00:00', 30, 160.33, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484690337824769, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-21', 1,
        '2024-03-21 17:00:00', '2024-03-21 18:00:00', 40, 114.49, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484690748866561, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-22', 1,
        '2024-03-22 17:00:00', '2024-03-22 18:00:00', 40, 152.07, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484690753060866, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-10', 2,
        '2024-03-10 18:00:00', '2024-03-10 19:00:00', 30, 92.89, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484691159908354, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-23', 1,
        '2024-03-23 17:00:00', '2024-03-23 18:00:00', 40, 105.85, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484691231211522, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-11', 2,
        '2024-03-11 18:00:00', '2024-03-11 19:00:00', 30, 77.52, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484691575144449, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-24', 1,
        '2024-03-24 17:00:00', '2024-03-24 18:00:00', 40, 91.09, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484691700973569, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-12', 2,
        '2024-03-12 18:00:00', '2024-03-12 19:00:00', 30, 57.80, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484691990380545, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-25', 1,
        '2024-03-25 17:00:00', '2024-03-25 18:00:00', 40, 105.74, '2024-03-09 23:22:24', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484692183318530, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-13', 2,
        '2024-03-13 18:00:00', '2024-03-13 19:00:00', 30, 150.38, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484692405616642, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-10', 2,
        '2024-03-10 18:00:00', '2024-03-10 19:00:00', 40, 30.22, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484692657274882, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-14', 2,
        '2024-03-14 18:00:00', '2024-03-14 19:00:00', 30, 16.86, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484692820852738, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-11', 2,
        '2024-03-11 18:00:00', '2024-03-11 19:00:00', 40, 24.04, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484693135425537, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-15', 2,
        '2024-03-15 18:00:00', '2024-03-15 19:00:00', 30, 55.41, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484693240283137, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-12', 2,
        '2024-03-12 18:00:00', '2024-03-12 19:00:00', 40, 172.02, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484693613576193, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-16', 2,
        '2024-03-16 18:00:00', '2024-03-16 19:00:00', 30, 157.32, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484693655519234, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-13', 2,
        '2024-03-13 18:00:00', '2024-03-13 19:00:00', 40, 91.61, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484694066561026, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-14', 2,
        '2024-03-14 18:00:00', '2024-03-14 19:00:00', 40, 121.64, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484694091726849, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-17', 2,
        '2024-03-17 18:00:00', '2024-03-17 19:00:00', 30, 16.54, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484694481797121, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-15', 2,
        '2024-03-15 18:00:00', '2024-03-15 19:00:00', 40, 93.74, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484694574071810, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-18', 2,
        '2024-03-18 18:00:00', '2024-03-18 19:00:00', 30, 84.58, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484694901227521, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-16', 2,
        '2024-03-16 18:00:00', '2024-03-16 19:00:00', 40, 127.62, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484695064805378, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-19', 2,
        '2024-03-19 18:00:00', '2024-03-19 19:00:00', 30, 98.18, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484695320657921, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-17', 2,
        '2024-03-17 18:00:00', '2024-03-17 19:00:00', 40, 75.91, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484695538761729, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-20', 2,
        '2024-03-20 18:00:00', '2024-03-20 19:00:00', 30, 120.01, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484695731699713, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-18', 2,
        '2024-03-18 18:00:00', '2024-03-18 19:00:00', 40, 148.27, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484696012718082, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-21', 2,
        '2024-03-21 18:00:00', '2024-03-21 19:00:00', 30, 134.95, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484696138547202, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-19', 2,
        '2024-03-19 18:00:00', '2024-03-19 19:00:00', 40, 113.12, '2024-03-09 23:22:25', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484696490868738, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-22', 2,
        '2024-03-22 18:00:00', '2024-03-22 19:00:00', 30, 53.86, '2024-03-09 23:22:26', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484696549588993, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-20', 2,
        '2024-03-20 18:00:00', '2024-03-20 19:00:00', 40, 41.99, '2024-03-09 23:22:26', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484696956436482, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-21', 2,
        '2024-03-21 18:00:00', '2024-03-21 19:00:00', 40, 153.15, '2024-03-09 23:22:26', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484696960630786, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-23', 2,
        '2024-03-23 18:00:00', '2024-03-23 19:00:00', 30, 146.16, '2024-03-09 23:22:26', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484697380061186, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-22', 2,
        '2024-03-22 18:00:00', '2024-03-22 19:00:00', 40, 168.50, '2024-03-09 23:22:26', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484697430392833, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-24', 2,
        '2024-03-24 18:00:00', '2024-03-24 19:00:00', 30, 123.49, '2024-03-09 23:22:26', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484697795297282, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-23', 2,
        '2024-03-23 18:00:00', '2024-03-23 19:00:00', 40, 199.07, '2024-03-09 23:22:26', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484697908543489, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-25', 2,
        '2024-03-25 18:00:00', '2024-03-25 19:00:00', 30, 119.49, '2024-03-09 23:22:26', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484698202144770, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-24', 2,
        '2024-03-24 18:00:00', '2024-03-24 19:00:00', 40, 129.62, '2024-03-09 23:22:26', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484698600603650, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-25', 2,
        '2024-03-25 18:00:00', '2024-03-25 19:00:00', 40, 100.33, '2024-03-09 23:22:26', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484699011645441, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-11', 1,
        '2024-03-11 01:00:00', '2024-03-12 04:00:00', 50, 24.04, '2024-03-09 23:22:26', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484699422687233, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-13', 1,
        '2024-03-13 01:00:00', '2024-03-14 04:00:00', 50, 87.14, '2024-03-09 23:22:26', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484699842117633, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-15', 1,
        '2024-03-15 01:00:00', '2024-03-16 04:00:00', 50, 158.22, '2024-03-09 23:22:26', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484700248965122, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-17', 1,
        '2024-03-17 01:00:00', '2024-03-18 04:00:00', 50, 75.04, '2024-03-09 23:22:26', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484700664201217, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-19', 1,
        '2024-03-19 01:00:00', '2024-03-20 04:00:00', 50, 66.21, '2024-03-09 23:22:27', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484701071048705, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-21', 1,
        '2024-03-21 01:00:00', '2024-03-22 04:00:00', 50, 173.22, '2024-03-09 23:22:27', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484701490479106, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-23', 1,
        '2024-03-23 01:00:00', '2024-03-24 04:00:00', 50, 194.58, '2024-03-09 23:22:27', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484701897326594, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-25', 1,
        '2024-03-25 01:00:00', '2024-03-26 04:00:00', 50, 130.60, '2024-03-09 23:22:27', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484702308368386, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-27', 1,
        '2024-03-27 01:00:00', '2024-03-28 04:00:00', 50, 120.29, '2024-03-09 23:22:27', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484702719410178, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-29', 1,
        '2024-03-29 01:00:00', '2024-03-30 04:00:00', 50, 34.88, '2024-03-09 23:22:27', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484703134646273, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-31', 1,
        '2024-03-31 01:00:00', '2024-04-01 04:00:00', 50, 145.89, '2024-03-09 23:22:27', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484703545688066, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-04-02', 1,
        '2024-04-02 01:00:00', '2024-04-03 04:00:00', 50, 89.35, '2024-03-09 23:22:27', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484703960924162, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-04-04', 1,
        '2024-04-04 01:00:00', '2024-04-05 04:00:00', 50, 42.73, '2024-03-09 23:22:27', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484704367771649, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-04-06', 1,
        '2024-04-06 01:00:00', '2024-04-07 04:00:00', 50, 171.93, '2024-03-09 23:22:27', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484704778813441, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-04-08', 1,
        '2024-04-08 01:00:00', '2024-04-09 04:00:00', 50, 164.11, '2024-03-09 23:22:28', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766484705185660929, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-04-10', 1,
        '2024-04-10 01:00:00', '2024-04-11 04:00:00', 50, 74.98, '2024-03-09 23:22:28', '2024-03-09 23:51:18', 1);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491963940438017, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-10', 1,
        '2024-03-10 06:05:00', '2024-03-10 07:00:00', 30, 173.23, '2024-03-09 23:51:18', '2024-03-09 23:51:18', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491963978186753, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-10', 1,
        '2024-03-10 06:05:00', '2024-03-10 07:00:00', 30, 173.14, '2024-03-09 23:51:18', '2024-03-09 23:51:18', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491964326313986, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-10', 1,
        '2024-03-10 11:00:00', '2024-03-10 12:00:00', 30, 198.99, '2024-03-09 23:51:18', '2024-03-09 23:51:18', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491964439560193, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-11', 1,
        '2024-03-11 06:05:00', '2024-03-11 07:00:00', 30, 104.05, '2024-03-09 23:51:18', '2024-03-09 23:51:18', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491964443754497, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-11', 1,
        '2024-03-11 06:05:00', '2024-03-11 07:00:00', 30, 144.06, '2024-03-09 23:51:18', '2024-03-09 23:51:18', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491964473114626, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-10', 1,
        '2024-03-10 11:00:00', '2024-03-10 12:00:00', 30, 142.91, '2024-03-09 23:51:18', '2024-03-09 23:51:18', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491964624109570, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-10', 1,
        '2024-03-10 11:00:00', '2024-03-10 12:00:00', 40, 94.41, '2024-03-09 23:51:18', '2024-03-09 23:51:18', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491964737355778, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-11', 1,
        '2024-03-11 11:00:00', '2024-03-11 12:00:00', 30, 148.26, '2024-03-09 23:51:18', '2024-03-09 23:51:18', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491964779298818, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-10', 1,
        '2024-03-10 11:00:00', '2024-03-10 12:00:00', 40, 117.34, '2024-03-09 23:51:18', '2024-03-09 23:51:18', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491964905127938, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-12', 1,
        '2024-03-12 06:05:00', '2024-03-12 07:00:00', 30, 50.44, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491964905127939, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-12', 1,
        '2024-03-12 06:05:00', '2024-03-12 07:00:00', 30, 189.02, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491964951265282, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-11', 1,
        '2024-03-11 11:00:00', '2024-03-11 12:00:00', 30, 89.67, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491964963848194, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-10', 1,
        '2024-03-10 11:00:00', '2024-03-10 12:00:00', 40, 164.41, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491964993208322, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-11', 1,
        '2024-03-11 11:00:00', '2024-03-11 12:00:00', 40, 15.24, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965022568450, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-10', 1,
        '2024-03-10 11:00:00', '2024-03-10 12:00:00', 40, 101.11, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965135814657, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-12', 1,
        '2024-03-12 11:00:00', '2024-03-12 12:00:00', 30, 170.33, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965240672258, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-11', 1,
        '2024-03-11 11:00:00', '2024-03-11 12:00:00', 40, 83.60, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965265838081, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-10', 1,
        '2024-03-10 15:00:00', '2024-03-10 17:00:00', 30, 114.22, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965379084289, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-12', 1,
        '2024-03-12 11:00:00', '2024-03-12 12:00:00', 40, 141.94, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965383278594, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-11', 1,
        '2024-03-11 11:00:00', '2024-03-11 12:00:00', 40, 39.05, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965383278595, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-13', 1,
        '2024-03-13 06:05:00', '2024-03-13 07:00:00', 30, 63.40, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965383278596, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-13', 1,
        '2024-03-13 06:05:00', '2024-03-13 07:00:00', 30, 80.72, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965387472898, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-10', 1,
        '2024-03-10 15:00:00', '2024-03-10 17:00:00', 30, 169.09, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965395861506, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-11', 1,
        '2024-03-11 11:00:00', '2024-03-11 12:00:00', 40, 99.29, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965509107713, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-10', 1,
        '2024-03-10 15:00:00', '2024-03-10 17:00:00', 40, 151.78, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965601382401, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-13', 1,
        '2024-03-13 11:00:00', '2024-03-13 12:00:00', 30, 46.91, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965634936833, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-13', 1,
        '2024-03-13 11:00:00', '2024-03-13 12:00:00', 30, 70.35, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965689462786, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-10', 1,
        '2024-03-10 15:00:00', '2024-03-10 17:00:00', 40, 114.17, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965735600130, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-11', 1,
        '2024-03-11 15:00:00', '2024-03-11 17:00:00', 30, 96.17, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965764960257, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-10', 1,
        '2024-03-10 17:00:00', '2024-03-10 18:00:00', 30, 153.81, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965802708993, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-12', 1,
        '2024-03-12 11:00:00', '2024-03-12 12:00:00', 40, 69.36, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965832069122, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-14', 1,
        '2024-03-14 06:05:00', '2024-03-14 07:00:00', 30, 116.50, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965840457730, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-11', 1,
        '2024-03-11 15:00:00', '2024-03-11 17:00:00', 30, 61.46, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965844652033, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-14', 1,
        '2024-03-14 06:05:00', '2024-03-14 07:00:00', 30, 43.06, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965861429250, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-13', 1,
        '2024-03-13 11:00:00', '2024-03-13 12:00:00', 40, 140.19, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965894983682, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-12', 1,
        '2024-03-12 11:00:00', '2024-03-12 12:00:00', 40, 79.48, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965987258369, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-10', 1,
        '2024-03-10 17:00:00', '2024-03-10 18:00:00', 30, 15.98, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491965991452674, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-13', 1,
        '2024-03-13 11:00:00', '2024-03-13 12:00:00', 40, 97.08, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966054367233, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-11', 1,
        '2024-03-11 15:00:00', '2024-03-11 17:00:00', 40, 160.92, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966209556481, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-12', 1,
        '2024-03-12 15:00:00', '2024-03-12 17:00:00', 30, 171.50, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966280859650, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-14', 1,
        '2024-03-14 11:00:00', '2024-03-14 12:00:00', 30, 28.93, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966347968513, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-14', 1,
        '2024-03-14 11:00:00', '2024-03-14 12:00:00', 30, 39.17, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966368940034, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-10', 1,
        '2024-03-10 17:00:00', '2024-03-10 18:00:00', 40, 154.31, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966398300161, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-10', 1,
        '2024-03-10 17:00:00', '2024-03-10 18:00:00', 40, 127.39, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966423465986, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-11', 1,
        '2024-03-11 17:00:00', '2024-03-11 18:00:00', 30, 122.20, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966440243202, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-15', 1,
        '2024-03-15 06:05:00', '2024-03-15 07:00:00', 30, 195.23, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966461214721, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-12', 1,
        '2024-03-12 15:00:00', '2024-03-12 17:00:00', 30, 80.49, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966469603329, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-13', 1,
        '2024-03-13 11:00:00', '2024-03-13 12:00:00', 40, 119.34, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966477991937, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-11', 1,
        '2024-03-11 01:00:00', '2024-03-12 04:00:00', 50, 11.03, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966519934978, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-15', 1,
        '2024-03-15 06:05:00', '2024-03-15 07:00:00', 30, 13.93, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966561878017, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-13', 1,
        '2024-03-13 11:00:00', '2024-03-13 12:00:00', 40, 87.28, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966561878018, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-14', 1,
        '2024-03-14 11:00:00', '2024-03-14 12:00:00', 40, 16.51, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966591238145, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-11', 1,
        '2024-03-11 17:00:00', '2024-03-11 18:00:00', 30, 84.59, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966670929921, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-12', 1,
        '2024-03-12 15:00:00', '2024-03-12 17:00:00', 40, 13.11, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966826119169, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-11', 1,
        '2024-03-11 01:00:00', '2024-03-12 04:00:00', 50, 119.40, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966880645121, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-12', 1,
        '2024-03-12 15:00:00', '2024-03-12 17:00:00', 40, 29.04, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491966981308418, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-13', 1,
        '2024-03-13 15:00:00', '2024-03-13 17:00:00', 30, 124.13, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967019057154, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-15', 1,
        '2024-03-15 11:00:00', '2024-03-15 12:00:00', 30, 120.65, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967056805890, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-11', 1,
        '2024-03-11 17:00:00', '2024-03-11 18:00:00', 40, 67.24, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967073583106, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-12', 1,
        '2024-03-12 17:00:00', '2024-03-12 18:00:00', 30, 98.85, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967077777410, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-15', 1,
        '2024-03-15 11:00:00', '2024-03-15 12:00:00', 30, 127.30, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967081971713, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-11', 1,
        '2024-03-11 17:00:00', '2024-03-11 18:00:00', 40, 36.66, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967102943233, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-14', 1,
        '2024-03-14 11:00:00', '2024-03-14 12:00:00', 40, 194.47, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967157469185, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-15', 1,
        '2024-03-15 11:00:00', '2024-03-15 12:00:00', 40, 37.93, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967174246402, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-16', 1,
        '2024-03-16 06:05:00', '2024-03-16 07:00:00', 30, 82.26, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967191023618, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-15', 1,
        '2024-03-15 11:00:00', '2024-03-15 12:00:00', 40, 44.87, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967224578049, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-13', 1,
        '2024-03-13 15:00:00', '2024-03-13 17:00:00', 30, 193.64, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967232966657, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-14', 1,
        '2024-03-14 11:00:00', '2024-03-14 12:00:00', 40, 45.50, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967232966658, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-13', 1,
        '2024-03-13 01:00:00', '2024-03-14 04:00:00', 50, 53.74, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967287492610, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-16', 1,
        '2024-03-16 06:05:00', '2024-03-16 07:00:00', 30, 31.04, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967316852738, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-12', 1,
        '2024-03-12 17:00:00', '2024-03-12 18:00:00', 30, 62.76, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967421710337, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-13', 1,
        '2024-03-13 01:00:00', '2024-03-14 04:00:00', 50, 135.22, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967694340098, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-13', 1,
        '2024-03-13 15:00:00', '2024-03-13 17:00:00', 40, 178.74, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967727894529, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-13', 1,
        '2024-03-13 15:00:00', '2024-03-13 17:00:00', 40, 101.84, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967757254657, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-14', 1,
        '2024-03-14 15:00:00', '2024-03-14 17:00:00', 30, 81.23, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967757254658, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-12', 1,
        '2024-03-12 17:00:00', '2024-03-12 18:00:00', 40, 55.84, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967820169218, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-13', 1,
        '2024-03-13 17:00:00', '2024-03-13 18:00:00', 30, 113.28, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967832752130, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-14', 1,
        '2024-03-14 15:00:00', '2024-03-14 17:00:00', 30, 61.97, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967836946433, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-16', 1,
        '2024-03-16 11:00:00', '2024-03-16 12:00:00', 30, 112.79, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967849529345, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-12', 1,
        '2024-03-12 17:00:00', '2024-03-12 18:00:00', 40, 141.89, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967887278081, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-17', 1,
        '2024-03-17 06:05:00', '2024-03-17 07:00:00', 30, 49.42, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967908249602, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-16', 1,
        '2024-03-16 11:00:00', '2024-03-16 12:00:00', 40, 11.09, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967908249603, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-17', 1,
        '2024-03-17 06:05:00', '2024-03-17 07:00:00', 30, 182.28, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967962775554, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-16', 1,
        '2024-03-16 11:00:00', '2024-03-16 12:00:00', 40, 95.60, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967971164161, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-15', 1,
        '2024-03-15 11:00:00', '2024-03-15 12:00:00', 40, 156.63, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491967987941378, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-16', 1,
        '2024-03-16 11:00:00', '2024-03-16 12:00:00', 30, 32.44, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968004718594, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-15', 1,
        '2024-03-15 01:00:00', '2024-03-16 04:00:00', 50, 155.84, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968096993281, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-13', 1,
        '2024-03-13 17:00:00', '2024-03-13 18:00:00', 30, 156.97, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968164102145, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-15', 1,
        '2024-03-15 11:00:00', '2024-03-15 12:00:00', 40, 65.47, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968394788866, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-15', 1,
        '2024-03-15 01:00:00', '2024-03-16 04:00:00', 50, 38.82, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968415760385, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-14', 1,
        '2024-03-14 15:00:00', '2024-03-14 17:00:00', 40, 116.81, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968466092034, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-14', 1,
        '2024-03-14 15:00:00', '2024-03-14 17:00:00', 40, 156.12, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968529006594, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-13', 1,
        '2024-03-13 17:00:00', '2024-03-13 18:00:00', 40, 151.99, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968558366722, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-15', 1,
        '2024-03-15 15:00:00', '2024-03-15 17:00:00', 30, 149.43, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968558366723, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-14', 1,
        '2024-03-14 17:00:00', '2024-03-14 18:00:00', 30, 153.98, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968617086977, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-15', 1,
        '2024-03-15 15:00:00', '2024-03-15 17:00:00', 30, 20.59, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968633864194, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-17', 1,
        '2024-03-17 11:00:00', '2024-03-17 12:00:00', 30, 193.92, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968654835713, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-18', 1,
        '2024-03-18 06:05:00', '2024-03-18 07:00:00', 30, 61.36, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968654835714, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-18', 1,
        '2024-03-18 06:05:00', '2024-03-18 07:00:00', 30, 54.76, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968709361666, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-13', 1,
        '2024-03-13 17:00:00', '2024-03-13 18:00:00', 40, 188.43, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968772276225, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-17', 1,
        '2024-03-17 11:00:00', '2024-03-17 12:00:00', 40, 96.56, '2024-03-09 23:51:19', '2024-03-09 23:51:19', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968776470530, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-17', 1,
        '2024-03-17 11:00:00', '2024-03-17 12:00:00', 40, 83.86, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968868745218, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-17', 1,
        '2024-03-17 11:00:00', '2024-03-17 12:00:00', 30, 27.32, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491968923271169, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-14', 1,
        '2024-03-14 17:00:00', '2024-03-14 18:00:00', 30, 67.34, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969271398402, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-17', 1,
        '2024-03-17 01:00:00', '2024-03-18 04:00:00', 50, 91.80, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969359478785, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-15', 1,
        '2024-03-15 15:00:00', '2024-03-15 17:00:00', 40, 161.36, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969367867394, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-14', 1,
        '2024-03-14 17:00:00', '2024-03-14 18:00:00', 40, 15.84, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969372061697, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-16', 1,
        '2024-03-16 15:00:00', '2024-03-16 17:00:00', 30, 141.45, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969393033218, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-15', 1,
        '2024-03-15 15:00:00', '2024-03-15 17:00:00', 40, 121.76, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969401421826, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-19', 1,
        '2024-03-19 06:05:00', '2024-03-19 07:00:00', 30, 26.14, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969409810434, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-15', 1,
        '2024-03-15 17:00:00', '2024-03-15 18:00:00', 30, 192.00, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969414004738, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-16', 1,
        '2024-03-16 15:00:00', '2024-03-16 17:00:00', 30, 36.91, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969493696514, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-19', 1,
        '2024-03-19 06:05:00', '2024-03-19 07:00:00', 30, 20.21, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969510473729, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-16', 1,
        '2024-03-16 11:00:00', '2024-03-16 12:00:00', 40, 151.08, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969518862338, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-18', 1,
        '2024-03-18 11:00:00', '2024-03-18 12:00:00', 30, 162.41, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969523056641, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-17', 1,
        '2024-03-17 01:00:00', '2024-03-18 04:00:00', 50, 44.95, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969548222466, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-14', 1,
        '2024-03-14 17:00:00', '2024-03-14 18:00:00', 40, 146.26, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969556611073, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-16', 1,
        '2024-03-16 11:00:00', '2024-03-16 12:00:00', 40, 62.85, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969564999682, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-18', 1,
        '2024-03-18 11:00:00', '2024-03-18 12:00:00', 40, 26.11, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969695023106, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-18', 1,
        '2024-03-18 11:00:00', '2024-03-18 12:00:00', 40, 35.95, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491969720188930, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-18', 1,
        '2024-03-18 11:00:00', '2024-03-18 12:00:00', 30, 106.34, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970013790209, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-16', 1,
        '2024-03-16 15:00:00', '2024-03-16 17:00:00', 40, 181.05, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970114453505, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-16', 1,
        '2024-03-16 15:00:00', '2024-03-16 17:00:00', 40, 191.83, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970118647810, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-15', 1,
        '2024-03-15 17:00:00', '2024-03-15 18:00:00', 40, 157.78, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970127036417, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-20', 1,
        '2024-03-20 06:05:00', '2024-03-20 07:00:00', 30, 115.75, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970148007938, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-16', 1,
        '2024-03-16 17:00:00', '2024-03-16 18:00:00', 30, 23.31, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970177368065, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-17', 1,
        '2024-03-17 15:00:00', '2024-03-17 17:00:00', 30, 21.72, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970181562369, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-19', 1,
        '2024-03-19 01:00:00', '2024-03-20 04:00:00', 50, 60.59, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970215116801, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-19', 1,
        '2024-03-19 11:00:00', '2024-03-19 12:00:00', 30, 43.69, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970248671233, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-16', 1,
        '2024-03-16 17:00:00', '2024-03-16 18:00:00', 30, 91.29, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970257059841, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-19', 1,
        '2024-03-19 01:00:00', '2024-03-20 04:00:00', 50, 193.25, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970257059842, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-20', 1,
        '2024-03-20 06:05:00', '2024-03-20 07:00:00', 30, 133.67, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970257059843, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-17', 1,
        '2024-03-17 11:00:00', '2024-03-17 12:00:00', 40, 22.63, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970278031361, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-15', 1,
        '2024-03-15 17:00:00', '2024-03-15 18:00:00', 40, 163.43, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970307391489, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-19', 1,
        '2024-03-19 11:00:00', '2024-03-19 12:00:00', 30, 34.27, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970332557313, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-17', 1,
        '2024-03-17 11:00:00', '2024-03-17 12:00:00', 40, 159.43, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970336751617, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-17', 1,
        '2024-03-17 15:00:00', '2024-03-17 17:00:00', 30, 112.14, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970395471873, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-19', 1,
        '2024-03-19 11:00:00', '2024-03-19 12:00:00', 40, 81.75, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970403860482, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-19', 1,
        '2024-03-19 11:00:00', '2024-03-19 12:00:00', 40, 132.68, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970806513665, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-21', 1,
        '2024-03-21 01:00:00', '2024-03-22 04:00:00', 50, 10.08, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970819096577, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-17', 1,
        '2024-03-17 15:00:00', '2024-03-17 17:00:00', 40, 106.18, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970819096578, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-17', 1,
        '2024-03-17 15:00:00', '2024-03-17 17:00:00', 40, 143.51, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970894594049, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-21', 1,
        '2024-03-21 06:05:00', '2024-03-21 07:00:00', 30, 18.94, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970923954178, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-16', 1,
        '2024-03-16 17:00:00', '2024-03-16 18:00:00', 40, 94.03, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970923954179, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-18', 1,
        '2024-03-18 15:00:00', '2024-03-18 17:00:00', 30, 147.66, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970965897217, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-21', 1,
        '2024-03-21 01:00:00', '2024-03-22 04:00:00', 50, 199.48, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970965897218, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-20', 1,
        '2024-03-20 11:00:00', '2024-03-20 12:00:00', 30, 79.28, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970970091521, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-17', 1,
        '2024-03-17 17:00:00', '2024-03-17 18:00:00', 30, 125.46, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491970982674434, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-21', 1,
        '2024-03-21 06:05:00', '2024-03-21 07:00:00', 30, 186.72, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971028811777, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-17', 1,
        '2024-03-17 17:00:00', '2024-03-17 18:00:00', 30, 99.92, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971049783298, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-18', 1,
        '2024-03-18 11:00:00', '2024-03-18 12:00:00', 40, 45.10, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971062366209, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-16', 1,
        '2024-03-16 17:00:00', '2024-03-16 18:00:00', 40, 141.20, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971100114945, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-20', 1,
        '2024-03-20 11:00:00', '2024-03-20 12:00:00', 40, 195.83, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971108503554, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-20', 1,
        '2024-03-20 11:00:00', '2024-03-20 12:00:00', 30, 162.52, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971108503555, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-18', 1,
        '2024-03-18 11:00:00', '2024-03-18 12:00:00', 40, 17.96, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971146252289, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-20', 1,
        '2024-03-20 11:00:00', '2024-03-20 12:00:00', 40, 103.59, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971154640897, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-18', 1,
        '2024-03-18 15:00:00', '2024-03-18 17:00:00', 30, 100.74, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971410493441, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-23', 1,
        '2024-03-23 01:00:00', '2024-03-24 04:00:00', 50, 18.06, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971527933954, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-22', 1,
        '2024-03-22 06:05:00', '2024-03-22 07:00:00', 30, 186.00, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971557294082, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-18', 1,
        '2024-03-18 15:00:00', '2024-03-18 17:00:00', 40, 89.82, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971565682690, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-22', 1,
        '2024-03-22 06:05:00', '2024-03-22 07:00:00', 30, 153.97, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971641180162, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-18', 1,
        '2024-03-18 17:00:00', '2024-03-18 18:00:00', 30, 61.67, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971653763073, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-23', 1,
        '2024-03-23 01:00:00', '2024-03-24 04:00:00', 50, 122.38, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971666345986, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-21', 1,
        '2024-03-21 11:00:00', '2024-03-21 12:00:00', 30, 104.75, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971716677633, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-19', 1,
        '2024-03-19 15:00:00', '2024-03-19 17:00:00', 30, 113.96, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971779592193, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-18', 1,
        '2024-03-18 15:00:00', '2024-03-18 17:00:00', 40, 196.86, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971804758018, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-18', 1,
        '2024-03-18 17:00:00', '2024-03-18 18:00:00', 30, 85.89, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971808952322, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-17', 1,
        '2024-03-17 17:00:00', '2024-03-17 18:00:00', 40, 34.45, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971825729538, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-19', 1,
        '2024-03-19 11:00:00', '2024-03-19 12:00:00', 40, 149.03, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971905421313, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-21', 1,
        '2024-03-21 11:00:00', '2024-03-21 12:00:00', 40, 82.18, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491971968335874, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-21', 1,
        '2024-03-21 11:00:00', '2024-03-21 12:00:00', 30, 14.12, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972001890306, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-19', 1,
        '2024-03-19 15:00:00', '2024-03-19 17:00:00', 30, 158.38, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972010278913, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-19', 1,
        '2024-03-19 11:00:00', '2024-03-19 12:00:00', 40, 168.57, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972110942210, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-17', 1,
        '2024-03-17 17:00:00', '2024-03-17 18:00:00', 40, 175.63, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972165468161, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-21', 1,
        '2024-03-21 11:00:00', '2024-03-21 12:00:00', 40, 14.70, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972194828290, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-25', 1,
        '2024-03-25 01:00:00', '2024-03-26 04:00:00', 50, 194.18, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972253548546, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-23', 1,
        '2024-03-23 06:05:00', '2024-03-23 07:00:00', 30, 86.70, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972350017537, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-19', 1,
        '2024-03-19 15:00:00', '2024-03-19 17:00:00', 40, 39.30, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972362600450, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-23', 1,
        '2024-03-23 06:05:00', '2024-03-23 07:00:00', 30, 191.42, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972404543490, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-25', 1,
        '2024-03-25 01:00:00', '2024-03-26 04:00:00', 50, 37.50, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972404543491, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-19', 1,
        '2024-03-19 17:00:00', '2024-03-19 18:00:00', 30, 12.94, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972484235266, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-22', 1,
        '2024-03-22 11:00:00', '2024-03-22 12:00:00', 30, 29.82, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972496818178, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-20', 1,
        '2024-03-20 15:00:00', '2024-03-20 17:00:00', 30, 13.65, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972618452993, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-20', 1,
        '2024-03-20 11:00:00', '2024-03-20 12:00:00', 40, 89.81, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972626841602, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-18', 1,
        '2024-03-18 17:00:00', '2024-03-18 18:00:00', 40, 46.93, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972643618818, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-22', 1,
        '2024-03-22 11:00:00', '2024-03-22 12:00:00', 40, 191.96, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972744282113, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-19', 1,
        '2024-03-19 15:00:00', '2024-03-19 17:00:00', 40, 47.19, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972773642242, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-20', 1,
        '2024-03-20 15:00:00', '2024-03-20 17:00:00', 30, 36.44, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972786225154, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-22', 1,
        '2024-03-22 11:00:00', '2024-03-22 12:00:00', 30, 30.31, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972790419458, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-20', 1,
        '2024-03-20 11:00:00', '2024-03-20 12:00:00', 40, 46.36, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972828168194, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-27', 1,
        '2024-03-27 01:00:00', '2024-03-28 04:00:00', 50, 93.29, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972832362498, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-18', 1,
        '2024-03-18 17:00:00', '2024-03-18 18:00:00', 40, 29.23, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972882694145, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-22', 1,
        '2024-03-22 11:00:00', '2024-03-22 12:00:00', 40, 103.08, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972949803009, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-20', 1,
        '2024-03-20 17:00:00', '2024-03-20 18:00:00', 30, 113.45, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972953997314, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-24', 1,
        '2024-03-24 06:05:00', '2024-03-24 07:00:00', 30, 84.22, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491972966580225, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-27', 1,
        '2024-03-27 01:00:00', '2024-03-28 04:00:00', 50, 141.76, '2024-03-09 23:51:20', '2024-03-09 23:51:20', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973021106178, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-20', 1,
        '2024-03-20 17:00:00', '2024-03-20 18:00:00', 30, 179.41, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973109186562, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-20', 1,
        '2024-03-20 15:00:00', '2024-03-20 17:00:00', 40, 50.24, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973255987202, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-24', 1,
        '2024-03-24 06:05:00', '2024-03-24 07:00:00', 30, 179.27, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973268570113, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-23', 1,
        '2024-03-23 11:00:00', '2024-03-23 12:00:00', 30, 65.32, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973339873281, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-21', 1,
        '2024-03-21 15:00:00', '2024-03-21 17:00:00', 30, 37.57, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973411176450, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-21', 1,
        '2024-03-21 11:00:00', '2024-03-21 12:00:00', 40, 33.08, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973415370754, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-23', 1,
        '2024-03-23 11:00:00', '2024-03-23 12:00:00', 40, 164.29, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973432147969, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-19', 1,
        '2024-03-19 17:00:00', '2024-03-19 18:00:00', 40, 69.61, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973486673922, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-21', 1,
        '2024-03-21 15:00:00', '2024-03-21 17:00:00', 30, 185.96, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973532811266, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-20', 1,
        '2024-03-20 15:00:00', '2024-03-20 17:00:00', 40, 117.31, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973537005569, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-23', 1,
        '2024-03-23 11:00:00', '2024-03-23 12:00:00', 30, 129.36, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973566365697, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-29', 1,
        '2024-03-29 01:00:00', '2024-03-30 04:00:00', 50, 175.82, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973566365698, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-21', 1,
        '2024-03-21 11:00:00', '2024-03-21 12:00:00', 40, 160.45, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973595725826, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-19', 1,
        '2024-03-19 17:00:00', '2024-03-19 18:00:00', 40, 170.74, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973679611905, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-21', 1,
        '2024-03-21 17:00:00', '2024-03-21 18:00:00', 30, 83.23, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973679611906, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-23', 1,
        '2024-03-23 11:00:00', '2024-03-23 12:00:00', 40, 97.35, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973721554946, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-29', 1,
        '2024-03-29 01:00:00', '2024-03-30 04:00:00', 50, 54.71, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973721554947, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-21', 1,
        '2024-03-21 15:00:00', '2024-03-21 17:00:00', 40, 141.77, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973826412546, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-21', 1,
        '2024-03-21 17:00:00', '2024-03-21 18:00:00', 30, 19.69, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973876744193, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-25', 1,
        '2024-03-25 06:05:00', '2024-03-25 07:00:00', 30, 73.55, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491973964824577, 1765263539561332738, 1765262891990155265, 1765262350547451906, '2024-03-25', 1,
        '2024-03-25 06:05:00', '2024-03-25 07:00:00', 30, 170.37, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974031933442, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-24', 1,
        '2024-03-24 11:00:00', '2024-03-24 12:00:00', 30, 12.42, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974115819522, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-22', 1,
        '2024-03-22 15:00:00', '2024-03-22 17:00:00', 30, 157.71, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974187122690, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-22', 1,
        '2024-03-22 11:00:00', '2024-03-22 12:00:00', 40, 29.94, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974187122691, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-24', 1,
        '2024-03-24 11:00:00', '2024-03-24 12:00:00', 40, 67.53, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974203899906, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-22', 1,
        '2024-03-22 15:00:00', '2024-03-22 17:00:00', 30, 36.17, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974254231554, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-20', 1,
        '2024-03-20 17:00:00', '2024-03-20 18:00:00', 40, 116.15, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974342311937, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-24', 1,
        '2024-03-24 11:00:00', '2024-03-24 12:00:00', 30, 110.57, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974346506242, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-31', 1,
        '2024-03-31 01:00:00', '2024-04-01 04:00:00', 50, 72.43, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974367477762, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-22', 1,
        '2024-03-22 11:00:00', '2024-03-22 12:00:00', 40, 51.42, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974396837889, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-20', 1,
        '2024-03-20 17:00:00', '2024-03-20 18:00:00', 40, 44.76, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974422003713, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-24', 1,
        '2024-03-24 11:00:00', '2024-03-24 12:00:00', 40, 62.53, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974463946753, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-22', 1,
        '2024-03-22 17:00:00', '2024-03-22 18:00:00', 30, 196.29, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974514278402, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-03-31', 1,
        '2024-03-31 01:00:00', '2024-04-01 04:00:00', 50, 125.86, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974514278403, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-22', 1,
        '2024-03-22 17:00:00', '2024-03-22 18:00:00', 30, 32.89, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974585581569, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-22', 1,
        '2024-03-22 15:00:00', '2024-03-22 17:00:00', 40, 16.41, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974585581570, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-22', 1,
        '2024-03-22 15:00:00', '2024-03-22 17:00:00', 40, 27.27, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974652690434, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-10', 2,
        '2024-03-10 07:00:00', '2024-03-10 08:00:00', 30, 54.55, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974686244865, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-10', 2,
        '2024-03-10 07:00:00', '2024-03-10 08:00:00', 30, 181.54, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974799491074, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-25', 1,
        '2024-03-25 11:00:00', '2024-03-25 12:00:00', 30, 182.77, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974837239810, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-23', 1,
        '2024-03-23 15:00:00', '2024-03-23 17:00:00', 30, 40.81, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974954680321, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-23', 1,
        '2024-03-23 15:00:00', '2024-03-23 17:00:00', 30, 145.74, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491974975651842, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-25', 1,
        '2024-03-25 11:00:00', '2024-03-25 12:00:00', 40, 142.55, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975101480962, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-21', 1,
        '2024-03-21 17:00:00', '2024-03-21 18:00:00', 40, 177.30, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975109869570, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-04-02', 1,
        '2024-04-02 01:00:00', '2024-04-03 04:00:00', 50, 89.37, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975109869571, 1765263566656536577, 1765262996428324866, 1765262396345057282, '2024-03-25', 1,
        '2024-03-25 11:00:00', '2024-03-25 12:00:00', 30, 72.66, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975118258177, 1765268806080430082, 1765262996428324866, 1765262396345057282, '2024-03-25', 1,
        '2024-03-25 11:00:00', '2024-03-25 12:00:00', 40, 169.32, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975189561346, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-23', 1,
        '2024-03-23 11:00:00', '2024-03-23 12:00:00', 40, 57.71, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975193755649, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-21', 1,
        '2024-03-21 17:00:00', '2024-03-21 18:00:00', 40, 165.85, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975218921474, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-23', 1,
        '2024-03-23 17:00:00', '2024-03-23 18:00:00', 30, 75.67, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975231504385, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-23', 1,
        '2024-03-23 11:00:00', '2024-03-23 12:00:00', 40, 128.20, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975248281602, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-04-02', 1,
        '2024-04-02 01:00:00', '2024-04-03 04:00:00', 50, 173.41, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975256670209, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-23', 1,
        '2024-03-23 17:00:00', '2024-03-23 18:00:00', 30, 134.01, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975315390466, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-24', 1,
        '2024-03-24 15:00:00', '2024-03-24 17:00:00', 30, 57.10, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975323779073, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-23', 1,
        '2024-03-23 15:00:00', '2024-03-23 17:00:00', 40, 48.62, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975344750594, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-23', 1,
        '2024-03-23 15:00:00', '2024-03-23 17:00:00', 40, 105.25, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975382499330, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-11', 2,
        '2024-03-11 07:00:00', '2024-03-11 08:00:00', 30, 116.80, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975583825922, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-11', 2,
        '2024-03-11 07:00:00', '2024-03-11 08:00:00', 30, 166.72, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975676100609, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-10', 2,
        '2024-03-10 12:00:00', '2024-03-10 13:00:05', 30, 41.26, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975718043649, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-10', 2,
        '2024-03-10 12:00:00', '2024-03-10 13:00:05', 40, 73.02, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975747403778, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-24', 1,
        '2024-03-24 15:00:00', '2024-03-24 17:00:00', 30, 109.48, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975818706945, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-10', 2,
        '2024-03-10 12:00:00', '2024-03-10 13:00:05', 30, 79.15, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975843872770, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-10', 2,
        '2024-03-10 12:00:00', '2024-03-10 13:00:05', 40, 128.53, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975843872771, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-04-04', 1,
        '2024-04-04 01:00:00', '2024-04-05 04:00:00', 50, 22.53, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975894204417, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-24', 1,
        '2024-03-24 11:00:00', '2024-03-24 12:00:00', 40, 26.29, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975906787330, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-22', 1,
        '2024-03-22 17:00:00', '2024-03-22 18:00:00', 40, 199.90, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975961313281, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-24', 1,
        '2024-03-24 17:00:00', '2024-03-24 18:00:00', 30, 105.00, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975961313282, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-22', 1,
        '2024-03-22 17:00:00', '2024-03-22 18:00:00', 40, 61.92, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975969701890, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-24', 1,
        '2024-03-24 11:00:00', '2024-03-24 12:00:00', 40, 101.15, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491975986479105, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-04-04', 1,
        '2024-04-04 01:00:00', '2024-04-05 04:00:00', 50, 196.34, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976028422146, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-24', 1,
        '2024-03-24 17:00:00', '2024-03-24 18:00:00', 30, 84.42, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976045199361, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-24', 1,
        '2024-03-24 15:00:00', '2024-03-24 17:00:00', 40, 65.01, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976103919617, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-25', 1,
        '2024-03-25 15:00:00', '2024-03-25 17:00:00', 30, 145.32, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976162639873, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-12', 2,
        '2024-03-12 07:00:00', '2024-03-12 08:00:00', 30, 101.38, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976233943042, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-24', 1,
        '2024-03-24 15:00:00', '2024-03-24 17:00:00', 40, 173.97, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976389132289, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-12', 2,
        '2024-03-12 07:00:00', '2024-03-12 08:00:00', 30, 140.60, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976510767106, 1765263598201896961, 1765263062421504002, 1765262432873250817, '2024-03-25', 1,
        '2024-03-25 15:00:00', '2024-03-25 17:00:00', 30, 160.75, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976519155713, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-11', 2,
        '2024-03-11 12:00:00', '2024-03-11 13:00:05', 30, 50.76, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976527544321, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-11', 2,
        '2024-03-11 12:00:00', '2024-03-11 13:00:05', 40, 113.28, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976540127234, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-04-06', 1,
        '2024-04-06 01:00:00', '2024-04-07 04:00:00', 50, 102.85, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976607236097, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-11', 2,
        '2024-03-11 12:00:00', '2024-03-11 13:00:05', 30, 29.44, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976670150657, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-04-06', 1,
        '2024-04-06 01:00:00', '2024-04-07 04:00:00', 50, 113.14, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976670150658, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-23', 1,
        '2024-03-23 17:00:00', '2024-03-23 18:00:00', 40, 168.48, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976670150659, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-23', 1,
        '2024-03-23 17:00:00', '2024-03-23 18:00:00', 40, 170.64, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976691122177, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-25', 1,
        '2024-03-25 17:00:00', '2024-03-25 18:00:00', 30, 173.79, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976728870913, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-25', 1,
        '2024-03-25 11:00:00', '2024-03-25 12:00:00', 40, 156.51, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976728870914, 1765263622000377858, 1765263153815388161, 1765262350547451906, '2024-03-25', 1,
        '2024-03-25 17:00:00', '2024-03-25 18:00:00', 30, 58.55, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976816951298, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-11', 2,
        '2024-03-11 12:00:00', '2024-03-11 13:00:05', 40, 159.37, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976816951299, 1765268884211924993, 1765262996428324866, 1765262396345057282, '2024-03-25', 1,
        '2024-03-25 11:00:00', '2024-03-25 12:00:00', 40, 66.30, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976825339905, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-13', 2,
        '2024-03-13 07:00:00', '2024-03-13 08:00:00', 30, 17.03, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491976984723457, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-25', 1,
        '2024-03-25 15:00:00', '2024-03-25 17:00:00', 40, 169.20, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977131524098, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-13', 2,
        '2024-03-13 07:00:00', '2024-03-13 08:00:00', 30, 155.40, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977135718402, 1765268921289572353, 1765263062421504002, 1765262432873250817, '2024-03-25', 1,
        '2024-03-25 15:00:00', '2024-03-25 17:00:00', 40, 11.21, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977215410178, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-12', 2,
        '2024-03-12 12:00:00', '2024-03-12 13:00:05', 40, 135.11, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977248964609, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-10', 2,
        '2024-03-10 18:00:00', '2024-03-10 19:00:00', 30, 17.58, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977248964610, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-24', 1,
        '2024-03-24 17:00:00', '2024-03-24 18:00:00', 40, 131.56, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977278324738, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-04-08', 1,
        '2024-04-08 01:00:00', '2024-04-09 04:00:00', 50, 39.95, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977286713345, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-10', 2,
        '2024-03-10 12:00:00', '2024-03-10 13:00:05', 40, 193.50, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977353822209, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-24', 1,
        '2024-03-24 17:00:00', '2024-03-24 18:00:00', 40, 172.78, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977353822210, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-12', 2,
        '2024-03-12 12:00:00', '2024-03-12 13:00:05', 30, 90.44, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977366405122, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-04-08', 1,
        '2024-04-08 01:00:00', '2024-04-09 04:00:00', 50, 42.91, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977391570945, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-10', 2,
        '2024-03-10 18:00:00', '2024-03-10 19:00:00', 30, 63.32, '2024-03-09 23:51:21', '2024-03-09 23:51:21', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977483845633, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-10', 2,
        '2024-03-10 12:00:00', '2024-03-10 13:00:05', 40, 187.34, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977546760194, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-12', 2,
        '2024-03-12 12:00:00', '2024-03-12 13:00:05', 40, 29.02, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977555148801, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-14', 2,
        '2024-03-14 07:00:00', '2024-03-14 08:00:00', 30, 72.92, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977722920962, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-12', 2,
        '2024-03-12 12:00:00', '2024-03-12 13:00:05', 30, 158.69, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977752281089, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-14', 2,
        '2024-03-14 07:00:00', '2024-03-14 08:00:00', 30, 141.04, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977840361473, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-11', 2,
        '2024-03-11 18:00:00', '2024-03-11 19:00:00', 30, 37.43, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977873915906, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-13', 2,
        '2024-03-13 12:00:00', '2024-03-13 13:00:05', 40, 167.49, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977894887426, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-25', 1,
        '2024-03-25 17:00:00', '2024-03-25 18:00:00', 40, 16.37, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977907470337, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-11', 2,
        '2024-03-11 12:00:00', '2024-03-11 13:00:05', 40, 171.16, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977957801986, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-11', 2,
        '2024-03-11 18:00:00', '2024-03-11 19:00:00', 30, 163.53, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491977957801987, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-11', 2,
        '2024-03-11 12:00:00', '2024-03-11 13:00:05', 40, 181.99, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978016522241, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-04-10', 1,
        '2024-04-10 01:00:00', '2024-04-11 04:00:00', 50, 77.15, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978024910849, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-13', 2,
        '2024-03-13 12:00:00', '2024-03-13 13:00:05', 30, 115.79, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978037493761, 1766476180275433473, 1766476032442994690, 1765262528792788993, '2024-04-10', 1,
        '2024-04-10 01:00:00', '2024-04-11 04:00:00', 50, 121.26, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978066853889, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-15', 2,
        '2024-03-15 07:00:00', '2024-03-15 08:00:00', 30, 106.26, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978066853890, 1765268946048548866, 1765263153815388161, 1765262350547451906, '2024-03-25', 1,
        '2024-03-25 17:00:00', '2024-03-25 18:00:00', 40, 169.35, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978310123521, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-15', 2,
        '2024-03-15 07:00:00', '2024-03-15 08:00:00', 30, 128.91, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978385620993, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-13', 2,
        '2024-03-13 12:00:00', '2024-03-13 13:00:05', 30, 84.05, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978427564033, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-14', 2,
        '2024-03-14 12:00:00', '2024-03-14 13:00:05', 40, 29.56, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978427564034, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-14', 2,
        '2024-03-14 12:00:00', '2024-03-14 13:00:05', 40, 77.02, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978528227329, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-12', 2,
        '2024-03-12 12:00:00', '2024-03-12 13:00:05', 40, 121.47, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978528227330, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-12', 2,
        '2024-03-12 12:00:00', '2024-03-12 13:00:05', 40, 72.13, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978528227331, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-12', 2,
        '2024-03-12 18:00:00', '2024-03-12 19:00:00', 30, 22.94, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978532421633, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-10', 2,
        '2024-03-10 18:00:00', '2024-03-10 19:00:00', 40, 17.37, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978549198849, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-14', 2,
        '2024-03-14 12:00:00', '2024-03-14 13:00:05', 30, 122.10, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978561781762, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-12', 2,
        '2024-03-12 18:00:00', '2024-03-12 19:00:00', 30, 28.24, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978670833666, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-10', 2,
        '2024-03-10 18:00:00', '2024-03-10 19:00:00', 40, 190.84, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978679222274, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-16', 2,
        '2024-03-16 07:00:00', '2024-03-16 08:00:00', 30, 163.52, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978838605825, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-16', 2,
        '2024-03-16 07:00:00', '2024-03-16 08:00:00', 30, 125.83, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978897326081, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-15', 2,
        '2024-03-15 12:00:00', '2024-03-15 13:00:05', 40, 100.06, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978901520385, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-13', 2,
        '2024-03-13 18:00:00', '2024-03-13 19:00:00', 30, 189.79, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491978935074817, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-15', 2,
        '2024-03-15 12:00:00', '2024-03-15 13:00:05', 40, 106.97, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979018960898, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-11', 2,
        '2024-03-11 18:00:00', '2024-03-11 19:00:00', 40, 68.73, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979018960899, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-15', 2,
        '2024-03-15 12:00:00', '2024-03-15 13:00:05', 30, 31.23, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979039932418, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-13', 2,
        '2024-03-13 12:00:00', '2024-03-13 13:00:05', 40, 140.96, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979048321026, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-13', 2,
        '2024-03-13 12:00:00', '2024-03-13 13:00:05', 40, 160.86, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979140595714, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-15', 2,
        '2024-03-15 12:00:00', '2024-03-15 13:00:05', 30, 19.33, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979140595715, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-11', 2,
        '2024-03-11 18:00:00', '2024-03-11 19:00:00', 40, 84.49, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979153178626, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-13', 2,
        '2024-03-13 18:00:00', '2024-03-13 19:00:00', 30, 106.26, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979258036225, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-17', 2,
        '2024-03-17 07:00:00', '2024-03-17 08:00:00', 30, 62.10, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979371282434, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-17', 2,
        '2024-03-17 07:00:00', '2024-03-17 08:00:00', 30, 152.23, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979371282435, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-16', 2,
        '2024-03-16 12:00:00', '2024-03-16 13:00:05', 40, 188.70, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979396448257, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-16', 2,
        '2024-03-16 12:00:00', '2024-03-16 13:00:05', 40, 114.54, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979505500162, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-14', 2,
        '2024-03-14 12:00:00', '2024-03-14 13:00:05', 40, 77.52, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979505500163, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-14', 2,
        '2024-03-14 18:00:00', '2024-03-14 19:00:00', 30, 135.23, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979543248898, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-14', 2,
        '2024-03-14 12:00:00', '2024-03-14 13:00:05', 40, 17.38, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979627134977, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-16', 2,
        '2024-03-16 12:00:00', '2024-03-16 13:00:05', 30, 11.98, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979627134978, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-14', 2,
        '2024-03-14 18:00:00', '2024-03-14 19:00:00', 30, 148.98, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979643912193, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-16', 2,
        '2024-03-16 12:00:00', '2024-03-16 13:00:05', 30, 161.27, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979648106498, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-12', 2,
        '2024-03-12 18:00:00', '2024-03-12 19:00:00', 40, 136.93, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979744575489, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-12', 2,
        '2024-03-12 18:00:00', '2024-03-12 19:00:00', 40, 179.11, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979782324226, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-18', 2,
        '2024-03-18 07:00:00', '2024-03-18 08:00:00', 30, 75.50, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979853627394, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-17', 2,
        '2024-03-17 12:00:00', '2024-03-17 13:00:05', 40, 95.24, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979866210306, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-18', 2,
        '2024-03-18 07:00:00', '2024-03-18 08:00:00', 30, 51.67, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979916541953, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-17', 2,
        '2024-03-17 12:00:00', '2024-03-17 13:00:05', 40, 44.93, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491979979456514, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-15', 2,
        '2024-03-15 18:00:00', '2024-03-15 19:00:00', 30, 119.30, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980000428033, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-15', 2,
        '2024-03-15 12:00:00', '2024-03-15 13:00:05', 40, 84.02, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980084314113, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-15', 2,
        '2024-03-15 12:00:00', '2024-03-15 13:00:05', 40, 72.34, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980088508418, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-15', 2,
        '2024-03-15 18:00:00', '2024-03-15 19:00:00', 30, 29.99, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980096897025, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-17', 2,
        '2024-03-17 12:00:00', '2024-03-17 13:00:05', 30, 191.56, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980143034370, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-17', 2,
        '2024-03-17 12:00:00', '2024-03-17 13:00:05', 30, 36.98, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980243697665, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-13', 2,
        '2024-03-13 18:00:00', '2024-03-13 19:00:00', 40, 31.50, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980310806530, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-13', 2,
        '2024-03-13 18:00:00', '2024-03-13 19:00:00', 40, 28.30, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980319195137, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-19', 2,
        '2024-03-19 07:00:00', '2024-03-19 08:00:00', 30, 73.13, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980398886914, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-19', 2,
        '2024-03-19 07:00:00', '2024-03-19 08:00:00', 30, 76.66, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980398886915, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-18', 2,
        '2024-03-18 12:00:00', '2024-03-18 13:00:05', 40, 40.15, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980461801474, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-18', 2,
        '2024-03-18 12:00:00', '2024-03-18 13:00:05', 40, 176.78, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980520521729, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-16', 2,
        '2024-03-16 12:00:00', '2024-03-16 13:00:05', 40, 133.71, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980549881857, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-16', 2,
        '2024-03-16 18:00:00', '2024-03-16 19:00:00', 30, 115.64, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980549881858, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-16', 2,
        '2024-03-16 12:00:00', '2024-03-16 13:00:05', 40, 31.08, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980575047681, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-16', 2,
        '2024-03-16 18:00:00', '2024-03-16 19:00:00', 30, 138.68, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980616990722, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-18', 2,
        '2024-03-18 12:00:00', '2024-03-18 13:00:05', 30, 187.06, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980667322370, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-18', 2,
        '2024-03-18 12:00:00', '2024-03-18 13:00:05', 30, 158.47, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980767985666, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-20', 2,
        '2024-03-20 07:00:00', '2024-03-20 08:00:00', 30, 81.31, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980797345794, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-14', 2,
        '2024-03-14 18:00:00', '2024-03-14 19:00:00', 40, 178.54, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980830900225, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-20', 2,
        '2024-03-20 07:00:00', '2024-03-20 08:00:00', 30, 97.47, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980923174914, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-19', 2,
        '2024-03-19 12:00:00', '2024-03-19 13:00:05', 40, 55.84, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980923174915, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-14', 2,
        '2024-03-14 18:00:00', '2024-03-14 19:00:00', 40, 87.39, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491980994478082, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-17', 2,
        '2024-03-17 18:00:00', '2024-03-17 19:00:00', 30, 95.63, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981002866689, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-19', 2,
        '2024-03-19 12:00:00', '2024-03-19 13:00:05', 40, 128.79, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981019643906, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-17', 2,
        '2024-03-17 12:00:00', '2024-03-17 13:00:05', 40, 90.49, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981049004033, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-17', 2,
        '2024-03-17 12:00:00', '2024-03-17 13:00:05', 40, 187.16, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981078364161, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-17', 2,
        '2024-03-17 18:00:00', '2024-03-17 19:00:00', 30, 178.22, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981137084418, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-19', 2,
        '2024-03-19 12:00:00', '2024-03-19 13:00:05', 30, 129.53, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981137084419, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-19', 2,
        '2024-03-19 12:00:00', '2024-03-19 13:00:05', 30, 31.94, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981229359105, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-21', 2,
        '2024-03-21 07:00:00', '2024-03-21 08:00:00', 30, 193.66, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981380354049, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-15', 2,
        '2024-03-15 18:00:00', '2024-03-15 19:00:00', 40, 196.74, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981409714178, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-21', 2,
        '2024-03-21 07:00:00', '2024-03-21 08:00:00', 30, 23.80, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981409714179, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-20', 2,
        '2024-03-20 12:00:00', '2024-03-20 13:00:05', 40, 21.13, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981434880002, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-15', 2,
        '2024-03-15 18:00:00', '2024-03-15 19:00:00', 40, 55.94, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981481017345, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-18', 2,
        '2024-03-18 12:00:00', '2024-03-18 13:00:05', 40, 10.28, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981531348993, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-18', 2,
        '2024-03-18 12:00:00', '2024-03-18 13:00:05', 40, 48.82, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981543931905, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-18', 2,
        '2024-03-18 18:00:00', '2024-03-18 19:00:00', 30, 82.50, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981564903426, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-18', 2,
        '2024-03-18 18:00:00', '2024-03-18 19:00:00', 30, 145.82, '2024-03-09 23:51:22', '2024-03-09 23:51:22', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981627817986, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-20', 2,
        '2024-03-20 12:00:00', '2024-03-20 13:00:05', 30, 32.84, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981640400898, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-22', 2,
        '2024-03-22 07:00:00', '2024-03-22 08:00:00', 30, 16.26, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981644595201, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-20', 2,
        '2024-03-20 12:00:00', '2024-03-20 13:00:05', 40, 183.36, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981753647105, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-20', 2,
        '2024-03-20 12:00:00', '2024-03-20 13:00:05', 30, 26.96, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981904642050, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-21', 2,
        '2024-03-21 12:00:00', '2024-03-21 13:00:05', 40, 61.41, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981904642051, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-16', 2,
        '2024-03-16 18:00:00', '2024-03-16 19:00:00', 40, 115.07, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981946585089, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-22', 2,
        '2024-03-22 07:00:00', '2024-03-22 08:00:00', 30, 55.18, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981988528129, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-19', 2,
        '2024-03-19 12:00:00', '2024-03-19 13:00:05', 40, 68.12, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491981992722434, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-19', 2,
        '2024-03-19 18:00:00', '2024-03-19 19:00:00', 30, 11.84, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982047248385, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-19', 2,
        '2024-03-19 18:00:00', '2024-03-19 19:00:00', 30, 116.38, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982047248386, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-19', 2,
        '2024-03-19 12:00:00', '2024-03-19 13:00:05', 40, 151.76, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982101774337, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-16', 2,
        '2024-03-16 18:00:00', '2024-03-16 19:00:00', 40, 146.75, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982110162945, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-23', 2,
        '2024-03-23 07:00:00', '2024-03-23 08:00:00', 30, 26.95, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982139523074, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-21', 2,
        '2024-03-21 12:00:00', '2024-03-21 13:00:05', 30, 36.57, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982189854722, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-21', 2,
        '2024-03-21 12:00:00', '2024-03-21 13:00:05', 40, 108.21, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982336655361, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-21', 2,
        '2024-03-21 12:00:00', '2024-03-21 13:00:05', 30, 75.62, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982399569922, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-17', 2,
        '2024-03-17 18:00:00', '2024-03-17 19:00:00', 40, 176.12, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982412152833, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-23', 2,
        '2024-03-23 07:00:00', '2024-03-23 08:00:00', 30, 145.92, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982458290178, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-20', 2,
        '2024-03-20 18:00:00', '2024-03-20 19:00:00', 30, 148.82, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982491844609, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-22', 2,
        '2024-03-22 12:00:00', '2024-03-22 13:00:05', 40, 62.14, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982491844610, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-20', 2,
        '2024-03-20 12:00:00', '2024-03-20 13:00:05', 40, 67.65, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982567342082, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-20', 2,
        '2024-03-20 12:00:00', '2024-03-20 13:00:05', 40, 93.26, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982571536385, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-24', 2,
        '2024-03-24 07:00:00', '2024-03-24 08:00:00', 30, 98.28, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982617673729, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-17', 2,
        '2024-03-17 18:00:00', '2024-03-17 19:00:00', 40, 72.74, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982617673730, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-20', 2,
        '2024-03-20 18:00:00', '2024-03-20 19:00:00', 30, 92.47, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982722531330, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-22', 2,
        '2024-03-22 12:00:00', '2024-03-22 13:00:05', 30, 198.65, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982777057282, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-22', 2,
        '2024-03-22 12:00:00', '2024-03-22 13:00:05', 40, 30.85, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982869331969, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-18', 2,
        '2024-03-18 18:00:00', '2024-03-18 19:00:00', 40, 56.97, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982877720578, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-22', 2,
        '2024-03-22 12:00:00', '2024-03-22 13:00:05', 30, 18.41, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982911275009, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-21', 2,
        '2024-03-21 12:00:00', '2024-03-21 13:00:05', 40, 178.11, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982923857921, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-23', 2,
        '2024-03-23 12:00:00', '2024-03-23 13:00:05', 40, 39.05, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491982940635138, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-21', 2,
        '2024-03-21 18:00:00', '2024-03-21 19:00:00', 30, 19.62, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983028715522, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-25', 2,
        '2024-03-25 07:00:00', '2024-03-25 08:00:00', 30, 163.17, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983070658561, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-21', 2,
        '2024-03-21 12:00:00', '2024-03-21 13:00:05', 40, 187.70, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983112601602, 1765263539561332738, 1765262891990155265, 1765262495926222849, '2024-03-25', 2,
        '2024-03-25 07:00:00', '2024-03-25 08:00:00', 30, 77.40, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983179710466, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-21', 2,
        '2024-03-21 18:00:00', '2024-03-21 19:00:00', 30, 50.77, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983280373761, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-19', 2,
        '2024-03-19 18:00:00', '2024-03-19 19:00:00', 40, 170.26, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983292956673, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-22', 2,
        '2024-03-22 12:00:00', '2024-03-22 13:00:05', 40, 147.12, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983334899714, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-23', 2,
        '2024-03-23 12:00:00', '2024-03-23 13:00:05', 30, 56.61, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983410397186, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-19', 2,
        '2024-03-19 18:00:00', '2024-03-19 19:00:00', 40, 76.64, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983410397187, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-23', 2,
        '2024-03-23 12:00:00', '2024-03-23 13:00:05', 30, 74.87, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983435563010, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-24', 2,
        '2024-03-24 12:00:00', '2024-03-24 13:00:05', 40, 76.24, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983523643393, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-24', 2,
        '2024-03-24 12:00:00', '2024-03-24 13:00:05', 40, 139.35, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983532032001, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-22', 2,
        '2024-03-22 18:00:00', '2024-03-22 19:00:00', 30, 137.47, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983557197826, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-22', 2,
        '2024-03-22 12:00:00', '2024-03-22 13:00:05', 40, 158.91, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983716581377, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-22', 2,
        '2024-03-22 18:00:00', '2024-03-22 19:00:00', 30, 163.98, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983762718722, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-24', 2,
        '2024-03-24 12:00:00', '2024-03-24 13:00:05', 30, 104.70, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983804661761, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-20', 2,
        '2024-03-20 18:00:00', '2024-03-20 19:00:00', 40, 60.87, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983804661762, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-23', 2,
        '2024-03-23 12:00:00', '2024-03-23 13:00:05', 40, 21.58, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983829827586, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-24', 2,
        '2024-03-24 12:00:00', '2024-03-24 13:00:05', 30, 117.97, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983854993409, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-20', 2,
        '2024-03-20 18:00:00', '2024-03-20 19:00:00', 40, 177.26, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983875964930, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-25', 2,
        '2024-03-25 12:00:00', '2024-03-25 13:00:05', 40, 75.87, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983905325058, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-23', 2,
        '2024-03-23 18:00:00', '2024-03-23 19:00:00', 30, 42.42, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983947268097, 1765268806080430082, 1765262996428324866, 1765262528792788993, '2024-03-25', 2,
        '2024-03-25 12:00:00', '2024-03-25 13:00:05', 40, 54.55, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491983985016834, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-23', 2,
        '2024-03-23 12:00:00', '2024-03-23 13:00:05', 40, 25.25, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491984186343426, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-25', 2,
        '2024-03-25 12:00:00', '2024-03-25 13:00:05', 30, 117.34, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491984240869378, 1765263566656536577, 1765262996428324866, 1765262528792788993, '2024-03-25', 2,
        '2024-03-25 12:00:00', '2024-03-25 13:00:05', 30, 18.64, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491984257646593, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-24', 2,
        '2024-03-24 12:00:00', '2024-03-24 13:00:05', 40, 81.92, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491984257646594, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-21', 2,
        '2024-03-21 18:00:00', '2024-03-21 19:00:00', 40, 81.03, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491984287006721, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-24', 2,
        '2024-03-24 18:00:00', '2024-03-24 19:00:00', 30, 91.63, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491984291201026, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-21', 2,
        '2024-03-21 18:00:00', '2024-03-21 19:00:00', 40, 27.39, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491984333144066, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-24', 2,
        '2024-03-24 18:00:00', '2024-03-24 19:00:00', 30, 20.07, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491984408641538, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-24', 2,
        '2024-03-24 12:00:00', '2024-03-24 13:00:05', 40, 39.56, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491984668688386, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-25', 2,
        '2024-03-25 18:00:00', '2024-03-25 19:00:00', 30, 92.61, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491984727408641, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-22', 2,
        '2024-03-22 18:00:00', '2024-03-22 19:00:00', 40, 10.32, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491984727408642, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-22', 2,
        '2024-03-22 18:00:00', '2024-03-22 19:00:00', 40, 178.77, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491984727408643, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-25', 2,
        '2024-03-25 12:00:00', '2024-03-25 13:00:05', 40, 131.61, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491984798711809, 1765263622000377858, 1765263153815388161, 1765262432873250817, '2024-03-25', 2,
        '2024-03-25 18:00:00', '2024-03-25 19:00:00', 30, 150.92, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491984853237761, 1765268884211924993, 1765262996428324866, 1765262528792788993, '2024-03-25', 2,
        '2024-03-25 12:00:00', '2024-03-25 13:00:05', 40, 36.12, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491985172004866, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-23', 2,
        '2024-03-23 18:00:00', '2024-03-23 19:00:00', 40, 172.53, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491985176199170, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-23', 2,
        '2024-03-23 18:00:00', '2024-03-23 19:00:00', 40, 31.17, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491985616601090, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-24', 2,
        '2024-03-24 18:00:00', '2024-03-24 19:00:00', 40, 123.69, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491985650155521, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-24', 2,
        '2024-03-24 18:00:00', '2024-03-24 19:00:00', 40, 139.63, '2024-03-09 23:51:23', '2024-03-09 23:51:23', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491986048614402, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-25', 2,
        '2024-03-25 18:00:00', '2024-03-25 19:00:00', 40, 157.20, '2024-03-09 23:51:24', '2024-03-09 23:51:24', 0);
INSERT INTO `remaining_tickets` (`id`, `carriage_id`, `train_id`, `departure_station_id`,
                                 `first_station_departure_date`, `sequence`, `departure_datetime`, `arrival_datetime`,
                                 `remaining_tickets`, `price`, `create_time`, `update_time`, `del_flag`)
VALUES (1766491986115723265, 1765268946048548866, 1765263153815388161, 1765262432873250817, '2024-03-25', 2,
        '2024-03-25 18:00:00', '2024-03-25 19:00:00', 40, 141.31, '2024-03-09 23:51:24', '2024-03-09 23:51:24', 0);



DROP TABLE IF EXISTS `station`;
CREATE TABLE `station`
(
    `id`          bigint unsigned NOT NULL COMMENT 'ID',
    `code`        varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  DEFAULT NULL COMMENT '车站编号',
    `name`        varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '车站名称',
    `name_pinyin` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  DEFAULT NULL COMMENT '拼音',
    `region_id`   bigint unsigned                                               DEFAULT NULL COMMENT '车站地区',
    `create_time` datetime                                                      DEFAULT (now()) COMMENT '创建时间',
    `update_time` datetime                                                      DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`    tinyint(1)                                                    DEFAULT '0' COMMENT '删除标识',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT ='车站表';



INSERT INTO `station` (`id`, `code`, `name`, `name_pinyin`, `region_id`, `create_time`, `update_time`, `del_flag`)
VALUES (1765262350547451906, 'AA', '西安北站', 'xianbeizhan', 1, '2024-03-06 14:25:16', '2024-03-06 14:25:16', 0);
INSERT INTO `station` (`id`, `code`, `name`, `name_pinyin`, `region_id`, `create_time`, `update_time`, `del_flag`)
VALUES (1765262396345057282, '232', '西安南站', 'xiannanzhan', 1, '2024-03-06 14:25:27', '2024-03-06 14:25:27', 0);
INSERT INTO `station` (`id`, `code`, `name`, `name_pinyin`, `region_id`, `create_time`, `update_time`, `del_flag`)
VALUES (1765262432873250817, '42', '北京西站', 'beijingxizhan', 4, '2024-03-06 14:25:35', '2024-03-06 14:25:35', 0);
INSERT INTO `station` (`id`, `code`, `name`, `name_pinyin`, `region_id`, `create_time`, `update_time`, `del_flag`)
VALUES (1765262464259227649, '634', '北京东站', 'beijingdongzhan', 4, '2024-03-06 14:25:43', '2024-03-06 14:25:43', 0);
INSERT INTO `station` (`id`, `code`, `name`, `name_pinyin`, `region_id`, `create_time`, `update_time`, `del_flag`)
VALUES (1765262495926222849, '46', '深圳北站', 'shenzhenbeizhan', 2, '2024-03-06 14:25:50', '2024-03-06 14:25:50', 0);
INSERT INTO `station` (`id`, `code`, `name`, `name_pinyin`, `region_id`, `create_time`, `update_time`, `del_flag`)
VALUES (1765262528792788993, '342', '新疆站', 'xinjiangzhan', 7, '2024-03-06 14:25:58', '2024-03-06 14:25:58', 0);



DROP TABLE IF EXISTS `train`;
CREATE TABLE `train`
(
    `id`                   bigint unsigned NOT NULL COMMENT 'ID',
    `train_number`         varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci         DEFAULT NULL COMMENT '列车车次',
    `train_type`           enum ('0','1','2','3','4','5','6','7','8') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '0 高速列车 (High-Speed Train) | 1 城际列车 (Intercity Train) | 2 动车组 (Multiple Unit Train) | 3 直达列车 (Through Train) | 4 特快列车 (Express Train) | 5 快速列车 (Rapid Train) | 6 普通列车 (Ordinary Train) | 7 货物列车 (Freight Train) | 8 旅游列车 (Tourist Train)',
    `departure_station_id` bigint unsigned                                                       DEFAULT NULL COMMENT '起始站ID',
    `arrival_station_id`   bigint unsigned                                                       DEFAULT NULL COMMENT '终点站ID',
    `departure_time`       time                                                                  DEFAULT NULL COMMENT '始发时间',
    `arrival_time`         time                                                                  DEFAULT NULL COMMENT '到达终点站时间',
    `day_crossed`          tinyint unsigned                                                      DEFAULT '0' COMMENT '行程所跨天数',
    `create_time`          datetime        NOT NULL                                              DEFAULT (now()) COMMENT '创建时间',
    `update_time`          datetime        NOT NULL                                              DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `available_flag`       tinyint(1)                                                            DEFAULT NULL COMMENT '列车可用标识',
    `del_flag`             tinyint(1)                                                            DEFAULT '0' COMMENT '删除标识',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT ='列车信息表';



INSERT INTO `train` (`id`, `train_number`, `train_type`, `departure_station_id`, `arrival_station_id`, `departure_time`,
                     `arrival_time`, `day_crossed`, `create_time`, `update_time`, `available_flag`, `del_flag`)
VALUES (1765262891990155265, 'G1', '0', 1765262350547451906, 1765262432873250817, '06:05:00', '08:00:00', 0,
        '2024-03-06 14:27:25', '2024-03-06 14:27:25', 1, 0);
INSERT INTO `train` (`id`, `train_number`, `train_type`, `departure_station_id`, `arrival_station_id`, `departure_time`,
                     `arrival_time`, `day_crossed`, `create_time`, `update_time`, `available_flag`, `del_flag`)
VALUES (1765262996428324866, 'G2', '0', 1765262396345057282, 1765262464259227649, '11:00:00', '13:00:05', 0,
        '2024-03-06 14:27:50', '2024-03-06 14:27:50', 1, 0);
INSERT INTO `train` (`id`, `train_number`, `train_type`, `departure_station_id`, `arrival_station_id`, `departure_time`,
                     `arrival_time`, `day_crossed`, `create_time`, `update_time`, `available_flag`, `del_flag`)
VALUES (1765263062421504002, 'G3', '0', 1765262432873250817, 1765262396345057282, '15:00:00', '17:00:00', 0,
        '2024-03-06 14:28:05', '2024-03-06 14:28:05', 1, 0);
INSERT INTO `train` (`id`, `train_number`, `train_type`, `departure_station_id`, `arrival_station_id`, `departure_time`,
                     `arrival_time`, `day_crossed`, `create_time`, `update_time`, `available_flag`, `del_flag`)
VALUES (1765263153815388161, 'G4', '0', 1765262350547451906, 1765262528792788993, '17:00:00', '19:00:00', 0,
        '2024-03-06 14:28:27', '2024-03-06 14:28:27', 1, 0);
INSERT INTO `train` (`id`, `train_number`, `train_type`, `departure_station_id`, `arrival_station_id`, `departure_time`,
                     `arrival_time`, `day_crossed`, `create_time`, `update_time`, `available_flag`, `del_flag`)
VALUES (1766476032442994690, 'G5', '0', 1765262528792788993, 1765262495926222849, '01:00:00', '04:00:00', 1,
        '2024-03-09 22:48:00', '2024-03-09 22:48:00', 1, 0);



DROP TABLE IF EXISTS `train_station_arrival`;
CREATE TABLE `train_station_arrival`
(
    `id`                     bigint unsigned NOT NULL COMMENT 'ID',
    `train_id`               bigint unsigned  DEFAULT NULL COMMENT '车次ID',
    `station_id`             bigint unsigned  DEFAULT NULL COMMENT '车站ID',
    `arrival_time`           time             DEFAULT NULL COMMENT '到站时间',
    `sequence`               tinyint unsigned DEFAULT NULL,
    `kth_from_departure_day` tinyint unsigned DEFAULT '1' COMMENT '到达时间是从出发起的第几天',
    `stopover_time`          tinyint unsigned DEFAULT NULL COMMENT '停留时间，单位分',
    `create_time`            datetime         DEFAULT (now()) COMMENT '创建时间',
    `update_time`            datetime         DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`               tinyint(1)       DEFAULT '0' COMMENT '删除标识',
    PRIMARY KEY (`id`),
    KEY `idx_train_id` (`train_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT ='列车到站表';



INSERT INTO `train_station_arrival` (`id`, `train_id`, `station_id`, `arrival_time`, `sequence`,
                                     `kth_from_departure_day`, `stopover_time`, `create_time`, `update_time`,
                                     `del_flag`)
VALUES (1765262073534644226, 1765262073270403074, 1, '03:00:00', 1, 1, 0, '2024-03-06 14:24:10', '2024-03-06 14:24:10',
        0);
INSERT INTO `train_station_arrival` (`id`, `train_id`, `station_id`, `arrival_time`, `sequence`,
                                     `kth_from_departure_day`, `stopover_time`, `create_time`, `update_time`,
                                     `del_flag`)
VALUES (1765262073773719554, 1765262073270403074, 2, '09:00:00', 2, 1, 0, '2024-03-06 14:24:10', '2024-03-06 14:24:10',
        0);
INSERT INTO `train_station_arrival` (`id`, `train_id`, `station_id`, `arrival_time`, `sequence`,
                                     `kth_from_departure_day`, `stopover_time`, `create_time`, `update_time`,
                                     `del_flag`)
VALUES (1765262892241813506, 1765262891990155265, 1765262350547451906, '06:05:00', 1, 1, 0, '2024-03-06 14:27:25',
        '2024-03-06 14:27:25', 0);
INSERT INTO `train_station_arrival` (`id`, `train_id`, `station_id`, `arrival_time`, `sequence`,
                                     `kth_from_departure_day`, `stopover_time`, `create_time`, `update_time`,
                                     `del_flag`)
VALUES (1765262892489277441, 1765262891990155265, 1765262432873250817, '08:00:00', 3, 1, 0, '2024-03-06 14:27:25',
        '2024-03-06 14:27:25', 0);
INSERT INTO `train_station_arrival` (`id`, `train_id`, `station_id`, `arrival_time`, `sequence`,
                                     `kth_from_departure_day`, `stopover_time`, `create_time`, `update_time`,
                                     `del_flag`)
VALUES (1765262996671594497, 1765262996428324866, 1765262396345057282, '11:00:00', 1, 1, 0, '2024-03-06 14:27:50',
        '2024-03-06 14:27:50', 0);
INSERT INTO `train_station_arrival` (`id`, `train_id`, `station_id`, `arrival_time`, `sequence`,
                                     `kth_from_departure_day`, `stopover_time`, `create_time`, `update_time`,
                                     `del_flag`)
VALUES (1765262996927447041, 1765262996428324866, 1765262464259227649, '13:00:05', 3, 1, 0, '2024-03-06 14:27:50',
        '2024-03-06 14:27:50', 0);
INSERT INTO `train_station_arrival` (`id`, `train_id`, `station_id`, `arrival_time`, `sequence`,
                                     `kth_from_departure_day`, `stopover_time`, `create_time`, `update_time`,
                                     `del_flag`)
VALUES (1765263062664773634, 1765263062421504002, 1765262432873250817, '15:00:00', 1, 1, 0, '2024-03-06 14:28:05',
        '2024-03-06 14:28:05', 0);
INSERT INTO `train_station_arrival` (`id`, `train_id`, `station_id`, `arrival_time`, `sequence`,
                                     `kth_from_departure_day`, `stopover_time`, `create_time`, `update_time`,
                                     `del_flag`)
VALUES (1765263062908043265, 1765263062421504002, 1765262396345057282, '17:00:00', 2, 1, 0, '2024-03-06 14:28:05',
        '2024-03-06 14:28:05', 0);
INSERT INTO `train_station_arrival` (`id`, `train_id`, `station_id`, `arrival_time`, `sequence`,
                                     `kth_from_departure_day`, `stopover_time`, `create_time`, `update_time`,
                                     `del_flag`)
VALUES (1765263154058657794, 1765263153815388161, 1765262350547451906, '17:00:00', 1, 1, 0, '2024-03-06 14:28:27',
        '2024-03-06 14:28:27', 0);
INSERT INTO `train_station_arrival` (`id`, `train_id`, `station_id`, `arrival_time`, `sequence`,
                                     `kth_from_departure_day`, `stopover_time`, `create_time`, `update_time`,
                                     `del_flag`)
VALUES (1765263154310316033, 1765263153815388161, 1765262528792788993, '19:00:00', 3, 1, 0, '2024-03-06 14:28:27',
        '2024-03-06 14:28:27', 0);
INSERT INTO `train_station_arrival` (`id`, `train_id`, `station_id`, `arrival_time`, `sequence`,
                                     `kth_from_departure_day`, `stopover_time`, `create_time`, `update_time`,
                                     `del_flag`)
VALUES (1765263229900062722, 1765262891990155265, 1765262495926222849, '07:00:00', 2, 1, 11, '2024-03-06 14:28:45',
        '2024-03-06 14:28:45', 0);
INSERT INTO `train_station_arrival` (`id`, `train_id`, `station_id`, `arrival_time`, `sequence`,
                                     `kth_from_departure_day`, `stopover_time`, `create_time`, `update_time`,
                                     `del_flag`)
VALUES (1765263335915290625, 1765262996428324866, 1765262528792788993, '12:00:00', 2, 1, 21, '2024-03-06 14:29:11',
        '2024-03-06 14:29:11', 0);
INSERT INTO `train_station_arrival` (`id`, `train_id`, `station_id`, `arrival_time`, `sequence`,
                                     `kth_from_departure_day`, `stopover_time`, `create_time`, `update_time`,
                                     `del_flag`)
VALUES (1765263424033423361, 1765263153815388161, 1765262432873250817, '18:00:00', 2, 1, 1, '2024-03-06 14:29:32',
        '2024-03-06 14:29:32', 0);
INSERT INTO `train_station_arrival` (`id`, `train_id`, `station_id`, `arrival_time`, `sequence`,
                                     `kth_from_departure_day`, `stopover_time`, `create_time`, `update_time`,
                                     `del_flag`)
VALUES (1766476032774344706, 1766476032442994690, 1765262528792788993, '01:00:00', 1, 1, 0, '2024-03-09 22:48:00',
        '2024-03-09 22:48:00', 0);
INSERT INTO `train_station_arrival` (`id`, `train_id`, `station_id`, `arrival_time`, `sequence`,
                                     `kth_from_departure_day`, `stopover_time`, `create_time`, `update_time`,
                                     `del_flag`)
VALUES (1766476033076334593, 1766476032442994690, 1765262495926222849, '04:00:00', 2, 2, 0, '2024-03-09 22:48:00',
        '2024-03-09 22:48:00', 0);



DROP TABLE IF EXISTS `train_station_price`;
CREATE TABLE `train_station_price`
(
    `id`                   bigint unsigned NOT NULL COMMENT 'ID',
    `train_id`             bigint unsigned DEFAULT NULL COMMENT '车次ID',
    `departure_station_id` bigint unsigned NOT NULL COMMENT '出发站点',
    `arrival_station_id`   bigint unsigned NOT NULL COMMENT '到达站点',
    `seat_id`              bigint unsigned NOT NULL COMMENT '座位ID',
    `price`                decimal(7, 2)   DEFAULT NULL COMMENT '车票价格',
    `create_time`          datetime        DEFAULT (now()) COMMENT '创建时间',
    `update_time`          datetime        DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`             tinyint(1)      DEFAULT '0' COMMENT '删除标识',
    PRIMARY KEY (`id`),
    KEY `idx_train_id` (`train_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT ='列车站点价格表';



DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`
(
    `id`          bigint NOT NULL COMMENT 'id',
    `nickname`    varchar(32)  DEFAULT NULL COMMENT '昵称',
    `avatar`      varchar(255) DEFAULT NULL COMMENT '头像链接',
    `email`       varchar(20)  DEFAULT NULL,
    `region`      varchar(20)  DEFAULT NULL,
    `address`     varchar(32)  DEFAULT NULL,
    `phone`       varchar(11)  DEFAULT NULL COMMENT '手机号',
    `password`    varchar(32)  DEFAULT NULL,
    `create_time` datetime     DEFAULT (now()),
    `update_time` datetime     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `del_flag`    tinyint(1)   DEFAULT '0',
    PRIMARY KEY (`id`),
    UNIQUE KEY `phone_unique` (`phone`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci COMMENT ='会员';



INSERT INTO `user` (`id`, `nickname`, `avatar`, `email`, `region`, `address`, `phone`, `password`, `create_time`,
                    `update_time`, `del_flag`)
VALUES (1234, 'nickname--1887848455', 'avatar--1193557984', '123124@qq.com', 'region-655501102', 'address-1686519579',
        '1231245', 'password-1237042488', '2024-02-19 12:38:59', '2024-02-19 12:38:59', 0);
INSERT INTO `user` (`id`, `nickname`, `avatar`, `email`, `region`, `address`, `phone`, `password`, `create_time`,
                    `update_time`, `del_flag`)
VALUES (1235234, 'nickname-837532622', 'avatar-54125712', '123124@qq.com', 'region-1779581316', 'address-1893273085',
        '12315445', 'password--1408682220381', '2024-02-19 12:46:11', '2024-02-19 12:46:34', 0);
INSERT INTO `user` (`id`, `nickname`, `avatar`, `email`, `region`, `address`, `phone`, `password`, `create_time`,
                    `update_time`, `del_flag`)
VALUES (1755628684603072514, '乘客4166',
        'https://common-1303914643.cos.ap-guangzhou.myqcloud.com/train/passenger/2024-02-13/c67f814d-5d13-4667-88c3-21dc6083dcff-1757271694252396544.png',
        '6122323222@qq.com', '123222332222', '123', '1736', '123', '2024-02-09 00:24:31', '2024-02-12 19:14:28', 0);
INSERT INTO `user` (`id`, `nickname`, `avatar`, `email`, `region`, `address`, `phone`, `password`, `create_time`,
                    `update_time`, `del_flag`)
VALUES (1757272649425256449, '乘客2755',
        'https://common-1303914643.cos.ap-guangzhou.myqcloud.com/train/passenger/2024-02-13/654fcd74-2169-4981-8501-109e944c70db-1757274202047729664.png',
        '1232322@qq.com', '12322', '123', '17364612480', '123', '2024-02-12 19:17:03', '2024-03-04 12:51:37', 0);
INSERT INTO `user` (`id`, `nickname`, `avatar`, `email`, `region`, `address`, `phone`, `password`, `create_time`,
                    `update_time`, `del_flag`)
VALUES (1757274446541914113, '乘客3526312',
        'https://common-1303914643.cos.ap-guangzhou.myqcloud.com/train/passenger/2024-02-13/195c3e19-8897-43b4-9340-fd211afd6e97-1757277869928828928.png',
        '126322@qq.com', '1231221', '33123122', '1736462', '123', '2024-02-12 19:24:11', '2024-02-12 19:48:37', 0);



DROP TABLE IF EXISTS `user_pay`;
CREATE TABLE `user_pay`
(
    `id`               bigint unsigned NOT NULL COMMENT 'ID',
    `order_sn`         varchar(64)    DEFAULT NULL COMMENT '订单号',
    `out_order_sn`     varchar(64)    DEFAULT NULL COMMENT '商户订单号',
    `channel`          varchar(64)    DEFAULT NULL COMMENT '支付渠道',
    `trade_type`       varchar(64)    DEFAULT NULL COMMENT '支付环境',
    `subject`          varchar(512)   DEFAULT NULL COMMENT '订单标题',
    `order_request_id` varchar(64)    DEFAULT NULL COMMENT '商户订单号',
    `total_amount`     decimal(10, 0) DEFAULT NULL COMMENT '交易总金额',
    `trade_no`         varchar(256)   DEFAULT NULL COMMENT '三方交易凭证号',
    `gmt_payment`      datetime       DEFAULT NULL COMMENT '付款时间',
    `pay_amount`       decimal(10, 0) DEFAULT NULL COMMENT '支付金额',
    `status`           varchar(32)    DEFAULT NULL COMMENT '支付状态',
    `create_time`      datetime       DEFAULT (now()) COMMENT '创建时间',
    `update_time`      datetime       DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`         tinyint(1)     DEFAULT '0' COMMENT '删除标记 0：未删除 1：删除',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `idx_order_sn` (`order_sn`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci COMMENT ='支付表';



DROP TABLE IF EXISTS `user_refund`;
CREATE TABLE `user_refund`
(
    `id`             bigint unsigned NOT NULL COMMENT 'ID',
    `pay_sn`         varchar(64)                        DEFAULT NULL COMMENT '支付流水号',
    `order_sn`       varchar(64)                        DEFAULT NULL COMMENT '订单号',
    `trade_no`       varchar(64)                        DEFAULT NULL COMMENT '三方交易凭证号',
    `amount`         decimal(10, 0)                     DEFAULT NULL COMMENT '退款金额',
    `user_id`        bigint unsigned                    DEFAULT NULL COMMENT '用户ID',
    `username`       varchar(256)                       DEFAULT NULL COMMENT '用户名',
    `train_id`       bigint unsigned                    DEFAULT NULL COMMENT '列车ID',
    `train_number`   varchar(256)                       DEFAULT NULL COMMENT '列车车次',
    `riding_date`    date                               DEFAULT NULL COMMENT '乘车日期',
    `departure`      varchar(64)                        DEFAULT NULL COMMENT '出发站点',
    `arrival`        varchar(64)                        DEFAULT NULL COMMENT '到达站点',
    `departure_time` datetime                           DEFAULT NULL COMMENT '出发时间',
    `arrival_time`   datetime                           DEFAULT NULL COMMENT '到达时间',
    `seat_type`      enum ('0','1','2','3','4','5','6') DEFAULT NULL COMMENT '座位类型 0无座 1硬座 2硬卧 3软卧 4二等座 5一等座 6商务座',
    `id_type`        int                                DEFAULT NULL COMMENT '证件类型',
    `id_card`        varchar(256)                       DEFAULT NULL COMMENT '证件号',
    `real_name`      varchar(256)                       DEFAULT NULL COMMENT '真实姓名',
    `status`         int                                DEFAULT NULL COMMENT '订单状态',
    `refund_time`    datetime                           DEFAULT NULL COMMENT '退款时间',
    `create_time`    datetime                           DEFAULT (now()) COMMENT '创建时间',
    `update_time`    datetime                           DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`       tinyint(1)                         DEFAULT NULL COMMENT '删除标记 0：未删除 1：删除',
    PRIMARY KEY (`id`),
    KEY `idx_user_id` (`user_id`) USING BTREE,
    KEY `idx_order_sn` (`order_sn`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci COMMENT ='退款记录表';

SET FOREIGN_KEY_CHECKS = 1;

delete
from remaining_tickets
where del_flag = 1;
