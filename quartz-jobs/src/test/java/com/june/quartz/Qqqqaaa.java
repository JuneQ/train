package com.june.quartz;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@SpringBootTest
public class Qqqqaaa {
    @Resource
    JdbcTemplate jdbcTemplate;

    @Test
    void contextLoads() throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new ClassPathResource("daily.sql").getInputStream()))) {
            String line;
            StringBuilder sqlBuilder = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                line = line.trim();
                if (!line.startsWith("--") && !line.isEmpty()) { // 忽略注释和空行
                    sqlBuilder.append(line).append(" ");
                    if (line.endsWith(";")) { // SQL语句通常以分号结束
                        jdbcTemplate.execute(sqlBuilder.toString());
                        sqlBuilder.setLength(0); // 清空缓冲区，准备下一个SQL语句
                    }
                }
            }
        }
    }
}
