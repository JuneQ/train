package com.june.ticket.config

import com.june.ticket.interceptor.LoginInterceptor
import jakarta.annotation.Resource
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
class InterceptorConfig : WebMvcConfigurer {
    @Resource
    lateinit var loginInterceptor: LoginInterceptor


    override fun addInterceptors(registry: InterceptorRegistry) {
        // 可添加多个
        registry.addInterceptor(loginInterceptor)
            .addPathPatterns("/**")
            .excludePathPatterns("/quartz/refresh-tickets/all")
            .excludePathPatterns("/quartz/tickets/{cycle}")
            .excludePathPatterns("/ticket/refresh-tickets/all")
            .excludePathPatterns("/seat/restore/mysql")
            .excludePathPatterns("/seat/restore/redis")
    }
}
