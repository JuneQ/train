package com.june.ticket.config

import com.alibaba.fastjson2.support.spring.data.redis.GenericFastJsonRedisSerializer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.connection.DefaultStringRedisConnection
import org.springframework.data.redis.connection.RedisConnectionFactory
import org.springframework.data.redis.connection.StringRedisConnection
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.serializer.GenericToStringSerializer


@Configuration
class RedisConfig {
    @Bean
    fun getStringRedisConnection(redisConnectionFactory: RedisConnectionFactory): StringRedisConnection {
        return DefaultStringRedisConnection(redisConnectionFactory.connection)
    }

    @Bean
    fun redisTemplate(redisConnectionFactory: RedisConnectionFactory?): RedisTemplate<Any, Any> {
        val redisTemplate = RedisTemplate<Any, Any>()

        redisTemplate.setConnectionFactory(redisConnectionFactory!!)

        // 使用 GenericFastJsonRedisSerializer 替换默认序列化
        val genericFastJsonRedisSerializer = GenericFastJsonRedisSerializer()

        // 设置key和value的序列化规则
        redisTemplate.keySerializer = GenericToStringSerializer(
            Any::class.java
        )

        redisTemplate.valueSerializer = genericFastJsonRedisSerializer


        // 设置hashKey和hashValue的序列化规则
        redisTemplate.hashKeySerializer = GenericToStringSerializer(
            Any::class.java
        )

        redisTemplate.hashValueSerializer = genericFastJsonRedisSerializer


        // 设置支持事物
        redisTemplate.afterPropertiesSet()

        return redisTemplate
    }
}
