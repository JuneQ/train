package com.june.ticket.controller

import com.june.common.dto.PlaceOrderDto
import com.june.common.entity.R
import com.june.ticket.pojo.dto.TicketDBView
import com.june.ticket.pojo.vo.BuyTicketReturn
import com.june.ticket.pojo.vo.BuyTicketVo
import com.june.ticket.pojo.vo.TicketSelectVo
import com.june.ticket.service.impl.RemainingTicketsServiceImpl
import io.swagger.v3.oas.annotations.media.Schema
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.time.LocalDate

@RestController
class TicketController(
    val remainingTicketsServiceImpl: RemainingTicketsServiceImpl,
) {
    @Schema(description = "查票")
    @GetMapping("/remaining-tickets/{startRegionId}/{endRegionId}/{departureDate}")
    fun searchRemainingTickets(
        @PathVariable("startRegionId") startRegionId: Long,
        @PathVariable("endRegionId") endRegionId: Long,
        @PathVariable("departureDate") departureDate: String,
    ): R<List<TicketDBView>> {
        return R.ok(
            remainingTicketsServiceImpl.searchRemainingTickets(
                TicketSelectVo(
                    LocalDate.parse(departureDate),
                    startRegionId,
                    endRegionId
                )
            )
        )
    }


    @Schema(description = "购票")
    @PostMapping("/remaining-tickets")
    fun buyTicket(@RequestBody @Validated vo: BuyTicketVo): R<BuyTicketReturn> {
        return R.ok(remainingTicketsServiceImpl.buyTicket(vo))
    }

    @Schema(description = "只针对Redis回滚座位")
    @PostMapping("/seat/restore/redis")
    fun restoreSeatForRedis(@RequestBody dto: PlaceOrderDto): R<Void> {
        remainingTicketsServiceImpl.rollbackSeatForRedis(dto)
        return R.ok()
    }

    @Schema(description = "只针对MySQL回滚座位")
    @PostMapping("/seat/restore/mysql")
    fun restoreSeatForMySQL(@RequestBody dto: PlaceOrderDto): R<Void> {
        remainingTicketsServiceImpl.rollbackSeatForMySQL(dto)
        return R.ok()
    }

    @Schema(defaultValue = "上票，参数为轮次")
    @PostMapping("/quartz/tickets/{cycle}")
    fun addTicket(@PathVariable("cycle") cycle: Int): R<Void> {
        remainingTicketsServiceImpl.addTickets(cycle)
        return R.ok()
    }

    @Schema(description = "删除所有车票，然后重新上票")
    @GetMapping("/quartz/refresh-tickets/all")
    fun initTickets(): R<Void> {
        remainingTicketsServiceImpl.initTickets()
        return R.ok()
    }
}


