package com.june.ticket.mqConsumer

import org.apache.rocketmq.common.message.MessageExt
import org.apache.rocketmq.spring.annotation.ConsumeMode
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener
import org.apache.rocketmq.spring.core.RocketMQListener
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component


@Component
@RocketMQMessageListener(
    topic = "TEST_TOPIC",
    selectorExpression = "thisisabc",
    consumerGroup = "TEST_GROUP",
    consumeMode = ConsumeMode.CONCURRENTLY
)
class SimpleConsumer(
    val log: Logger = LoggerFactory.getLogger(SimpleConsumer::class.java),
) : RocketMQListener<MessageExt> {

    override fun onMessage(message: MessageExt) {

        log.debug("!!!simple body:{}", String(message.body))
    }
}
