package com.june.ticket.feign;

import com.june.common.dto.SubStationDto;
import com.june.common.entity.R;
import com.june.ticket.pojo.dto.Carriage;
import com.june.ticket.pojo.dto.TrainStationArrival;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDateTime;
import java.util.List;

@FeignClient(name = "train", path = "/train", contextId = "ticket123894")
public interface TrainFeignClient {
    @GetMapping("/station-arrival/sequence/{stationId}/{trainId}")
    R<Byte> getStationArrivalSequence(@PathVariable("stationId") Long stationId, @PathVariable("trainId") Long trainId);

    @GetMapping("/station-arrival/arrival-datetime/{stationId}/{trainId}/{departureDate}")
    R<LocalDateTime> getTrainStationArrivalDateTime(@PathVariable("stationId") Long stationId,
                                                    @PathVariable("trainId") Long trainId,
                                                    @PathVariable("departureDate") String departureDate
    );

    @Schema(description = "后台上票调用的接口")
    @GetMapping("station-arrival/{current}/{size}/{trainId}")
    R<List<TrainStationArrival>> getStationArrivalPageFromSys(@PathVariable("current") Integer current,
                                                              @PathVariable("size") Integer size,
                                                              @PathVariable("trainId") Long trainId
    );

    @Operation(description = "查询所有carriage")
    @GetMapping("carriage/all")
    R<List<Carriage>> getCarriage();

    @GetMapping("station-crossed/{trainId}")
    @Operation(description = "查询列车所夸站数")
    R<Integer> getStationCrossed(@PathVariable("trainId") Long trainId);


    @Operation(description = "获取列车到站表，假设当前不存在任何正在运行的列车，以查询日期作为初始运行日期")
    @GetMapping("sub-station/{trainId}")
    R<List<SubStationDto>> getPastAndNearestSubStationIdAndSequenceAndTime(@PathVariable("trainId") Long trainId);
}
