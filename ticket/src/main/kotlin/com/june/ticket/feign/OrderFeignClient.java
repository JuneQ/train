package com.june.ticket.feign;

import com.june.common.dto.PlaceOrderDto;
import com.june.common.entity.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "order", path = "/order", contextId = "order123894")
public interface OrderFeignClient {

    @PostMapping("order/place-order")
    R<Void> placeOrder(@RequestBody PlaceOrderDto dto);
}
