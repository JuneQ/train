package com.june.ticket.pojo.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * <p>
 * 列车到站表
 * </p>
 *
 * @author June
 */
@Data
@TableName("train_station_arrival")
@Schema(name = "TrainStationArrival", description = "$!{table.comment}")
public class TrainStationArrival implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "ID")
    @TableId("id")
    private Long id;

    @Schema(description = "车次ID")
    @TableField("train_id")
    private Long trainId;

    @TableField(exist = false)
    private Train train;

    @Schema(description = "车站ID")
    @TableField("station_id")
    private Long stationId;

    @Schema(description = "站序：此到达站是自始发站的第几个站")
    @TableField("sequence")
    private Byte sequence;

    @TableField(exist = false)
    private Station station;

    @Schema(description = "到站时间")
    @TableField("arrival_time")
    private LocalTime arrivalTime;

    @Schema(description = "停留时间，单位分")
    @TableField("stopover_time")
    private Byte stopoverTime;

    @Schema(description = "此到达时间是自始发时间的第几天")
    @TableField("kth_from_departure_day")
    private Byte kthFromDepartureDay;

    @Schema(description = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @Schema(description = "修改时间")
    @TableField("update_time")
    private LocalDateTime updateTime;

    @Schema(description = "删除标识")
    @TableField("del_flag")
    private Boolean delFlag;
}
