package com.june.ticket.pojo.dao.entity

import com.baomidou.mybatisplus.annotation.TableField
import com.baomidou.mybatisplus.annotation.TableId
import io.swagger.v3.oas.annotations.media.Schema
import java.io.Serial
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * 余票信息表
 * @author June
 */
@Schema(name = "RemainingTickets", description = "$!{table.comment}")
data class RemainingTickets(
    @Schema(description = "ID")
    @TableId("id")
    var id: Long?,

    @Schema(description = "车厢号")
    @TableField("carriage_id")
    var carriageId: Long?,

    @Schema(description = "车次")
    @TableField("train_id")
    var trainId: Long?,

    @Schema(description = "始发站发车日期")
    @TableField("first_station_departure_date")
    var firstStationDepartureDate: LocalDate?,

    @Schema(description = "该站出发日时间")
    @TableField("departure_datetime")
    var departureDateTime: LocalDateTime?,


    @Schema(description = "到达下一站时间")
    @TableField("arrival_datetime")
    var arrivalDateTime: LocalDateTime?,

    @Schema(description = "仅保存出发站，因为默认下一站为此列车的行驶的紧挨的下一站")
    @TableField("departure_station_id")
    var startStationId: Long?,

    @Schema(description = "站序，始发站是1")
    @TableField("sequence")
    var sequence: Byte?,

    @Schema(description = "从此站到下一站的余票数")
    @TableField("remaining_tickets")
    var remainingTickets: Short?,

    @Schema(description = "从此站到下一站的余票数")
    @TableField("price")
    var price: BigDecimal?,


    @Schema(description = "创建时间")
    @TableField("create_time")
    var createTime: LocalDateTime? = null,

    @Schema(description = "修改时间")
    @TableField("update_time")
    var updateTime: LocalDateTime? = null,

    @Schema(description = "删除标识")
    @TableField("del_flag")
    var delFlag: Boolean? = null,
) : Serializable {
    companion object {
        @Serial
        private val serialVersionUID = 1L
    }
}
