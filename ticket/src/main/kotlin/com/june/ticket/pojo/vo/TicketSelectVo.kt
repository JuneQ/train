package com.june.ticket.pojo.vo

import org.jetbrains.annotations.NotNull
import java.time.LocalDate

data class TicketSelectVo(
    @NotNull
    val departureDate: LocalDate,
    @NotNull
    val startRegionId: Long,
    @NotNull
    val endRegionId: Long,
)
