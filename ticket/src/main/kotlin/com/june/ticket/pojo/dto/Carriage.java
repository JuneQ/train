package com.june.ticket.pojo.dto;

import com.june.common.enums.CarriageType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 车厢表
 * </p>
 *
 * @author June
 */
@Data
public class Carriage implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "ID")
    public Long id;

    @Schema(description = "列车ID")
    public Long trainId;

    @Schema(description = "车厢号")
    public String carriageNumber;

    @Schema(description = "车厢类型")
    public CarriageType carriageType;

    @Schema(description = "座位数")
    public Short seatCount;

    @Schema(description = "创建时间")
    public LocalDateTime createTime;

    @Schema(description = "修改时间")
    public LocalDateTime updateTime;

    @Schema(description = "删除标识")
    public Boolean delFlag;

    public Train train;

}
