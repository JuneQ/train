package com.june.ticket.pojo.vo;

import com.june.common.dto.Passenger;
import com.june.common.enums.CarriageType;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public record BuyTicketVo(
        @NotNull(message = "departureStationId 不能为null")
        Long departureStationId,

        @NotNull(message = "departureStationSequence 不能为null")
        Byte departureStationSequence,

        @NotNull(message = "arrivalStationId 不能为null")
        Long arrivalStationId,

        @NotNull(message = "arrivalStationName 不能为null")
        String arrivalStationName,


        @NotNull(message = "departureStationName 不能为null")
        String departureStationName,
        @NotNull(message = "arrivalStationSequence 不能为null")
        Byte arrivalStationSequence,

        @NotNull(message = "trainId 不能为null")
        Long trainId,

        @NotNull(message = "carriageType 不能为null")
        CarriageType carriageType,

        @NotNull(message = "carriageId 不能为null")
        Long carriageId,

        @NotNull(message = "passengerIds 不能为null")
        @Size(min = 1, max = 5, message = "至少添加一个乘客")
        List<Passenger> passengers,

        @NotNull(message = "departureDatetime 不能为null")
        LocalDateTime departureDatetime,

        @NotNull(message = "firstStationDepartureDate 不能为null")
        LocalDate firstStationDepartureDate,

        @NotNull(message = "price 不能为null")
        @Schema(description = "每张票的售价")
        BigDecimal price,


        @NotNull(message = "trainNumber 不能为null")
        String trainNumber,

        @NotNull(message = "carriageNumber 不能为null")
        String carriageNumber

) {
}
