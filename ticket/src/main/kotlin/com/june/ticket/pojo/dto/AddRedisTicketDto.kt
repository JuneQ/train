package com.june.ticket.pojo.dto

data class AddRedisTicketDto(
    val trainId: Long,
    val carriageTypeOrdinal: Byte,
    val carriageId: Long,
)
