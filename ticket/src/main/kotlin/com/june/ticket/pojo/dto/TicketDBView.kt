package com.june.ticket.pojo.dto

import com.june.common.enums.CarriageType
import com.june.common.enums.TrainType
import java.io.Serial
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime

data class TicketDBView(
    var tickets: Int? = 0,
    val trainId: Long?,
    val departureDatetime: LocalDateTime? = null,
    val arrivalDatetime: LocalDateTime? = null,
    val departureStationSequence: Byte?,
    val arrivalStationSequence: Byte?,
    val departureStationName: String?,
    val arrivalStationName: String?,
    val departureStationId: Long?,
    val arrivalStationId: Long?,
    val carriageId: Long?,
    val carriageType: CarriageType?,
    val carriageNumber: String?,
    val trainNumber: String?,
    val trainType: TrainType?,
    val firstStationDepartureDate: LocalDate?,
    val price: BigDecimal?,

    ) : Serializable {
    companion object {
        @Serial
        private val serialVersionUID = 1L
    }
}
