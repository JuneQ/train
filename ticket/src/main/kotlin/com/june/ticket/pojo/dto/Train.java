package com.june.ticket.pojo.dto;

import com.june.common.enums.TrainType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * <p>
 * 列车信息表
 * </p>
 *
 * @author June
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Train implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "ID")
    public Long id;

    @Schema(description = "列车车次")
    public String trainNumber;

    @Schema(description = "0 高速列车 (High-Speed Train) | 1 城际列车 (Intercity Train) | 2 动车组 (Multiple Unit Train) | 3 直达列车 (Through Train) | 4 特快列车 (Express Train) | 5 快速列车 (Rapid Train) | 6 普通列车 (Ordinary Train) | 7 货物列车 (Freight Train) | 8 旅游列车 (Tourist Train)")
    public TrainType trainType;


    @Schema(description = "起始站ID")
    public Long startStationId;

    @Schema(description = "终点站ID")
    public Long endStationId;

    @Schema(description = "始发时间")
    public LocalTime departureTime;

    @Schema(description = "到达终点站时间")
    public LocalTime arrivalTime;

    @Schema(description = "行驶所跨天数")
    public Byte dayCrossed;

    @Schema(description = "创建时间")
    public LocalDateTime createTime;


    @Schema(description = "修改时间")
    public LocalDateTime updateTime;

    @Schema(description = "列车可用标识")
    public Boolean availableFlag;

    @Schema(description = "删除标识")
    public Boolean delFlag;

    public Station startStation;

    public Station endStation;


}
