package com.june.ticket.pojo.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 车站表
 * </p>
 *
 * @author June
 */
@Data
public class Station implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "ID")
    public Long id;

    @Schema(description = "车站编号")
    public String code;

    @Schema(description = "车站名称")
    public String name;

    @Schema(description = "拼音")
    public String namePinyin;

    @Schema(description = "车站地区")
    public Long regionId;

    @Schema(description = "创建时间")
    public LocalDateTime createTime;

    @Schema(description = "修改时间")
    public LocalDateTime updateTime;


    @TableField(exist = false)
    public Region region;

    @Schema(description = "删除标识")
    public Boolean delFlag;

}
