package com.june.ticket.pojo.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.june.ticket.pojo.dao.entity.RemainingTickets;
import com.june.ticket.pojo.dto.TicketDBView;
import com.june.ticket.pojo.vo.TicketSelectVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 余票信息表 Mapper 接口
 * </p>
 *
 * @author June
 */
@Mapper
public interface RemainingTicketsMapper extends BaseMapper<RemainingTickets> {

    List<TicketDBView> searchRemainingTickets(TicketSelectVo condition);

}
