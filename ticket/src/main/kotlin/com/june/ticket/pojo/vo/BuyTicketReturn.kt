package com.june.ticket.pojo.vo

data class BuyTicketReturn(
    val orderSn: Long,
    val seatNumber: Array<Byte>,
)
