package com.june.ticket.pojo.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 地区表
 * </p>
 *
 * @author June
 */
@Data
public class Region implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "ID")
    public Long id;

    @Schema(description = "地区名称")
    public String name;

    @Schema(description = "地区全名")
    public String fullName;

    @Schema(description = "地区编码")
    public String code;

    @Schema(description = "地区首字母")
    public String initial;

    @Schema(description = "拼音")
    public String pinyin;

    @Schema(description = "热门标识")
    public Boolean hotFlag;

    @Schema(description = "创建时间")
    public LocalDateTime createTime;

    @Schema(description = "修改时间")
    public LocalDateTime updateTime;

    @Schema(description = "删除标识")
    public Boolean delFlag;
}
