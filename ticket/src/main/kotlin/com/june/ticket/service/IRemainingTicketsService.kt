package com.june.ticket.service

import com.june.common.dto.DecrTicketDto
import com.june.common.dto.PlaceOrderDto
import com.june.ticket.pojo.dto.TicketDBView
import com.june.ticket.pojo.vo.BuyTicketReturn
import com.june.ticket.pojo.vo.BuyTicketVo
import com.june.ticket.pojo.vo.TicketSelectVo

interface IRemainingTicketsService {
    fun searchRemainingTickets(condition: TicketSelectVo): List<TicketDBView>
    fun buyTicket(vo: BuyTicketVo): BuyTicketReturn
    fun rollbackSeatForRedis(dto: PlaceOrderDto)
    fun rollbackSeatForMySQL(dto: PlaceOrderDto)


    fun addTickets(cycle: Int)

    fun decrTicket(dto: DecrTicketDto)
    fun initTickets()
}
