@file:OptIn(ExperimentalStdlibApi::class)

package com.june.ticket.service.impl

import cn.hutool.core.date.LocalDateTimeUtil
import cn.hutool.core.util.IdUtil
import cn.hutool.core.util.RandomUtil
import com.alibaba.fastjson2.JSONObject
import com.baomidou.mybatisplus.extension.kotlin.KtQueryWrapper
import com.baomidou.mybatisplus.extension.kotlin.KtUpdateWrapper
import com.june.common.constant.OrderConstant
import com.june.common.constant.TicketConstant
import com.june.common.dto.DecrTicketDto
import com.june.common.dto.PlaceOrderDto
import com.june.common.dto.SubStationDto
import com.june.common.exception.BusinessException
import com.june.common.exception.ErrorCodeEnum
import com.june.common.utils.UserHolder
import com.june.ticket.feign.OrderFeignClient
import com.june.ticket.feign.TrainFeignClient
import com.june.ticket.pojo.dao.entity.RemainingTickets
import com.june.ticket.pojo.dao.mapper.RemainingTicketsMapper
import com.june.ticket.pojo.dto.TicketDBView
import com.june.ticket.pojo.vo.BuyTicketReturn
import com.june.ticket.pojo.vo.BuyTicketVo
import com.june.ticket.pojo.vo.TicketSelectVo
import com.june.ticket.service.IRemainingTicketsService
import org.apache.rocketmq.client.producer.DefaultMQProducer
import org.apache.rocketmq.client.producer.SendCallback
import org.apache.rocketmq.client.producer.SendResult
import org.apache.rocketmq.client.producer.SendStatus
import org.apache.rocketmq.common.message.Message
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.redis.connection.ReturnType
import org.springframework.data.redis.connection.StringRedisConnection
import org.springframework.data.redis.core.Cursor
import org.springframework.data.redis.core.KeyScanOptions
import org.springframework.data.redis.core.ScanOptions
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit


@Service
class RemainingTicketsServiceImpl(
    val remainingTicketsMapper: RemainingTicketsMapper,
    val trainFeignClient: TrainFeignClient,
    val orderFeignClient: OrderFeignClient,
    val stringRedisTemplate: StringRedisTemplate,
    val stringRedisConnection: StringRedisConnection,
    val rocketMQProducer: DefaultMQProducer,

    ) : IRemainingTicketsService {
    companion object {
        val log: Logger = LoggerFactory.getLogger(RemainingTicketsServiceImpl::class.java)
        val placeOrderScript = """
                    local cursor = 0
                    local departure_idx = tonumber(ARGV[1])
                    local arrival_idx = tonumber(ARGV[2])
                    
                    repeat
                        local result = redis.call('SCAN', cursor, 'MATCH', KEYS[1], 'COUNT', 1)
                        cursor = tonumber(result[1])
                        local data = result[2]

                        for i, key in ipairs(data) do
                            if redis.call('BITCOUNT', key, departure_idx, arrival_idx,'BIT') == 0 then
                                for i = departure_idx, arrival_idx, 1 do
                                    redis.call('SETBIT', key, i, 1)
                                end
                                return key
                            end
                        end
                    until cursor == 0

                    return '0'
                """.trimIndent()
        val rollbackOrderScript = """
            for i = ARGV[1],ARGV[2]  do
                redis.call('SETBIT',KEYS[1],i,0)
            end
        """.trimIndent()
    }

    @Value("\${snowflake.worker-id}")
    lateinit var workerId: String

    override fun searchRemainingTickets(condition: TicketSelectVo): List<TicketDBView> {
        return remainingTicketsMapper.searchRemainingTickets(condition).groupBy { it.carriageId }
            .mapValues { (_, carriageForId) ->
                val minStartStationSequenceTicket = carriageForId.minByOrNull { it.departureStationId!! }
                val maxStartStationSequenceTicket = carriageForId.maxByOrNull { it.departureStationSequence!! }
                carriageForId.reduce { acc, curr ->
                    TicketDBView(
                        minOf(acc.tickets!!, curr.tickets!!),
                        acc.trainId,
                        minOf(acc.departureDatetime!!, curr.departureDatetime!!),
                        maxOf(acc.arrivalDatetime!!, curr.arrivalDatetime!!),
                        minOf(acc.departureStationSequence!!, curr.departureStationSequence!!),
                        maxOf(acc.arrivalStationSequence!!, curr.arrivalStationSequence!!),
                        minStartStationSequenceTicket!!.departureStationName,
                        maxStartStationSequenceTicket!!.arrivalStationName,
                        minStartStationSequenceTicket.departureStationId,
                        maxStartStationSequenceTicket.arrivalStationId,
                        acc.carriageId,
                        acc.carriageType,
                        acc.carriageNumber,
                        acc.trainNumber,
                        acc.trainType,
                        acc.firstStationDepartureDate,
                        acc.price!!.plus(curr.price!!)
                    )
                }
            }.values.distinctBy {
                (it.carriageId.toString() + it.firstStationDepartureDate)
            }.toList()
    }


    @OptIn(ExperimentalStdlibApi::class)
    override fun buyTicket(vo: BuyTicketVo): BuyTicketReturn {
        val passengerCount = vo.passengers.size

        val seatNumbers = Array<Byte>(passengerCount) { 0 }

        val userId = UserHolder.getCurrentUser().id

        var successesCount = 0
        for (i in 0..<passengerCount) {
            // 示例:  ticket:tickets-bitmap:12312:123412:2312:202201011515:12  (trainId:firstStationDepartureDate:carriageTypeOrdinal:carriageId:seatNumber)
            val res = stringRedisConnection.eval<ByteArray>(
                placeOrderScript,
                ReturnType.VALUE,
                1,
                "${TicketConstant.REDIS_REMAINING_TICKETS_BITMAP}${vo.trainId}:${vo.firstStationDepartureDate}:${vo.carriageType?.ordinal ?: ""}*",
                (vo.departureStationSequence - 1).toString(),
                (vo.arrivalStationSequence - 2).toString()
            ).toString(Charsets.UTF_8)

            if (successesCount > 0 && res == "0") {
                log.debug("客户订单无法完全完成，但已有成功订单，回滚")
                CompletableFuture.runAsync {
                    for (j in 0..<successesCount) {
                        val ret = stringRedisConnection.eval<Boolean>(
                            rollbackOrderScript,
                            ReturnType.BOOLEAN,
                            1,
                            // 示例:  ticket:tickets-bitmap:12312:123412:2312:202201011515:12  (trainId:firstStationDepartureDate:carriageTypeOrdinal:carriageId::seatNumber)
                            "${TicketConstant.REDIS_REMAINING_TICKETS_BITMAP}${vo.trainId}:${vo.firstStationDepartureDate}:${vo.carriageType!!.ordinal}:${vo.carriageId}:${seatNumbers[j]}",
                            (vo.departureStationSequence - 1).toString(),
                            (vo.arrivalStationSequence - 2).toString()
                        )
                    }
                }
                throw BusinessException(ErrorCodeEnum.NO_ENOUGH_TICKETS)
            }
            if (res == "0") throw BusinessException(ErrorCodeEnum.NO_REMAINING_TICKET)

            val split = res.split(":")
            seatNumbers[i] = split.last().toByte()

            successesCount++
        }

        val orderSn = IdUtil.getSnowflake(workerId.toLong()).nextId()


        // 1. 异步更新MySQL余票数
        CompletableFuture.runAsync {
            remainingTicketsMapper.update(
                KtUpdateWrapper(RemainingTickets::class.java)
                    .eq(RemainingTickets::trainId, vo.trainId)
                    .eq(RemainingTickets::firstStationDepartureDate, vo.firstStationDepartureDate)
                    .eq(RemainingTickets::carriageId, vo.carriageId)
                    .between(RemainingTickets::sequence, vo.departureStationSequence, vo.arrivalStationSequence - 1)
                    .setSql("remaining_tickets = remaining_tickets - $passengerCount")
            )
        }

        val dto = PlaceOrderDto(
            orderSn.toString(),
            userId,
            vo.carriageType,
            vo.carriageId,
            vo.trainId,
            vo.departureStationId,
            vo.departureStationName,
            vo.arrivalStationName,
            vo.arrivalStationId,
            vo.passengers.toTypedArray(),
            vo.departureStationSequence,
            vo.arrivalStationSequence,
            seatNumbers,
            vo.departureDatetime,
            vo.firstStationDepartureDate,
            LocalDateTime.now(),
            vo.price,
            0,
            vo.trainNumber,
            vo.carriageNumber
        )

        val dtoBytes = JSONObject.toJSONString(dto).toByteArray(Charsets.UTF_8)

        // 2. 异步消息更新Order模块
        val placeOrderMsg = Message(OrderConstant.MQ_TOPIC, dtoBytes)
        placeOrderMsg.keys = orderSn.toString()
        placeOrderMsg.tags = OrderConstant.MQ_PLACE_ORDER_TAG
        rocketMQProducer.send(placeOrderMsg, object : SendCallback {
            override fun onSuccess(sendResult: SendResult?) {
                log.debug("Ticket->Order PlaceOrder MQ消息发送成功，消息内容：{}", String(dtoBytes))
            }

            override fun onException(e: Throwable?) {
                handleExceptionMsg(dtoBytes, e)
            }
        })

        // 3. 订单超时消息
        val rollbackMsg =
            Message(
                OrderConstant.MQ_TOPIC_DELAYED,
                dtoBytes
            )
        rollbackMsg.tags = OrderConstant.MQ_ROLLBACK_ORDER_TAG
        rollbackMsg.keys = orderSn.toString()
        rollbackMsg.delayTimeSec = 15 * 60
        rocketMQProducer.send(rollbackMsg, object : SendCallback {
            override fun onSuccess(sendResult: SendResult?) {
                if (sendResult!!.sendStatus != SendStatus.SEND_OK) log.error(
                    "订单 rollbackMsg 未成功发送，SendResult：{}", sendResult
                )
                log.debug("订单延时回滚消息已发送，消息内容：{}", String(dtoBytes))
            }

            override fun onException(e: Throwable?) {
                handleExceptionMsg(dtoBytes, e)
            }
        })

        // -1:消息中间件老是不给通知？？？
        CompletableFuture.runAsync {
            log.debug("消息中间件老是不给通知，prepare！")
            orderFeignClient.placeOrder(dto)
            log.debug("消息中间件老是不给通知，已异步调用！")
        }

        return BuyTicketReturn(orderSn, seatNumbers)
    }

    override fun rollbackSeatForRedis(dto: PlaceOrderDto) {
        for (seatNumber in dto.seatNumbers) {
            stringRedisConnection.eval<Boolean>(
                rollbackOrderScript,
                ReturnType.BOOLEAN,
                1,
                "${TicketConstant.REDIS_REMAINING_TICKETS_BITMAP}${dto.trainId}:${dto.firstStationDepartureDate}:${dto.carriageType.ordinal}:${dto.carriageId}:${seatNumber}",
                // 示例:  ticket:tickets-bitmap:12312:123412:2312:202201011515:12  (trainId:firstStationDepartureDate:carriageTypeOrdinal:carriageId::seatNumber)
                (dto.startSequence - 1).toString(),
                (dto.endSequence - 2).toString()
            )
        }
    }

    override fun rollbackSeatForMySQL(dto: PlaceOrderDto) {
        remainingTicketsMapper.update(
            KtUpdateWrapper(RemainingTickets::class.java).eq(RemainingTickets::trainId, dto.trainId)
                .eq(RemainingTickets::carriageId, dto.carriageId)
                .between(RemainingTickets::sequence, dto.startSequence, dto.endSequence - 1)
                .setSql("remaining_tickets = remaining_tickets + " + dto.passengers.size)
        )
    }


    override fun addTickets(cycle: Int) {
        val s1 = System.currentTimeMillis()
        val carriages = trainFeignClient.getCarriage().data
        val features: MutableList<CompletableFuture<Void>> = mutableListOf()
        for (carriage in carriages) {
            val substations: List<SubStationDto> =
                trainFeignClient.getPastAndNearestSubStationIdAndSequenceAndTime(carriage.trainId).data
            val dayCrossed = ChronoUnit.DAYS.between(
                substations.first().arrivalDatetime.toLocalDate(), substations.last().arrivalDatetime.toLocalDate()
            )
            val stationCrossed = (trainFeignClient.getStationCrossed(carriage.trainId).data - 2).toLong()
            // 示例:  ticket:tickets-bitmap:12312:20220101:123412:2312:12  (trainId:firstStationDepartureDate:carriageTypeOrdinal:carriageId::seatNumber)
            // 2.1 redis
            features.add(CompletableFuture.runAsync {
                repeat(cycle + 1) { index1 ->
                    repeat(carriage.seatCount!! + 1) { index2 ->
                        val binaryValue = ByteArray(stationCrossed.toInt() / 8 + 1) { 0 }
                        stringRedisTemplate.opsForValue().setIfAbsent(
                            "${TicketConstant.REDIS_REMAINING_TICKETS_BITMAP}${carriage.trainId}:${
                                substations[0].arrivalDatetime.toLocalDate().plusDays((dayCrossed + 1) * (index1 + 1))
                            }:${carriage.carriageType.ordinal}:${carriage.id}:${index2 + 1}",

                            String(binaryValue),

                            LocalDateTimeUtil.between(
                                LocalDateTime.now(), substations.last().arrivalDatetime.plusDays(
                                    (index1 + 1) * (dayCrossed + 1)
                                )
                            ).seconds,

                            TimeUnit.SECONDS
                        )
                    }
                }
            })
            // 2.2 mysql
            features.add(CompletableFuture.runAsync {
                repeat(substations.size - 1) { index1 ->
                    repeat(cycle + 1) { index2 ->
                        // 检查是否已添加
                        if (!remainingTicketsMapper.exists(
                                KtQueryWrapper(RemainingTickets::class.java).eq(
                                    RemainingTickets::trainId, carriage.trainId
                                ).eq(RemainingTickets::carriageId, carriage.id).eq(
                                    RemainingTickets::departureDateTime,
                                    substations[index1].arrivalDatetime.plusDays((index2 + 1) * (dayCrossed + 1))
                                )
                            )
                        ) {
                            remainingTicketsMapper.insert(
                                RemainingTickets(
                                    null,
                                    carriage.id,
                                    carriage.trainId,
                                    substations[0].arrivalDatetime.plusDays((index2 + 1) * (dayCrossed + 1))
                                        .toLocalDate(),
                                    substations[index1].arrivalDatetime.plusDays((index2 + 1) * (dayCrossed + 1)),
                                    substations[index1 + 1].arrivalDatetime.plusDays((index2 + 1) * (dayCrossed + 1)),
                                    substations[index1].stationId,
                                    (index1 + 1).toByte(),
                                    carriage.seatCount,
                                    BigDecimal.valueOf(RandomUtil.randomDouble(10.0, 200.0))
                                )
                            )
                        }
                    }
                }
            })
        }
        CompletableFuture.allOf(*features.toTypedArray()).get()
        val s2 = System.currentTimeMillis()
        log.debug("上票任务耗时:{}s", (s2 - s1) / 1000)
    }

    override fun decrTicket(dto: DecrTicketDto) {

    }

    override fun initTickets() {
        // 清理任务
        val s1 = System.currentTimeMillis()
        val cursor = stringRedisTemplate.scan(
            ScanOptions.scanOptions().match("${TicketConstant.REDIS_REMAINING_TICKETS_BITMAP}*").build()
        )
        while (cursor.hasNext()) {
            stringRedisTemplate.delete(cursor.next())
        }
        cursor.close()
        remainingTicketsMapper.delete(null)
        log.debug("清理任务耗时：{}s", (System.currentTimeMillis() - s1) / 1000)
        addTickets(15)
    }

    private fun handleExceptionMsg(str: ByteArray, e: Throwable?) {
        val errorMsg = Message(
            "ERROR_TOPIC", JSONObject.of("data", str, "error", JSONObject.toJSONString(e)).toJSONString().toByteArray()
        )
        // 同步发送
        val sendRet = rocketMQProducer.send(errorMsg)
        if (sendRet.sendStatus != SendStatus.SEND_OK) {
            log.error("投递正常业务队列失败后，投递异常队列仍然失败，异常打印：{}，投递异常：{}", e, sendRet)
            // TODO 发邮箱通知运维和开发
        }
    }

    private fun redisScan(pattern: String?, count: Int): Set<String> {
        val keys: MutableSet<String> = HashSet()
        val serializer = stringRedisTemplate.keySerializer
        val scanOptions = KeyScanOptions.scanOptions().match(
            pattern!!
        ).count(count.toLong()).build()

        val cursor: Cursor<ByteArray> = stringRedisConnection.scan(scanOptions)
        while (cursor.hasNext()) {
            keys.add(serializer.deserialize(cursor.next()).toString())
        }
        return keys
    }
}


