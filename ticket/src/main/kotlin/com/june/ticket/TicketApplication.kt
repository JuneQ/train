package com.june.ticket

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@EnableFeignClients
@ComponentScan(basePackages = ["com.june.ticket", "com.june.common"])
class TicketApplication

fun main(args: Array<String>) {
    runApplication<TicketApplication>(*args)
}
