package com.june.ticket

import com.june.common.dto.PlaceOrderDto
import com.june.ticket.feign.OrderFeignClient
import com.june.ticket.pojo.dao.mapper.RemainingTicketsMapper
import jakarta.annotation.Resource
import org.apache.rocketmq.client.producer.DefaultMQProducer
import org.apache.rocketmq.spring.core.RocketMQTemplate
import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.redis.connection.StringRedisConnection
import org.springframework.data.redis.core.StringRedisTemplate


@SpringBootTest
class TicketDBViewApplicationTests {
    companion object {
        val logger = LoggerFactory.getLogger(TicketDBViewApplicationTests::class.java)
    }

    @Resource
    lateinit var orderFeignClient: OrderFeignClient

    @Resource
    lateinit var stringRedisConnection: StringRedisConnection

    @Resource
    lateinit var stringRedisTemplate: StringRedisTemplate

    @Resource
    lateinit var rocketMQTemplate: RocketMQTemplate

    @Resource
    lateinit var rocketMQProducer: DefaultMQProducer

    @Resource
    lateinit var remainingTicketsMapper: RemainingTicketsMapper


    @Test
    fun test() {
        orderFeignClient.placeOrder(
            PlaceOrderDto()
        )
    }

}
