## 项目介绍

12307是一款售票软件，正如其名，设计理念借鉴了12306火车票订购系统的核心功能，旨在提供高效、稳定、易用的线上购票体验。

[演示地址](http://101.126.78.252:8080) 

 **测试账号**  ： 17364612480    123

1. 系统架构

本项目基于SpringBoot分布式微服务架构，前端基于Vue3，使用[ArcoDesign Pro](https://pro.arco.design/)
进行开发，确保在各种设备上都能提供良好的用户体验。数据库方面，我们使用了MySQL+Redis来存储票务信息、用户数据等核心数据，并通过缓存技术提高系统性能。

2. 核心功能

- **在线购票**：用户可以根据`出发地`、`目的地`、`出发日期`三个条件查询票务信息，选择合适的票种进行购买。系统支持使用支付宝进行支付。
- **车次管理**：系统内火车票的上票是全自动进行的，管理员仅需设置每轮次火车发车时间与到达时间。
- **用户注册与登录**：用户可以通过手机号或账号密码进行注册，并设置密码。注册成功后，用户可以使用账号登录系统，享受购票服务。
- **订单管理**：用户可以查看自己的购票订单，包括待支付、已支付、已取消等状态的订单。
- **车站和地区管理**：管理员需要手动设置此模块。

3. 技术选型及原因

- MySQL：开源免费，且其他数据库的兼容性大多以MySQL为基准，有深入理解的必要。以后有使用TiDB或者PostgreSQL的打算
- Redis：做缓存+辅助存储。没有必要用es全文检索，且数据更新频繁，属于是不得不的选择
- RocketMQ：异步下单，超时消息
- Redisson：分布式锁
- Nacos、Openfeign：服务注册、远程调用
- MyBatisPlus：简化增删改查

## 系统核心设计

### 1. 火车车站与地区维护

属于基础功能，数据库设计如图所示，station通过region_id与region表进行关联

<div style="display: flex; justify-content: space-between;">  
  <img src="assets/image-20240309192540324.png" alt="Image 1">  
  <img src="assets/image-20240309192827387.png" alt="Image 2">  
</div>

### 2. 余票设计

属于核心功能，先看几张表

<div style="display: flex; justify-content: space-between;">  
  <img src="assets/image-20240309193706046.png" alt="Image 1">  
  <img src="assets/image-20240309193732015.png" alt="Image 2">  
    <img src="assets/image-20240309193749231.png" alt="Image 2">  
</div>

首先说明，除了`create_time`和`update_time`这两字段外，其余涉及到时间的类型都严格采用如下标准：

- 名字只含有`date`，数据类型也只会含有date信息
- 名字只含有`time`，数据类型也只会含有time信息
- 名字含有`datetime`，数据类型会含有日期和时间的信息

第一张是车次表，发车时间只保存始发与到终点站时间，其中`day_crossed`
字段表明列车从始发站到终点站经过了多少天，不足一天用0表示。其余字段皆可按名知意思或查询建表SQL注释，不做赘述。

第二张表是余票表，`first_station_departure_date`是**本趟列车**
的始发日期（用于划分同一个车次列车所处于的不同运行轮次）。`departure_station_id`
是出发站ID，而此表没有保存`arrival_station_id`，这是因为默认`remaining_tickets`语义是从`departure_station_id`
到列车运行轨迹的紧挨下一站所剩余票数，`sequence`字段则保存了此出发站是该车次运行轨迹的第几个车站，`price`
的语义同样是从此出发站到紧挨下一站的票价。此表主要由定时任务维护，一般不允许人工添加

第三张是列车到站表，保存了列车ID、到站ID、到站时间、站序等信息，其中`kth_from_departure_day`是用来记录此到站时间是始发时间的第几天（from
1），其余字段仍不再赘述，皆可见名知意或查询建表SQL注释。

### 3. 订单设计

<div style="display: flex; justify-content: space-between; height:500px">  
  <img src="assets/image-20240309195359093.png" alt="Image 1">  
  <img src="assets/image-20240309195409918.png" alt="Image 2">  
</div>

所有字段皆可见名知意，`order_item`表保存很多冗余字段的原因是，这些字段通常设置后都不会更改。

## 核心业务实现

### 1. 上票

规定：列车如果在前一天完成该趟车次，第二天一定会继续发车，作为该趟列车始发日期。

上票分为初始上票与增量上票，两者不同在于前者会做清理工作。

项目部署后，会有一个quartz定时任务模块，在首次运行项目时会进行初始化工作，其中包括初始上票任务。清理时为保redis不阻塞，使用游标进行清除。

上票时，首先会查询所有车厢信息，此查询会关联`train`、`station`
表将拿到所有信息（车厢唯一关联列车/车次，车厢有多少个座位就有多少张票，车厢是什么类型座位就是什么类型），然后对车厢进行遍历，进行如下任务：

- 规定'2024-01-01'为所有列车的初始发车时间，然后获取此车厢关联车次的最近的一次发车信息
    - 计算算法采用类似退避算法的思想，定义一个数组[5,3,2,1]，首先用arr[0]*dayCrossed+'2024-01-01'
      进行试探，若超出当前时间，则采用arr[1]\*dayCrossed继续试探，若早于当前时间，继续采用arr[1]
      \*dayCrossed进行试探，直至用尽arr[lastIdx]，可以计算出一个偏移天数
    - 对所有基于'2024-01-01'的中途到站加上此偏移天数，即可得到完整的过去且最近的中途到站的datetime信息
- 再进行Redis上票，命令采用setIfAbsent，Redis数据结构采用bitmap。
    - key形如：`ticket:tickets-bitmap:12312:20220101:0:23434312:12` 对应：prefix:trainId:firstStationDepartureDate:
      carriageTypeOrdinal:carriageId::seatNumber
    - value是一个arrivalStationSequence-departureStationSequence长度的位图，由于redis设置位是最少基于字节，故会存在向8向上取整的情况
    - 此数据结构仅用于购票时的查询，可以达到秒杀的效果
- 再对MySQL进行上票，主要填充`remaining_tickets`表，各字段值释义见注释
    - 添加数据时会先进行查询，不存在唯一信息才会插入
    - 由发车日期、车厢号等信息即可确定唯一性

增量上票没有了清理任务，便实现了幂等性

### 2. 下单

细说上一步位图的设计：假设有五个车站，那么我会用4个bit保存一个座位的购票情况，这4个bit其实就是乘车区间。

用户下单时，会有departureStationSequence、arrivalStationSequence等信息，然后通过定义的lua脚本（scan扫描座位，分析此乘车区间的购票情况是否满足要求的sequence来决定是否应该生成这张票），试图去锁定座位。若出现座位不足的情况，则会回滚redis已锁定座位，若座位充足，则会通过异步线程更新DB库存，再由异步消息去通知订单模块生成订单，此处除了查票行为外，均是非阻塞调用。

为什么不用es做查询？

- 数据更新过于频繁
- 查询内容简单，不属于全文检索的范畴

扫描操作是否有性能瓶颈？

- 有，但比update db ,再判断ret==1 这样的逻辑要快
- 根据实际业务逻辑，一个车厢一般有几十上百个座位，且找到符合条件的会立即返回，速度会非常快
    - 如果redis可以保证遍历顺序，那保存遍历位置就可以再次优化速度了

### 3. 用户登录

实现了两种登录逻辑：手机号+验证码、手机号+密码。底层均为JWT认证方式。`user模块`依赖事先规定的`jwt-secret-key`
对登录成功的用户信息进行加密，然后各个业务模块依赖此密钥进行解密用户信息，校验是否过期等信息。另外，在此之前，需要先查询Redis的token-black-list，方便实现踢人下线功能。如果有多端登陆的这样的需求，可以反过来保存user模块签发过的token，这样可以方便查询存活token数，进而实现这样的需求。

## 系统优化

### 1. SQL优化

```sql
# 查票SQL
explain
select min(tickets),
       carriage_id,
       min(departure_datetime),
       max(arrival_datetime),
       c.carriage_type,
       c.carriage_number,
       c.train_id,
       min(departure_sequence) as departure_sequence,
       max(arrival_sequence)   as arrival_sequence,
       tsa_start.station_id    as departure_station_id,
       tsa_end.station_id      as arrival_station_id
from (SELECT tmp.carriage_id,
             tmp.tickets,
             tmp.departure_datetime,
             tmp.arrival_datetime,
             tmp.departure_sequence,
             tmp.arrival_sequence
      FROM (SELECT MIN(rt.remaining_tickets)  as tickets,
                   rt.carriage_id,
                   min(rt.departure_datetime) as departure_datetime,
                   max(rt.arrival_datetime)   as arrival_datetime,
                   MIN(rt.sequence)           AS departure_sequence,
                   MAX(rt.sequence) + 1       AS arrival_sequence
            FROM remaining_tickets rt
            WHERE date(rt.departure_datetime) in (:departureDate)
              and remaining_tickets > 0
              and rt.departure_datetime > now()
              AND EXISTS (SELECT 1
                          FROM train_station_arrival tsa_start
                                   JOIN station s1 ON tsa_start.station_id = s1.id
                          WHERE s1.region_id = :startRegionId
                            AND tsa_start.train_id = rt.train_id
                            AND rt.sequence >= tsa_start.sequence)
              AND EXISTS (SELECT 1
                          FROM train_station_arrival tsa_end
                                   JOIN station s2 ON tsa_end.station_id = s2.id
                          WHERE s2.region_id = :endRegionId
                            AND tsa_end.train_id = rt.train_id
                            AND rt.sequence < tsa_end.sequence)
            GROUP BY rt.carriage_id
                   , rt.departure_datetime, rt.arrival_datetime) tmp) ret
         JOIN train.carriage c ON ret.carriage_id = c.id
         JOIN
     train_station_arrival tsa_start ON tsa_start.train_id = c.train_id
         AND tsa_start.sequence = ret.departure_sequence
         JOIN
     train_station_arrival tsa_end ON tsa_end.train_id = c.train_id
         AND tsa_end.sequence = ret.arrival_sequence

group by ret.carriage_id, c.carriage_type, c.carriage_number, c.train_id, ret.departure_datetime,
         tsa_start.station_id, tsa_end.station_id;
```

优化思路

1. 去掉子查询，尽可能用JOIN
2. 由于查询结果一定不会很多，处理逻辑可以放在代码中进行
3. 加索引
4. 适当放回代码内处理

#### 去掉内连接，尽量改用JOIN

```sql
SELECT MIN(ret1.tickets)            as tickets,
               ret1.carriage_id             as carriage_id,
               MIN(ret1.departure_datetime) AS departure_datetime,
               MAX(ret1.arrival_datetime)   AS arrival_datetime,
               MIN(ret1.departure_sequence) AS departure_station_sequence,
               MAX(ret1.arrival_sequence)   AS arrival_station_sequence,
               departure_station_name,
               arrival_station_name,
               tsa_departure.station_id     AS departure_station_id,
               tsa_arrival.station_id       AS arrival_station_id,
               carriage_type,
               carriage_number,
               c.train_id,
               t.train_number,
               t.train_type,
               ret1.first_station_departure_date,
               ret1.price
        FROM (SELECT rt.remaining_tickets AS tickets,
                     rt.carriage_id,
                     rt.departure_datetime,
                     rt.arrival_datetime,
                     rt.first_station_departure_date,
                     MIN(rt.sequence)     AS departure_sequence,
                     MAX(rt.sequence) + 1 AS arrival_sequence,
                     s1.name              as departure_station_name,
                     s2.name              as arrival_station_name,
                     rt.price
              FROM remaining_tickets rt
                       JOIN train_station_arrival tsa_start
                            ON tsa_start.train_id = rt.train_id AND rt.sequence >= tsa_start.sequence
                       JOIN train_station_arrival tsa_end
                            ON tsa_end.train_id = rt.train_id AND rt.sequence &lt; tsa_end.sequence
                       JOIN station s1 ON tsa_start.station_id = s1.id AND s1.region_id = #{startRegionId}
                       JOIN station s2 ON tsa_end.station_id = s2.id AND s2.region_id = #{endRegionId}
              WHERE date(rt.departure_datetime) = #{departureDate}
                AND rt.departure_datetime > NOW()
                AND rt.remaining_tickets > 0
              GROUP BY rt.carriage_id, rt.departure_datetime, rt.arrival_datetime, rt.remaining_tickets,
                       rt.first_station_departure_date, departure_station_name, arrival_station_name, rt.price) ret1
                 JOIN train.carriage c ON ret1.carriage_id = c.id
                 JOIN train_station_arrival tsa_departure
                      ON tsa_departure.train_id = c.train_id AND tsa_departure.sequence = ret1.departure_sequence
                 JOIN train_station_arrival tsa_arrival
                      ON tsa_arrival.train_id = c.train_id AND tsa_arrival.sequence = ret1.arrival_sequence
                 JOIN train t on t.id = c.train_id
                 JOIN remaining_tickets rt_initial on rt_initial.train_id = c.train_id and rt_initial.sequence = 1
        GROUP BY ret1.carriage_id, c.carriage_type, c.carriage_number, c.train_id,
                 tsa_departure.station_id, tsa_arrival.station_id, t.train_number, t.train_type,
                 ret1.first_station_departure_date, ret1.departure_station_name, ret1.arrival_station_name, ret1.price
        order by carriage_id, departure_datetime
```

#### 索引

先explain一下

![image-20240317212133884](assets/image-20240317212133884.png)

加索引

```sql
-- 为remaining_tickets表创建索引
CREATE INDEX idx_rt_train_id ON remaining_tickets(train_id);
CREATE INDEX idx_rt_sequence ON remaining_tickets(sequence);
CREATE INDEX idx_rt_departure_datetime ON remaining_tickets(departure_datetime);
CREATE INDEX idx_rt_remaining_tickets ON remaining_tickets(remaining_tickets);

-- tsa表创建索引
CREATE INDEX idx_tsa_train_id_sequence_station_id ON train_station_arrival(train_id, sequence,station_id);


-- station表创建索引
CREATE INDEX idx_station_id_region_id_name ON station(id, region_id,name);


-- carriage表创建索引
CREATE INDEX idx_carriage_id ON carriage(id);

-- 为train表创建索引
CREATE INDEX idx_train_id_train_number_train_type ON train(id,train_number,train_type);
```

![image-20240317220818888](assets/image-20240317220818888.png)

```
鉴于执行顺序是序号高的优先，同序号从上往下，因此在JOIN完后，走到第一行也没剩多少记录了，用buffer也没关系，而且临时表也不太好加索引。最关键的还是rt(remaining_tickets)表的优化。
```

```
以上SQL不是完全正确的，需要代码继续处理，理由仍然是：数据量小，没有必要加SQL，而且SQL优化难度大
```

#### 代码处理

这是一个查询从西安->北京的3.18的车票，中途只经过深圳北站。此查询结果需要聚合一下

![image-20240317223750323](assets/image-20240317223750323.png)

```kotlin
remainingTicketsMapper.searchRemainingTickets(condition).groupBy { it.carriageId }
    .mapValues { (_, carriageForId) ->
        val minStartStationSequenceTicket = carriageForId.minByOrNull { it.departureStationId!! }
        val maxStartStationSequenceTicket = carriageForId.maxByOrNull { it.departureStationSequence!! }
        carriageForId.reduce { acc, curr ->
            TicketDBView(
                minOf(acc.tickets!!, curr.tickets!!),
                acc.trainId,
                minOf(acc.departureDatetime!!, curr.departureDatetime!!),
                maxOf(acc.arrivalDatetime!!, curr.arrivalDatetime!!),
                minOf(acc.departureStationSequence!!, curr.departureStationSequence!!),
                maxOf(acc.arrivalStationSequence!!, curr.arrivalStationSequence!!),
                minStartStationSequenceTicket!!.departureStationName,
                maxStartStationSequenceTicket!!.arrivalStationName,
                minStartStationSequenceTicket.departureStationId,
                maxStartStationSequenceTicket.arrivalStationId,
                acc.carriageId,
                acc.carriageType,
                acc.carriageNumber,
                acc.trainNumber,
                acc.trainType,
                acc.firstStationDepartureDate,
                acc.price!!.plus(curr.price!!)
            )
        }
    }.values.distinctBy {
        (it.carriageId.toString() + it.firstStationDepartureDate)
    }.toList()
```

### 2. 查票缓存

关于余票查询一直是本系统的痛点和难点。购票功能加的redis数据在这里并不能复用，因为此处是基于日期-地区进行查询的，并不是基于座位。故需要设计一个缓存。

#### 查库

```
// 伪代码
{
	var ret = selectRedis
	if(ret!=null) return ret
	try(){
	    lock.lock()
	    ret = selectRedis
	    if(ret!=null) return ret
	    else{
	    	 ret = selectDB
	    	 C.runAsync{
	    	 		setRedis(ret)
	    	 }
	    }
	}finally{
      lock.unlock()
	}
	return ret
}
```

#### 更新(即购票)

先更新MySQL再更新Redis

```
// 伪代码
{
	var res = searchRedisTicketRemainingTickets()
	if(res = "no ticket"){
		return "no ticket"
	}else if(res = "partinal buy"){
		rollbackRedis()
		return "no ticket"
	}
	var orderSn = IdUtil.getSnowflake(workerId.toLong()).nextId()

	var dto(res,orderSn)

	// redis 票够，且已锁定
	// res 此时会包含座位号，前端会传来购票的其他参数，两者结合可锁定mysql行
  CompletableFuture.runAsync {
  	updateMySQL(dto)
  }.thenRun{
  	invalidateRedis(dto)
  }
  
  asyncMsg(dto) // update order service
  
  return orderSn
}
```
