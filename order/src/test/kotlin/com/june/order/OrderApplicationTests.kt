package com.june.order

import com.june.order.feign.TrainFeignClient
import jakarta.annotation.Resource
import org.apache.rocketmq.spring.core.RocketMQTemplate
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest


@SpringBootTest
class OrderApplicationTests {
    @Resource
    lateinit var trainFeignClient: TrainFeignClient

    @Resource
    lateinit var rocketMQTemplate: RocketMQTemplate

    @Test
    fun contextLoads() {
    }

}
