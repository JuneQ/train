package com.june.order.config

import jakarta.annotation.Resource
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
class InterceptorConfig : WebMvcConfigurer {
    @Resource
    lateinit var loginInterceptor: LoginInterceptor


    override fun addInterceptors(registry: InterceptorRegistry) {
        // 可添加多个
        registry.addInterceptor(loginInterceptor)
            .addPathPatterns("/**")
            .excludePathPatterns("/pay")
            .excludePathPatterns("/pay/success-callback")
            .excludePathPatterns("/order/place-order")
    }
}
