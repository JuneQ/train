package com.june.order.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.connection.DefaultStringRedisConnection
import org.springframework.data.redis.connection.RedisConnectionFactory
import org.springframework.data.redis.connection.StringRedisConnection

@Configuration
class RedisConfig {
    @Bean
    fun getStringRedisConnection(redisConnectionFactory: RedisConnectionFactory): StringRedisConnection {
        return DefaultStringRedisConnection(redisConnectionFactory.connection)
    }
}
