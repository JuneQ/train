package com.june.order.config

import com.alipay.easysdk.factory.Factory
import com.alipay.easysdk.kernel.Config
import com.alipay.easysdk.payment.page.models.AlipayTradePagePayResponse
import com.june.order.pojo.dto.AliPayDto
import jakarta.annotation.PostConstruct
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import java.net.URLEncoder


@ConfigurationProperties(prefix = "alipay")
@Component
class AlipayTemplate {
    companion object {
        val log: Logger = LoggerFactory.getLogger(AlipayTemplate::class.java)
    }


    @Value("\${alipay.appPrivateKey}")
    private lateinit var appPrivateKey: String

    @Value("\${alipay.alipayPublicKey}")
    private lateinit var alipayPublicKey: String

    @Value("\${alipay.notifyUrl}")
    private lateinit var notifyUrl: String

    @Value("\${alipay.appId}")
    private lateinit var appId: String

    @Value("\${alipay.returnUrl}")
    private lateinit var returnUrl: String

    @Value("\${alipay.timeout}")
    private lateinit var timeout: String

    @Value("\${alipay.gateway}")
    private lateinit var gateway: String

    @PostConstruct
    fun init() {
        // 设置参数（全局只需设置一次）
        val config: Config = Config()
        config.gatewayHost = gateway
        config.protocol = "https"
        config.signType = "RSA2"
        config.appId = this.appId
        config.merchantPrivateKey = this.appPrivateKey
        config.alipayPublicKey = this.alipayPublicKey
        config.notifyUrl = this.notifyUrl
        Factory.setOptions(config)
        println("=======支付宝SDK初始化成功=======")
    }


    fun pay(dto: AliPayDto): String {
        val response: AlipayTradePagePayResponse
        try {
            //  发起API调用（以创建当面付收款二维码为例）
            response = Factory.Payment.Page()
                .pay(URLEncoder.encode(dto.subject, "UTF-8"), dto.outTradeNo, dto.totalAmount, this.returnUrl)
        } catch (e: Exception) {
            System.err.println("调用遭遇异常，原因：" + e.message)
            throw RuntimeException(e.message, e)
        }
        return response.body
    }
}
