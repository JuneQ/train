package com.june.order.pojo.dao.entity

import com.baomidou.mybatisplus.annotation.TableField
import com.baomidou.mybatisplus.annotation.TableId
import com.baomidou.mybatisplus.annotation.TableName
import io.swagger.v3.oas.annotations.media.Schema
import java.math.BigDecimal
import java.time.LocalDateTime

@TableName("user_pay")
data class UserPay(
    @TableId
    @Schema(description = "ID")
    var id: Long,

    @Schema(description = "订单号")
    @TableField("order_sn")
    var orderSn: String?,

    @Schema(description = "商户订单号")
    @TableField("out_order_sn")
    var outOrderSn: String?,

    @Schema(description = "支付渠道")
    @TableField("channel")
    var channel: String?,

    @Schema(description = "支付环境")
    @TableField("trade_type")
    var tradeType: String?,

    @Schema(description = "订单标题")
    @TableField("subject")
    var subject: String?,

    @Schema(description = "商户订单号")
    @TableField("order_request_id")
    var orderRequestId: String?,

    @Schema(description = "交易总金额")
    @TableField("total_amount")
    var totalAmount: BigDecimal?,

    @Schema(description = "三方交易凭证号")
    @TableField("trade_no")
    var tradeNo: String?,

    @Schema(description = "付款时间")
    @TableField("gmt_payment")
    var gmtPayment: LocalDateTime?,

    @Schema(description = "支付金额")
    @TableField("pay_amount")
    var payAmount: BigDecimal?,

    @Schema(description = "支付状态")
    @TableField("status")
    var status: String?,

    @Schema(description = "创建时间")
    @TableField("create_time")
    var createTime: LocalDateTime?,

    @Schema(description = "修改时间")
    @TableField("update_time")
    var updateTime: LocalDateTime?,

    @Schema(description = "删除标记 0：未删除 1：删除")
    @TableField("del_flag")
    var delFlag: Boolean?,
)
