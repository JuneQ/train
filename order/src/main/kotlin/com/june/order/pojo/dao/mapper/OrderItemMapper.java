package com.june.order.pojo.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.june.order.pojo.dao.entity.OrderItem;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 订单项-车票明细表 Mapper 接口
 * </p>
 *
 * @author June
 */
@Mapper
public interface OrderItemMapper extends BaseMapper<OrderItem> {

}
