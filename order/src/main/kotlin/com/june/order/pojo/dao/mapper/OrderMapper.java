package com.june.order.pojo.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.june.order.pojo.dao.entity.Order;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author June
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

}
