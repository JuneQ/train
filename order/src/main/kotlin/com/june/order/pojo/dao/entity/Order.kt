package com.june.order.pojo.dao.entity

import com.baomidou.mybatisplus.annotation.*
import com.june.common.enums.OrderStatus
import com.june.common.enums.PayType
import io.swagger.v3.oas.annotations.media.Schema
import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor
import java.time.LocalDateTime

@TableName("`order`")
@AllArgsConstructor
@NoArgsConstructor
@Data
class Order(
    @TableId
    @Schema(description = "ID")
    var id: Long? = null,

    @Schema(description = "订单号")
    @TableField("order_sn")
    var orderSn: String? = null,

    @Schema(description = "用户ID")
    @TableField("user_id")
    var userId: Long? = null,

    @Schema(description = "订单来源")
    @TableField("source")
    var source: String? = null,

    @Schema(description = "订单状态: 0已付款 1有退款 2已完成")
    @TableField("status")
    @EnumValue
    var status: OrderStatus? = null,

    @Schema(description = "支付时间")
    @TableField("pay_time")
    var payTime: LocalDateTime? = null,

    @Schema(description = "支付方式: 0支付宝 1微信 2银行卡 3现金 4其他 5 未付款")
    @TableField("pay_type")
    @EnumValue
    var payType: PayType? = null,

    @Schema(description = "创建时间")
    @TableField("create_time")
    var createTime: LocalDateTime? = null,

    @Schema(description = "修改时间")
    @TableField("update_time")
    var updateTime: LocalDateTime? = null,

    @Schema(description = "删除标识")
    @TableField("del_flag")
    var delFlag: Boolean? = null,

    @Schema(description = "版本号")
    @TableField("version")
    @Version
    var version: Int? = null,


    ) {
    @TableField(exist = false)
    var orderItems: MutableList<OrderItem> = mutableListOf()
}
