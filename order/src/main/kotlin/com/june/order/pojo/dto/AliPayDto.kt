package com.june.order.pojo.dto

data class AliPayDto(
    var outTradeNo: String, // 商户订单号 必填

    var subject: String, // 订单名称 必填

    var totalAmount: String, // 付款金额 必填

    var body: String?, // 商品描述 可空
)
