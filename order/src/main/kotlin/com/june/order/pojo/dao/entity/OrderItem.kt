package com.june.order.pojo.dao.entity

import com.baomidou.mybatisplus.annotation.TableField
import com.baomidou.mybatisplus.annotation.TableId
import com.baomidou.mybatisplus.annotation.TableName
import com.june.common.enums.CarriageType
import com.june.common.enums.PassengerType
import com.june.order.pojo.dto.Passenger
import io.swagger.v3.oas.annotations.media.Schema
import java.math.BigDecimal
import java.time.LocalDateTime

@TableName("order_item")
data class OrderItem(
    @Schema(description = "ID") @TableId("id")
    var id: Long?,

    @TableField("order_id")
    var orderId: Long?,

    @TableField("train_id")
    var trainId: Long?,

    @TableField("train_number")
    var trainNumber: String?,

    @TableField("carriage_id")
    var carriageId: Long?,

    @TableField("carriage_type")
    var carriageType: CarriageType?,

    @TableField("carriage_number")
    var carriageNumber: String?,

    @TableField("departure_station_id")
    var departureStationId: Long?,

    @TableField("departure_station_name")
    var departureStationName: String?,

    @TableField("arrival_station_id")
    var arrivalStationId: Long?,

    @TableField("arrival_station_name")
    var arrivalStationName: String?,
    @TableField("seat_number")
    var seatNumber: Byte?,

    @TableField("passenger_id")
    var passengerId: Long?,

    @TableField("passenger_name")
    var passengerName: String?,

    @TableField("passenger_type")
    var passengerType: PassengerType?,

    @TableField("passenger_phone")
    var passengerPhone: String?,

    @TableField("ticket_price")
    var ticketPrice: BigDecimal?,

    @TableField("departure_datetime")
    var departureDatetime: LocalDateTime?,

    @TableField("arrival_datetime")
    var arrivalDatetime: LocalDateTime?,

    @Schema(description = "创建时间")
    @TableField("create_time")
    var createTime: LocalDateTime?,

    @Schema(description = "修改时间")
    @TableField("update_time")
    var updateTime: LocalDateTime?,

    @Schema(description = "删除标识")
    @TableField("del_flag")
    var delFlag: Boolean?,

    ) {
    @TableField(exist = false)
    var passengers: MutableList<Passenger>? = mutableListOf()
}
