package com.june.order.pojo.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.june.order.pojo.dao.entity.UserPay;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 支付表 Mapper 接口
 * </p>
 *
 * @author June
 */
@Mapper
public interface UserPayMapper extends BaseMapper<UserPay> {

}
