package com.june.order.pojo.dao.entity//package com.june.ticket.pojo.dao
//
//import com.june.common.enums.SeatType
//import io.swagger.v3.oas.annotations.media.Schema
//import org.springframework.data.annotation.TableId
//import org.springframework.data.relational.core.mapping.TableName
//import java.math.BigDecimal
//import java.util.*
//
//@TableName("user_refund")
//data class UserRefund(
//    @TableId
//    @Schema(description = "ID")
//    var id: String,
//
//    @Schema(description = "支付流水号")
//    var paySn: String?,
//
//    @Schema(description = "订单号")
//    var orderSn: String?,
//
//    @Schema(description = "三方交易凭证号")
//    var tradeNo: String?,
//
//    @Schema(description = "退款金额")
//    var amount: BigDecimal?,
//
//    @Schema(description = "用户ID")
//    var userId: String?,
//
//    @Schema(description = "用户名")
//    var username: String?,
//
//    @Schema(description = "列车ID")
//    var trainId: String?,
//
//    @Schema(description = "列车车次")
//    var trainNumber: String?,
//
//    @Schema(description = "乘车日期")
//    var ridingDate: Date?,
//
//    @Schema(description = "出发站点")
//    var departure: String?,
//
//    @Schema(description = "到达站点")
//    var arrivar: String?,
//
//    @Schema(description = "出发时间")
//    var departureTime: Date?,
//
//    @Schema(description = "到达时间")
//    var arrivarTime: Date?,
//
//    @Schema(description = "座位类型 0无座 1硬座 2硬卧 3软卧 4二等座 5一等座 6商务座")
//    var seatType: SeatType?,
//
//    @Schema(description = "证件类型")
//    var idType: Int?,
//
//    @Schema(description = "证件号")
//    var idCard: String?,
//
//    @Schema(description = "真实姓名")
//    var realName: String?,
//
//    @Schema(description = "订单状态")
//    var status: Int?,
//
//    @Schema(description = "退款时间")
//    var refundTime: Date?,
//
//    @Schema(description = "创建时间")
//    var createTime: Date?,
//
//    @Schema(description = "修改时间")
//    var updateTime: Date?,
//
//    @Schema(description = "删除标记 0：未删除 1：删除")
//    var delFlag: Boolean?
//)
