package com.june.order.pojo.dto;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.june.common.enums.IdType;
import com.june.common.enums.PassengerType;
import com.june.common.enums.VerifyStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 乘车人
 * </p>
 *
 * @author june
 */
@Data
@TableName("passenger")
@Schema(name = "Passenger", description = "$!{table.comment}")
public class Passenger implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "id")
    @TableId("id")
    private Long id;

    @Schema(description = "会员id")
    @TableField("user_id")
    private Long userId;

    @Schema(description = "姓名")
    @TableField("name")
    private String name;

    @Schema(description = "身份证")
    @TableField("id_card")
    private String idCard;

    @Schema(description = "证件类型")
    @TableField("id_type")
    private IdType idType;

    @Schema(description = "旅客类型")
    @TableField("type")
    private PassengerType passengerType;


    @Schema(description = "校验状态")
    @TableField("verify_status")
    private VerifyStatus verifyStatus;

    @Schema(description = "手机号")
    @TableField("phone")
    private String phone;

    @Schema(description = "新增时间")
    @TableField(value = "create_time", updateStrategy = FieldStrategy.NOT_NULL)
    private LocalDateTime createTime;

    @Schema(description = "修改时间")
    @TableField(value = "update_time", updateStrategy = FieldStrategy.NOT_NULL)
    private LocalDateTime updateTime;

    @Schema(description = "删除标识")
    @TableField(value = "del_flag")
    private Boolean delFlag;
}
