package com.june.order.mqConsumer

import com.alibaba.fastjson2.JSONObject
import com.june.common.constant.OrderConstant
import com.june.common.dto.PlaceOrderDto
import com.june.order.service.IOrderService
import org.apache.rocketmq.common.message.MessageExt
import org.apache.rocketmq.spring.annotation.ConsumeMode
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener
import org.apache.rocketmq.spring.core.RocketMQListener
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component


/**
 * 异步下单普通消息
 */
@Component
@RocketMQMessageListener(
    topic = OrderConstant.MQ_TOPIC,
    selectorExpression = OrderConstant.MQ_PLACE_ORDER_TAG,
    consumerGroup = OrderConstant.MQ_CONSUMER_GROUP, // 云RocketMQ也需要配置ConsumerGroup来决定消费顺序
    consumeMode = ConsumeMode.CONCURRENTLY  // 此处和上一行都决定消费顺序
)
class PlaceOrderConsumer(
    val orderServiceImpl: IOrderService,
) : RocketMQListener<MessageExt> {
    companion object {
        val log: Logger = LoggerFactory.getLogger(PlaceOrderConsumer::class.java)
    }

    override fun onMessage(message: MessageExt) {
        log.debug("{} 收到消息：{}", OrderConstant.MQ_TOPIC, String(message.body))
        val parseObject = JSONObject.parseObject(String(message.body), PlaceOrderDto::class.java)
        orderServiceImpl.placeOrder(parseObject)
    }
}

/**
 * 订单超时消息，需要回滚redis和mysql
 */
@Component
@RocketMQMessageListener(
    topic = OrderConstant.MQ_TOPIC_DELAYED,
    selectorExpression = OrderConstant.MQ_ROLLBACK_ORDER_TAG,
    consumerGroup = OrderConstant.MQ_CONSUMER_GROUP, // 云RocketMQ也需要配置ConsumerGroup来决定消费顺序
    consumeMode = ConsumeMode.CONCURRENTLY  // 此处和上一行都决定消费顺序
)
class RollBackOrderConsumer(
    val orderServiceImpl: IOrderService,
) : RocketMQListener<MessageExt> {
    companion object {
        val log: Logger = LoggerFactory.getLogger(RollBackOrderConsumer::class.java)
    }

    override fun onMessage(message: MessageExt) {

        log.debug("{} 收到消息：{}", OrderConstant.MQ_TOPIC_DELAYED, String(message.body))
        val dto = JSONObject.parseObject(String(message.body), PlaceOrderDto::class.java)
        orderServiceImpl.rollbackOrder(dto)
    }

}
