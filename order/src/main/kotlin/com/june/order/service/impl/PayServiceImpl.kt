package com.june.order.service.impl

import com.june.order.feign.PassengerFeignClient
import com.june.order.pojo.dao.mapper.OrderItemMapper
import com.june.order.pojo.dao.mapper.OrderMapper
import com.june.order.pojo.dao.mapper.UserPayMapper
import com.june.order.pojo.dto.AliPayDto
import com.june.order.service.IOrderService
import com.june.order.service.IPayService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service


@Service
class PayServiceImpl(
    val payMapper: UserPayMapper,
    val orderMapper: OrderMapper,
    val orderItemMapper: OrderItemMapper,
    val orderServiceImpl: IOrderService,
    val passengerFeignClient: PassengerFeignClient,
) : IPayService {
    companion object {
        val log: Logger = LoggerFactory.getLogger(PayServiceImpl::class.java)
    }

    @Value("\${snowflake.worker-id}")
    lateinit var workerId: String
    override fun prepareGoThirdPartyPay(orderSn: String): AliPayDto {
        val order = orderServiceImpl.getOrderByOrderSn(orderSn)

        return AliPayDto(
            orderSn,
            "订单流水号：${order.orderSn}",
            order.orderItems.sumOf { it.ticketPrice!! }.toPlainString(),
            ""
        )
    }
}

