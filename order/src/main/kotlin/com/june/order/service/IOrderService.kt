package com.june.order.service

import com.june.common.dto.PlaceOrderDto
import com.june.order.pojo.dao.entity.Order

interface IOrderService {
    fun placeOrder(placeOrderDto: PlaceOrderDto): Order?
    fun rollbackOrder(dto: PlaceOrderDto)
    fun getOrderByOrderSn(orderSn: String): Order

    fun getOrderInfo(orderSn: String): Order?
    fun getOrderInfoList(): Array<Order>?
    fun paySuccess(orderSn: String)
}
