package com.june.order.service

import com.june.order.pojo.dto.AliPayDto

interface IPayService {
    fun prepareGoThirdPartyPay(orderSn: String): AliPayDto

}
