package com.june.order.service.impl

import com.baomidou.mybatisplus.extension.kotlin.KtQueryWrapper
import com.baomidou.mybatisplus.extension.kotlin.KtUpdateWrapper
import com.june.common.constant.OrderConstant
import com.june.common.dto.PlaceOrderDto
import com.june.common.enums.OrderStatus
import com.june.common.enums.PayType
import com.june.common.exception.BusinessException
import com.june.common.exception.ErrorCodeEnum
import com.june.common.utils.UserHolder
import com.june.order.feign.TicketFeignClient
import com.june.order.feign.TrainFeignClient
import com.june.order.pojo.dao.entity.Order
import com.june.order.pojo.dao.entity.OrderItem
import com.june.order.pojo.dao.mapper.OrderItemMapper
import com.june.order.pojo.dao.mapper.OrderMapper
import com.june.order.service.IOrderService
import org.redisson.api.RedissonClient
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.concurrent.CompletableFuture


@Service
class OrderServiceImpl(
    val orderMapper: OrderMapper,
    val orderItemMapper: OrderItemMapper,
    val trainFeignClient: TrainFeignClient,
    val ticketFeignClient: TicketFeignClient,
    val redissonClient: RedissonClient,
) : IOrderService {
    companion object {
        val log: Logger = LoggerFactory.getLogger(OrderServiceImpl::class.java)
    }

    @Value("\${snowflake.worker-id}")
    lateinit var workerId: String

    @OptIn(ExperimentalStdlibApi::class)
//    @Transactional
    override fun placeOrder(dto: PlaceOrderDto): Order? {
        // 检查订单是否超时
        if (dto.createOrderTime == null || dto.createOrderTime.plusMinutes(15)
                .isBefore(LocalDateTime.now())
        ) return null

        val passengerCount = dto.passengers.size
        if (orderMapper.exists(KtQueryWrapper(Order::class.java).eq(Order::orderSn, dto.orderSn))) return null

        val lock = redissonClient.getLock(OrderConstant.LOCK_PLACE_ORDER)
        lock.lock()

        if (orderMapper.exists(KtQueryWrapper(Order::class.java).eq(Order::orderSn, dto.orderSn))) return null

        val ret: Order

        try {
            val future1 = CompletableFuture.supplyAsync {
                trainFeignClient.getTrainStationArrivalDateTime(
                    dto.arrivalStationId,
                    dto.trainId,
                    dto.departureTime.toLocalDate().toString()
                ).data
            }

            val future2 = CompletableFuture.supplyAsync {
                val order = Order(
                    null,
                    dto.orderSn,
                    dto.userId,
                    "2",
                    OrderStatus.PLACE_ORDER,
                    null,
                    PayType.NOT_PAID,
                    null,
                    null,
                    null,
                    dto.orderVersion,
                )
                orderMapper.insert(order)
                order
            }

            ret = future1.thenCombine(future2) { result1, result2 ->
                val futures = arrayOfNulls<CompletableFuture<Void>>(passengerCount)

                for (i in 0..<passengerCount) {
                    val feature = CompletableFuture.runAsync {
                        val orderItem = OrderItem(
                            null,
                            orderId = result2.id,
                            trainId = dto.trainId,
                            carriageId = dto.carriageId,
                            carriageType = dto.carriageType,
                            departureStationId = dto.departureStationId,
                            arrivalStationId = dto.arrivalStationId,
                            seatNumber = dto.seatNumbers[i],
                            passengerId = dto.passengers[i].id,
                            departureDatetime = dto.departureTime,
                            arrivalDatetime = result1,
                            createTime = dto.createOrderTime,
                            updateTime = null,
                            delFlag = null,
                            trainNumber = dto.trainNumber,
                            carriageNumber = dto.carriageNumber,
                            passengerName = dto.passengers[i].name,
                            passengerType = dto.passengers[i].passengerType,
                            passengerPhone = dto.passengers[i].phone,
                            ticketPrice = dto.pricePerTicket,
                            departureStationName = dto.departureStationName,
                            arrivalStationName = dto.arrivalStationName,
                        )

                        orderItemMapper.insert(orderItem)
                        result2.orderItems.add(i, orderItem)
                    }
                    futures[i] = feature
                }
                CompletableFuture.allOf(*futures).get()
                result2
            }.get()
        } finally {
            lock.unlock()
        }

        return ret
    }

    override fun rollbackOrder(dto: PlaceOrderDto) {
        if (orderMapper.update(
                KtUpdateWrapper(Order::class.java)
                    .eq(Order::orderSn, dto.orderSn)
                    .eq(Order::version, dto.orderVersion)
                    .eq(Order::status, OrderStatus.PLACE_ORDER)
                    .set(Order::status, OrderStatus.CANCELED)
                    .set(Order::version, dto.orderVersion + 1)
            ) != 1
        ) {
            log.warn("期望回滚订单状态，但订单版本号不是期望值 或者 订单不存在。")
            val order = orderMapper.selectOne(
                KtQueryWrapper(Order::class.java)
                    .eq(Order::orderSn, dto.orderSn)
            )
            if (order != null && order.status != OrderStatus.PLACE_ORDER && order.status != OrderStatus.CANCELED) {
                log.error("订单状态已变更，回滚中断\n 订单信息:{}", order.toString())
                return
            } else {
                log.debug("订单不存在，可能并未来得及创建订单，执行回滚")
            }
        }

        val future1 = CompletableFuture.runAsync {
            ticketFeignClient.restoreSeatForMySQL(dto)
        }
            .whenComplete { _, throwable ->
                if (throwable != null) {
                    log.error(
                        "已回滚订单状态，但MySQL库存回滚异常！，异常信息：{}\nthrowable.stackTrace:{}",
                        throwable.message,
                        throwable.stackTrace
                    )
                }
            }

        val future2 = CompletableFuture.runAsync {
            ticketFeignClient.restoreSeatForRedis(dto)
        }
            .whenComplete { _, throwable ->
                if (throwable != null) {
                    log.error(
                        "已回滚订单状态，但Redis回滚异常！，异常信息：{}\nthrowable.stackTrace:{}",
                        throwable.message,
                        throwable.stackTrace
                    )
                }
            }

        CompletableFuture.allOf(future1, future2)
    }

    override fun getOrderByOrderSn(orderSn: String): Order {
        val order = orderMapper.selectOne(
            KtQueryWrapper(Order::class.java)
                .eq(Order::orderSn, orderSn)
        )
        order.orderItems = orderItemMapper.selectList(
            KtQueryWrapper(OrderItem::class.java)
                .eq(OrderItem::orderId, order.id)
        )
        return order
    }


    override fun getOrderInfo(orderSn: String): Order {
        val order = orderMapper.selectOne(
            KtQueryWrapper(Order::class.java)
                .eq(Order::orderSn, orderSn)
        )
        if (order == null) throw BusinessException(ErrorCodeEnum.ORDER_NOT_EXIST_BY_ORDER_SN)
        order.orderItems = orderItemMapper.selectList(
            KtQueryWrapper(OrderItem::class.java)
                .eq(OrderItem::orderId, order.id)
        )
        return order
    }

    override fun getOrderInfoList(): Array<Order>? {
        val userId = UserHolder.getCurrentUser().id
        return orderMapper.selectList(
            KtQueryWrapper(Order::class.java)
                .eq(Order::userId, userId)
                .orderByDesc(Order::createTime)
        ).map {
            it.orderItems = orderItemMapper.selectList(
                KtQueryWrapper(OrderItem::class.java)
                    .eq(OrderItem::orderId, it.id)
            )
            it
        }.toTypedArray()
    }

    override fun paySuccess(orderSn: String) {
        val update = orderMapper.update(
            KtUpdateWrapper(Order::class.java)
                .eq(Order::orderSn, orderSn)
                .eq(Order::status, OrderStatus.PLACE_ORDER)
                .set(Order::status, OrderStatus.PAID)
        )
        if (update != 1) {
            val order = orderMapper.selectOne(
                KtQueryWrapper(Order::class.java)
                    .eq(Order::orderSn, orderSn)
                    .eq(Order::status, OrderStatus.PAID)
            )
            if (order == null)
                log.error("用户【已支付】，但订单状态修改异常！")
        }
    }
}

