package com.june.order.feign;

import com.june.common.entity.R;
import com.june.order.pojo.dto.Passenger;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "passenger", path = "/passenger", contextId = "passenger3123")
public interface PassengerFeignClient {
    @GetMapping("passenger")
    R<Passenger> getPassenger(@RequestParam("id") Long id);
}
