package com.june.order.feign;

import com.june.common.dto.PlaceOrderDto;
import com.june.common.entity.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "ticket", path = "/ticket", contextId = "ticket3123")
public interface TicketFeignClient {
    @PostMapping("/seat/restore/redis")
    R<Void> restoreSeatForRedis(PlaceOrderDto dto);

    @PostMapping("/seat/restore/mysql")
    R<Void> restoreSeatForMySQL(PlaceOrderDto dto);
}
