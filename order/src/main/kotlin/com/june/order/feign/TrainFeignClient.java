package com.june.order.feign;

import com.june.common.entity.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDateTime;

@FeignClient(name = "train", path = "/train", contextId = "train123512312")
public interface TrainFeignClient {
    @GetMapping("station-arrival/sequence/{stationId}/{trainId}")
    R<Byte> getStationArrivalSequence(@PathVariable("stationId") Long stationId, @PathVariable("trainId") Long trainId);

    @GetMapping("station-arrival/arrival-datetime/{stationId}/{trainId}/{departureDate}")
    R<LocalDateTime> getTrainStationArrivalDateTime(@PathVariable("stationId") Long stationId,
                                                    @PathVariable("trainId") Long trainId,
                                                    @PathVariable("departureDate") String departureDate
    );
}
