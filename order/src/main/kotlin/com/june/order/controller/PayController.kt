package com.june.order.controller

import com.june.common.dto.PlaceOrderDto
import com.june.common.entity.R
import com.june.order.config.AlipayTemplate
import com.june.order.pojo.dao.entity.Order
import com.june.order.pojo.dto.AliPayDto
import com.june.order.service.IOrderService
import com.june.order.service.IPayService
import jakarta.servlet.http.HttpServletResponse
import org.springframework.web.bind.annotation.*

@RestController
class PayController(
    val payServiceImpl: IPayService,
    val alipayTemplate: AlipayTemplate,
    val orderServiceImpl: IOrderService,
) {
    @GetMapping("/pay")
    fun pay(@RequestParam("orderSn") orderSn: String, response: HttpServletResponse) {
        val dto: AliPayDto = payServiceImpl.prepareGoThirdPartyPay(orderSn)
        val pay = alipayTemplate.pay(dto)

        response.setHeader("Content-Type", "text/html;charset=UTF-8")
        response.writer.write(pay)
        response.writer.close()
    }

    /**
     * 示例：total_amount=2.00&buyer_id=20****7&body=大乐透2.1&trade_no=2016071921001003030200089909&refund_fee=0.00&notify_time=2016-07-19 14:10:49&subject=大乐透2.1&sign_type=RSA2&charset=utf-8&notify_type=trade_status_sync&out_trade_no=0719141034-6418&gmt_close=2016-07-19 14:10:46&gmt_payment=2016-07-19 14:10:47&trade_status=TRADE_SUCCESS&version=1.0&sign=kPbQIjX+xQc8F0/A6/AocELIjhhZnGbcBN6G4MM/HmfWL4ZiHM6fWl5NQhzXJusaklZ1LFuMo+lHQUELAYeugH8LYFvxnNajOvZhuxNFbN2LhF0l/KL8ANtj8oyPM4NN7Qft2kWJTDJUpQOzCzNnV9hDxh5AaT9FPqRS6ZKxnzM=&gmt_create=2016-07-19 14:10:44&app_id=20151*****3&seller_id=20881021****8&notify_id=4a91b7a78a503640467525113fb7d8bg8e
     */
    @RequestMapping("/pay/success-callback")
    fun paySuccessCallback(@RequestParam("out_trade_no") orderSn: String) {
        orderServiceImpl.paySuccess(orderSn)
    }


    @GetMapping("/order/all")
    fun getOrderInfoList(): R<Array<Order>> {
        return R.ok(orderServiceImpl.getOrderInfoList())
    }

    @GetMapping("/order/{orderSn}")
    fun getOrderInfo(@PathVariable orderSn: String): R<Order> {
        return R.ok(orderServiceImpl.getOrderInfo(orderSn))
    }

    @PostMapping("/order/place-order")
    fun placeOrder(@RequestBody dto: PlaceOrderDto): R<Void> {
        orderServiceImpl.placeOrder(dto)
        return R.ok()
    }

}
