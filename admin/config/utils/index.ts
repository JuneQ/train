/**
 * Whether to generate package preview
 * 是否生成打包报告
 */
export default {};

export function isReportMode(): boolean {
    return process.env.REPORT === 'true';
}

export function defaultIfNull<T>(value: T | null | undefined, defaultValue: T): T {
    return value === null || value === undefined ? defaultValue : value;
}
