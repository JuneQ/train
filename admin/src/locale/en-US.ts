import localeMessageBox from '@/components/message-box/locale/en-US';
import localeLogin from '@/views/login/locale/en-US';

import localeSettings from './en-US/settings';

export default {
    'menu.dashboard': 'Dashboard',
    'menu.dashboard.monitor': 'Monitor',


    'menu.ticket': 'TicketService',
    'menu.ticket.buy': 'Ticketing',

    'menu.order': 'OrderService',
    'menu.order.info': 'Select',

    'menu.passenger': 'PassengerService',
    'menu.passenger.info': 'Info',


    'menu.station': "车站服务",
    'menu.station.info': '车站管理',
    'menu.region.info': '地区管理',


    'menu.train.info': '列车信息',
    'menu.train.carriage': '车厢信息',
    'menu.train.seat': '座位信息',


    'menu.server.dashboard': 'Dashboard-Server',
    'menu.server.workplace': 'Workplace-Server',
    'menu.server.monitor': 'Monitor-Server',
    'menu.list': 'List',
    'menu.result': 'Result',
    'menu.exception': 'Exception',
    'menu.form': 'Form',
    'menu.profile': 'Profile',
    'menu.visualization': 'Data Visualization',
    'menu.user': 'User Center',
    'menu.arcoWebsite': 'Arco Design',
    'menu.faq': 'FAQ',
    'navbar.docs': 'Docs',
    'navbar.action.locale': 'Switch to English',
    ...localeSettings,
    ...localeMessageBox,
    ...localeLogin,
};
