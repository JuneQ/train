import {defineStore} from "pinia";
import {TrainState} from "@/store/modules/train/types";

const useTrainStore = defineStore("train", {
    state: (): TrainState => ({
        simpleTrainSelectCondition: {
            keyword: '',
        },
        simpleCarriageSelectCondition: {
            keyword: ''
        },
    }),

    getters: {
        passengerInfos(state: TrainState): TrainState {
            return {...state};
        }
    },
    actions: {
        resetInfo() {
            this.$reset();
        },

        getTrainPage() {

        }

        // async info() {
        //     const res = await getUserInfo();
        //     this.setInfo(res.data);
        // },
    }
});

export default useTrainStore;
