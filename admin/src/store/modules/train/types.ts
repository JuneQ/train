import {Station} from "@/store/modules/station/types";

export interface SimpleTrainSelectCondition extends Page {
    keyword: string;
}

export interface SimpleCarriageSelectCondition extends Page {
    keyword: string;
}

export interface TrainState {
    simpleTrainSelectCondition: SimpleTrainSelectCondition;
    simpleCarriageSelectCondition: SimpleCarriageSelectCondition;
}

export interface TrainStationArrival {
    id?: string,
    trainId?: string,
    stationId?: string,
    sequence?: number,
    arrivalTime?: string,
    stopoverTime?: number, // minutes
    kthFromDepartureDay?: number

    station?: Station,
    train?: Train,
}

export interface Carriage {
    id?: string,
    trainId?: string,
    carriageNumber?: string,
    carriageType?: CarriageType,
    seatCount?: string,
    createTime?: string,
    updateTime?: string,

    train?: Train
}


// 0 HARD_SEAT_CAR 硬座车厢
// 1 SOFT_SEAT_CAR 软座车厢
// 2 HARD_SLEEPER_CAR 硬卧车厢
// 3 SOFT_SLEEPER_CAR 软卧车厢
// 4 DELUXE_SOFT_SLEEPER_CAR 高级软卧车厢
// 5 BUSINESS_CLASS_CAR 商务座车厢
// 6 FIRST_CLASS_CAR 特等座车厢
export enum CarriageType {
    HARD_SEAT_CAR,
    SOFT_SEAT_CAR,
    HARD_SLEEPER_CAR,
    SOFT_SLEEPER_CAR,
    DELUXE_SOFT_SLEEPER_CAR,
    BUSINESS_CLASS_CAR,
    FIRST_CLASS_CAR,
}

export function carriageTypeToString(carriageType: CarriageType) {
    switch (carriageType) {
        case CarriageType.HARD_SEAT_CAR:
            return "硬座车厢";
        case CarriageType.SOFT_SEAT_CAR:
            return "软座车厢";
        case CarriageType.HARD_SLEEPER_CAR:
            return "硬卧车厢";
        case CarriageType.SOFT_SLEEPER_CAR:
            return "软卧车厢";
        case CarriageType.DELUXE_SOFT_SLEEPER_CAR:
            return "高级软卧车厢";
        case CarriageType.BUSINESS_CLASS_CAR:
            return "商务座车厢";
        case CarriageType.FIRST_CLASS_CAR:
            return "特等座车厢";
    }
}

export interface Train {
    id?: string,
    trainNumber?: string,
    trainType?: TrainType,
    startStationId?: string,
    endStationId?: string,
    dayCrossed?: number,
    arrivalTime?: string,
    departureTime?: string,
    createTime?: string,
    updateTime?: string,
    availableFlag?: boolean,

    startStation?: Station,
    endStation?: Station
}

// 0 高速列车 High-Speed Train
// 1 城际列车 Intercity Train
// 2 动车组 Multiple Unit Train
// 3 直达列车 Through Train
// 4 特快列车 Express Train
// 5 快速列车 Rapid Train
// 6 普通列车 Ordinary Train
// 7 货物列车 Freight Train
// 8 旅游列车 Tourist Train
export enum TrainType {
    HIGH_SPEED_TRAIN,
    INTERCITY_TRAIN,
    MULTIPLE_UNIT_TRAIN,
    THROUGH_TRAIN,
    EXPRESS_TRAIN,
    RAPID_TRAIN,
    ORDINARY_TRAIN,
    FREIGHT_TRAIN,
    TOURIST_TRAIN,
}

export function trainTypeToString(trainType: TrainType) {
    switch (trainType) {
        case TrainType.HIGH_SPEED_TRAIN:
            return "高速列车";
        case TrainType.INTERCITY_TRAIN:
            return "城际列车";
        case TrainType.MULTIPLE_UNIT_TRAIN:
            return "动车组";
        case TrainType.THROUGH_TRAIN:
            return "直达列车";
        case TrainType.EXPRESS_TRAIN:
            return "特快列车";
        case TrainType.RAPID_TRAIN:
            return "快速列车";
        case TrainType.ORDINARY_TRAIN:
            return "普通列车";
        case TrainType.FREIGHT_TRAIN:
            return "货物列车";
        case TrainType.TOURIST_TRAIN:
            return "旅游列车";
    }
}
