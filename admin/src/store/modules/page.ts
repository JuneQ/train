interface OrderItem {
    column?: string
    asc?: boolean  // true升序，false降序
}

interface Page {
    current?: number
    size?: number
    total?: number
    orders?: Array<OrderItem>
}
