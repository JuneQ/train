import {CarriageType} from "@/store/modules/train/types";
import {PassengerType} from "@/store/modules/passenger/types";


export interface Order {
    id?: string,
    orderSn?: string,
    source?: string,
    status?: OrderStatus,
    payTime?: Date,
    payType?: PayType,
    createTime?: Date,
    updateTime?: Date,
    version?: string,
    orderItems: Array<OrderItem>,
}

// 订单状态: 0已下单 1已付款 2已完成 3有退款 4已取消
// 0 ordered 1 paid 2 completed 3 refunded 4 cancelled
export enum OrderStatus {
    PLACE_ORDER,
    PAID,
    COMPLETED,
    HAS_REFUND,
    CANCELED,
}

export function orderStatusToString(o: OrderStatus) {
    switch (o) {
        case OrderStatus.CANCELED:
            return "已取消";
        case OrderStatus.PAID:
            return "已支付";
        case OrderStatus.COMPLETED:
            return "已完成";
        case OrderStatus.HAS_REFUND:
            return "有退款";
    }
}

// 支付方式: 0支付宝 1微信 2银行卡 3现金 4其他
//  0 Alipay 1 WeChat 2 Bank card 3 Cash 4 Others
export enum PayType {
    ALIPAY,
    WECHAT,
    BANKCARD,
    CASH,
    OTHERS,
}

export function payTypeToString(p: PayType) {
    switch (p) {
        case PayType.ALIPAY:
            return "支付宝";
        case PayType.WECHAT:
            return "微信";
        case PayType.BANKCARD:
            return "银行卡";
        case PayType.CASH:
            return "现金";
        case PayType.OTHERS:
            return "其他";
    }
}


export interface OrderItem {
    id: string,
    orderId: string,
    trainId: string,
    trainNumber: string,
    carriageId: string,
    carriageType: CarriageType,
    carriageNumber: string,
    departureStationId: string,
    arrivalStationId: string,
    departureStationName: string,
    arrivalStationName: string,
    seatNumber: number,
    passengerId: string,
    passengerName: string,
    passengerType: PassengerType,
    passengerPhone: string,
    ticketPrice: string,
    departureDatetime: Date,
    arrivalDatetime: Date,
}


export interface OrderState {
    payOrder: Order,
    orderInfoList: Array<Order>
}
