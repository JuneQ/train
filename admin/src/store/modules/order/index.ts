import {defineStore} from "pinia";
import {Order, OrderState} from "@/store/modules/order/types";
import {getOrderInfo, getOrderInfoList} from "@/api/order";


const useOrderStore = defineStore("order", {
    state: (): OrderState => ({
        payOrder: {} as Order,
        orderInfoList: []
    }),
    getters: {},
    actions: {
        resetInfo() {
            this.$reset();
        },
        refreshPayOrder(orderSn: string) {
            getOrderInfo(orderSn).then(r => {
                this.payOrder = r.data;
            }).catch(e => {
            });
        },
        refreshOrderInfoList() {
            getOrderInfoList().then(r => {
                this.orderInfoList = r.data;
            });
        }
    }
});

export default useOrderStore;
