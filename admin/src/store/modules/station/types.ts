export interface SimpleStationSelectCondition extends Page {
    keyword: string
}

export interface SimpleRegionSelectCondition extends Page {
    keyword: string
}

export interface Station extends Page {
    id?: string
    name?: string
    namePinyin?: string
    code?: string
    regionId?: string
    createTime?: Date
    updateTime?: Date
    region?: Region
}

export interface Region {
    id?: string
    name?: string
    fullName?: string
    code?: string
    initial?: string
    pinyin?: string
    hotFlag?: boolean
    createTime?: Date
    updateTime?: Date
}

export interface StationState {
    simpleStationSelectCondition: SimpleStationSelectCondition,
    simpleRegionSelectCondition: SimpleRegionSelectCondition,
}

