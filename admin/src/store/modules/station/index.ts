import {defineStore} from "pinia";
import {StationState} from "@/store/modules/station/types";
import {getRegionByPage, getStationByPage} from "@/api/train";

// 只存储查询条件，不存储表格数据

const useStationStore = defineStore('station', {
    state: (): StationState => ({
        simpleStationSelectCondition: {
            size: 10,
            current: 1,
            total: undefined,
            keyword: ''
        },
        simpleRegionSelectCondition: {
            size: 10,
            current: 1,
            total: undefined,
            keyword: ''
        }
    }),

    getters: {
        // conditionInfo(state: any): SimpleStationSelectCondition {
        //     return state.simpleSelectCondition;
        // },
    },
    actions: {
        reset() {
            this.$reset();
        },

        pageRegion() {
            return getRegionByPage(this.simpleRegionSelectCondition)
        },

        pageStation() {
            return getStationByPage(this.simpleRegionSelectCondition)
        },
    },
});

export default useStationStore;
