/**
 * 以下枚举值与后端严格一致
 */
export enum IDType {
    IDENTITY_CARD,
    PASSPORT,
    MAINLAND_TRAVEL_PERMIT_FOR_HONG_KONG_AND_MACAO_RESIDENTS,
    MAINLAND_TRAVEL_PERMIT_FOR_TAIWAN_RESIDENTS,
    MILITARY_OFFICER_ID,
}


export enum PassengerType {
    CHILD,
    STUDENT,
    ADULT,
}

export const passengerTypeToString = (type: PassengerType): string => {
    switch (type) {
        case PassengerType.CHILD:
            return "儿童";
        case PassengerType.STUDENT:
            return "学生";
        case PassengerType.ADULT:
            return "成人";
    }
};

export const idTypeToString = (type: IDType): string => {
    switch (type) {
        case IDType.IDENTITY_CARD:
            return "居民身份证";
        case IDType.PASSPORT:
            return "护照";
        case IDType.MAINLAND_TRAVEL_PERMIT_FOR_HONG_KONG_AND_MACAO_RESIDENTS:
            return "港澳通行证";
        case IDType.MAINLAND_TRAVEL_PERMIT_FOR_TAIWAN_RESIDENTS:
            return "台湾通行证";
        case IDType.MILITARY_OFFICER_ID:
            return "军官证";
    }
};

export enum VerifyStatus {
    PASSED,
    UNDER_REVIEW,
    FAILED,
}

export interface Passenger {
    id?: string;
    userId?: string;
    name?: string;
    phone?: string;
    idType?: IDType,
    passengerType?: PassengerType,
    idCard?: number,
    verifyStatus?: VerifyStatus,
}

export interface PassengerState {
    passengers: Array<Passenger>;
}

