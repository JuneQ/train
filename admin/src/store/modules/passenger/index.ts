import {defineStore} from "pinia";
import {Passenger, PassengerState} from "./types";
import {getPassengersByCurrentLoginUser} from "@/api/passenger";

const usePassengerStore = defineStore('passenger', {
    state: (): PassengerState => ({
        passengers: []
    }),

    getters: {
        passenger(state: PassengerState): Array<Passenger> {
            return state.passengers;
        },
    },
    actions: {
        // setInfo(partial: Partial<PassengerState>) {
        //     this.$patch(partial);
        // },

        resetInfo() {
            this.$reset();
        },

        refreshData() {
            getPassengersByCurrentLoginUser().then(r => {
                this.passengers = r.data
            })
        },

        // async info() {
        //     const res = await getUserInfo();
        //     this.setInfo(res.data);
        // },
    },
});

export default usePassengerStore;
