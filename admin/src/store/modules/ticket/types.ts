import {Region} from "@/store/modules/station/types";
import {CarriageType, TrainType} from "@/store/modules/train/types";
import {Passenger} from "@/store/modules/passenger/types";

export interface TicketSelectCondition {
    departureRegionId?: string,
    arrivalRegionId?: string,
    departureDate?: string
}

export interface TicketView {
    tickets: number,
    trainId: string,
    departureDatetime: Date,
    arrivalDatetime: Date,
    departureStationSequence: number,
    arrivalStationSequence: number,
    departureStationId: string,
    arrivalStationId: string,
    carriageId: string,
    carriageType: CarriageType,
    carriageNumber: number,
    trainNumber: string,
    trainType: TrainType,
}

export interface BuyTicketInfo {
    departureStationId?: string,
    departureStationSequence?: string,
    arrivalStationId?: string,
    arrivalStationSequence?: string,
    departureStationName?: string,
    arrivalStationName?: string,
    trainId?: string,
    trainNumber?: string,
    carriageType?: CarriageType,
    passengers?: Array<Passenger>,
    departureDatetime?: Date,
    arrivalDatetime?: Date,
    firstStationDepartureDate?: string,
    price?: string,
    carriageNumber?: string,
}

export interface OrderInfo {
    orderSn?: string,
    orderItems?: Array<OrderItem>,
}

export interface OrderItem {

}

export interface BuyTicketReturn {
    orderSn: string,
    seatNumbers: Array<number>,
}

export interface TicketState {
    passengers?: Array<Passenger>
    regions?: Array<Region>,
    searchCondition?: TicketSelectCondition,
    trains?: Array<TicketView>,
    buyTicketInfo: BuyTicketInfo,
    successSeatNumbers?: Array<number>,
}
