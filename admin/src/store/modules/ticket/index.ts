import {defineStore} from "pinia";
import {TicketSelectCondition, TicketState} from "@/store/modules/ticket/types";
import {getRegions} from "@/api/train";
import {searchTickets} from "@/api/ticket";
import dayjs from "dayjs";
import {Message} from "@arco-design/web-vue";
import {getPassengersByCurrentLoginUser} from "@/api/passenger";


const useTicketStore = defineStore("ticket", {
    state: (): TicketState => ({
        regions: [],
        passengers: [],
        searchCondition: {
            departureDate: dayjs().add(1, 'days').format("YYYY-MM-DD")
        },
        trains: [],
        buyTicketInfo: {},
        successSeatNumbers: []
    }),
    getters: {},
    actions: {
        resetInfo() {
            this.$reset();
        },

        refreshRegions() {
            getRegions().then(r => {
                this.regions = r.data;
            });
        },

        refreshPassengers() {
            getPassengersByCurrentLoginUser().then(r => {
                this.passengers = r.data;
            });
        },

        async searchTicket() {
            return searchTickets(<TicketSelectCondition>this.searchCondition).then(r => {
                this.trains = r.data;
                Message.success("查询成功");
            });
        }

    }
});

export default useTicketStore;
