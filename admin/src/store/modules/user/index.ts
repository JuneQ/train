import {defineStore} from 'pinia';
import {
    checkToken,
    generateNewToken,
    getUserInfo,
    login as userLogin,
    LoginData,
    logout as userLogout,
    sendCode,
    updateUserInfo
} from '@/api/user';
import {clearToken, getToken, setToken} from '@/utils/auth';
import {removeRouteListener} from '@/utils/route-listener';
import {UserState} from './types';
import useAppStore from '../app';

const useUserStore = defineStore('user', {
    state: (): UserState => ({
        id: undefined,
        nickname: undefined,
        avatar: undefined,
        phone: undefined,
        email: undefined,
        region: undefined,
        address: undefined,
        registrationDate: undefined,
        role: '',
    }),

    getters: {
        userInfo(state: UserState): UserState {
            return {...state};
        },
    },

    actions: {
        switchRoles() {
            return new Promise((resolve) => {
                this.role = this.role === 'user' ? 'admin' : 'user';
                resolve(this.role);
            });
        },

        async updateInfo(user: UserState) {
            return updateUserInfo(user).then(async r => {
                const oldToken = getToken()
                generateNewToken(oldToken || '').then(r => {
                    console.log("r.data", r.data);
                    setToken(r.data)
                    this.$patch(user);
                })
            })
        },

        resetInfo() {
            this.$reset();
        },

        async info() {
            const res = await getUserInfo();
            this.$patch(res.data)
        },

        async sendCode(phone: number) {
            await sendCode(phone);
        },


        // Login
        async login(loginForm: LoginData) {
            try {
                const res = await userLogin(loginForm);
                setToken(res.data);

            } catch (err) {
                clearToken();
                throw err;
            }
        },
        logoutCallBack() {
            const appStore = useAppStore();
            this.resetInfo();
            clearToken();
            removeRouteListener();
            appStore.clearServerMenu();
        },
        // Logout
        async logout() {
            try {
                await userLogout();
            } finally {
                this.logoutCallBack();
            }
        },

        async checkToken(token: string) {
            const res = await checkToken(token);
            return res.data
        },
    },
});

export default useUserStore;
