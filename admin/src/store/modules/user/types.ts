export type RoleType = '*' | '' | 'admin' | 'user';

export interface UserState {
    id?: string;
    nickname?: string;
    avatar?: string;
    email?: string;
    phone?: string;
    registrationDate?: string;
    region?: string,
    address?: string,

    role: RoleType;
}
