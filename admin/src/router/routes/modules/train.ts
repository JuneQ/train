import {DEFAULT_LAYOUT} from '../base';
import {AppRouteRecordRaw} from '../types';

const TRAIN: AppRouteRecordRaw = {
    path: '/train',
    name: 'train',
    component: DEFAULT_LAYOUT,
    meta: {
        locale: 'menu.train',
        requiresAuth: true,
        icon: 'icon-desktop',
        order: 3,
    },
    children: [
        {
            path: 'info',
            name: 'TrainInfo',
            component: () => import('@/views/train/info/index.vue'),
            meta: {
                locale: 'menu.train.info',
                requiresAuth: true,
                roles: ['*'],
            },
        },
        {
            path: 'carriage',
            name: 'TrainCarriage',
            component: () => import('@/views/train/carriage/index.vue'),
            meta: {
                locale: 'menu.train.carriage',
                requiresAuth: true,
                roles: ['*'],
            },
        },
    ],
};

export default TRAIN;
