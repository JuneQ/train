import {DEFAULT_LAYOUT} from '../base';
import {AppRouteRecordRaw} from '../types';

const USER: AppRouteRecordRaw = {
    path: '/user',
    name: 'user',
    component: DEFAULT_LAYOUT,
    meta: {
        hideInMenu: true,
        requiresAuth: true,
        icon: 'icon-desktop',
        order: 5,
    },
    children: [
        {
            path: 'center',
            name: 'UserInfo',
            component: () => import('@/views/user/info/index.vue'),
            meta: {
                locale: 'menu.user.info',
                requiresAuth: true,
                roles: ['*'],
                hideInMenu: true,
            },
        },
    ],
};

export default USER;
