import {DEFAULT_LAYOUT} from '../base';
import {AppRouteRecordRaw} from '../types';

const PASSENGER: AppRouteRecordRaw = {
    path: '/passenger',
    name: 'passenger',
    component: DEFAULT_LAYOUT,
    meta: {
        locale: 'menu.passenger',
        requiresAuth: true,
        icon: 'icon-idcard',
        order: 54,
    },
    children: [
        {
            path: 'info',
            name: 'PassengerInfo',
            component: () => import('@/views/passenger/info/index.vue'),
            meta: {
                locale: 'menu.passenger.info',
                requiresAuth: true,
                roles: ['*'],
            },
        },
    ],
};

export default PASSENGER;
