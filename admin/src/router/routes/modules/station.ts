import {DEFAULT_LAYOUT} from '../base';
import {AppRouteRecordRaw} from '../types';

const STATION: AppRouteRecordRaw = {
    path: '/station',
    name: 'station',
    component: DEFAULT_LAYOUT,
    meta: {
        locale: 'menu.station',
        requiresAuth: true,
        icon: 'icon-idcard',
        order: 55,
    },
    children: [
        {
            path: 'station',
            name: 'StationInfo',
            component: () => import('@/views/station/info/index.vue'),
            meta: {
                locale: 'menu.station.info',
                requiresAuth: true,
                roles: ['*'],
            },
        },
        {
            path: 'region',
            name: 'RegionInfo',
            component: () => import('@/views/station/region/index.vue'),
            meta: {
                locale: 'menu.region.info',
                requiresAuth: true,
                roles: ['*'],
            },
        },
    ],
};

export default STATION;
