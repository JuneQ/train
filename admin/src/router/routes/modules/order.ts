import {DEFAULT_LAYOUT} from '../base';
import {AppRouteRecordRaw} from '../types';

const ORDER: AppRouteRecordRaw = {
    path: '/order',
    name: 'order',
    component: DEFAULT_LAYOUT,
    meta: {
        locale: 'menu.order',
        requiresAuth: true,
        icon: 'icon-desktop',
        order: 1,
    },
    children: [
        {
            path: 'info',
            name: 'OrderInfo',
            component: () => import('@/views/order/info/index.vue'),
            meta: {
                locale: 'menu.order.info',
                requiresAuth: true,
                roles: ['*'],
            },
        },
        {
            path: 'pay/:orderSn',
            name: 'PayTicket',
            component: () => import('@/views/order/pay/index.vue'),
            meta: {
                locale: 'menu.order.info',
                requiresAuth: true,
                hideInMenu: true,
                roles: ['*'],
            },
        },
    ],
};

export default ORDER;
