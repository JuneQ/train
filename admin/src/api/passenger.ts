import axios from 'axios';
import {Passenger} from "@/store/modules/passenger/types";


export function addPassenger(passenger: Passenger) {
    return axios.post<string>('/api/passenger/passenger', passenger);
}

export function deletePassenger(id: string) {
    return axios.delete<void>('/api/passenger/passenger', {params: {id}});
}

export function updatePassenger(passenger: Passenger) {
    return axios.put<void>('/api/passenger/passenger', passenger);
}


export function getPassengersByCurrentLoginUser() {
    return axios.get<Array<Passenger>>('/api/passenger/passenger/all');
}
