import axios from "axios";
import {BuyTicketInfo, BuyTicketReturn, TicketSelectCondition, TicketView} from "@/store/modules/ticket/types";

export function searchTickets(condition: TicketSelectCondition) {
    return axios.get<Array<TicketView>>('/api/ticket/remaining-tickets/' + condition.departureRegionId + '/' + condition.arrivalRegionId + '/' + condition.departureDate);
}

export function buyTicket(vo: BuyTicketInfo) {
    return axios.post<BuyTicketReturn>('/api/ticket/remaining-tickets', vo);
}


