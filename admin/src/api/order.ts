import axios from "axios";
import {Order} from "@/store/modules/order/types";

export function getOrderInfo(orderSn: string) {
    return axios.get<Order>('/api/order/order/' + orderSn);
}

export function getOrderInfoList() {
    return axios.get<Array<Order>>('/api/order/order/all');
}


