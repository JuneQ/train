import axios from "axios";
import {
    Region,
    SimpleRegionSelectCondition,
    SimpleStationSelectCondition,
    Station
} from "@/store/modules/station/types";
import {Carriage, SimpleCarriageSelectCondition, Train, TrainStationArrival} from "@/store/modules/train/types";


export function updateTrainStationArrival(a: TrainStationArrival) {
    return axios.put<void>("/api/train/station-arrival", a);
}


export function deleteTrainStationArrival(id: string) {
    return axios.delete<void>("/api/train/station-arrival", {params: {id}});
}


export function addTrainStationArrival(a: TrainStationArrival) {
    return axios.post<string>("/api/train/station-arrival", a);
}


export function getTrainStationArrivals(id: string) {
    return axios.get<Array<TrainStationArrival>>("/api/train/station-arrival", {params: {id}});
}


export function addCarriage(carriage: Carriage) {
    return axios.post<string>("/api/train/carriage", carriage);
}


export function updateCarriage(carriage: Carriage) {
    return axios.put<void>("/api/train/carriage", carriage);
}


export function deleteCarriage(id: string) {
    return axios.delete<void>("/api/train/carriage", {params: {id}});
}


export function getCarriageByPage(condition: SimpleCarriageSelectCondition) {
    return axios.get<Array<Carriage>>("/api/train/carriage/page", {params: {"params": JSON.stringify(condition)}});
}


export function getStations() {
    return axios.get<Array<Station>>("/api/train/station/all");
}


export function addTrain(train: Train) {
    return axios.post<string>("/api/train/train", train);
}


export function updateTrain(train: Train) {
    return axios.put<void>("/api/train/train", train);
}


export function deleteTrain(id: string) {
    return axios.delete<void>("/api/train/train", {params: {id}});
}


export function getTrainByPage(condition: SimpleStationSelectCondition) {
    return axios.get<Array<Train>>("/api/train/train", {params: {"params": JSON.stringify(condition)}});
}


export function addRegion(region: Region) {
    return axios.post<string>("/api/train/region", region);
}


export function updateRegion(region: Region) {
    return axios.put<void>("/api/train/region", region);
}


export function deleteRegion(id: string) {
    return axios.delete<void>("/api/train/region", {params: {id}});
}


export function getRegionByPage(condition: SimpleRegionSelectCondition) {
    return axios.get<Array<Region>>("/api/train/region/page", {params: {"params": JSON.stringify(condition)}});
}


export function addStation(station: Station) {
    return axios.post<string>("/api/train/station", station);
}


export function getRegions() {
    return axios.get<Array<Region>>("/api/train/region/all");
}


export function updateStation(station: Station) {
    return axios.put<void>("/api/train/station", station);
}


export function getStationByPage(condition: SimpleStationSelectCondition) {
    return axios.get<Array<Station>>("/api/train/station", {params: {"params": JSON.stringify(condition)}});
}

export function deleteStation(id: string) {
    return axios.delete<void>("/api/train/station", {params: {id}});
}
