package com.june.passenger;

import com.june.passenger.pojo.dao.mapper.PassengerMapper;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

@SpringBootTest
class PassengerApplicationTests {
    @Resource
    private PassengerMapper passengerMapper;
    @Resource
    private Environment environment;

    @Test
    void contextLoads() {
        System.out.println(environment.getProperty("jwt.secret-key"));
    }
}
