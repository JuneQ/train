package com.june.passenger.pojo.vo;

import com.june.common.enums.IdType;
import com.june.common.enums.PassengerType;
import com.june.passenger.pojo.vo.group.Add;
import com.june.passenger.pojo.vo.group.Update;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;

public record AddOrUpdatePassengerVo(
        @NotNull(groups = Update.class)
        Long id,
        @NotNull(groups = {Add.class, Update.class})
        String userId,
        @NotNull(message = "证件类型不能为空", groups = {Add.class, Update.class})
        IdType idType,
        @Pattern(regexp = "^([1-6][1-9]|50)\\d{4}(18|19|20)\\d{2}((0[1-9])|10|11|12)(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$", message = "身份证号不符合规则", groups = {Add.class, Update.class})
        String idCard,
        @NotNull(message = "姓名不能为空", groups = {Add.class, Update.class})
        String name,
        @Pattern(regexp = "^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\\d{8}$", message = "手机号不符合规则", groups = {Add.class, Update.class})
        String phone,
        @NotNull(message = "旅客类型不能为空", groups = {Add.class, Update.class})
        PassengerType passengerType
) {
}
