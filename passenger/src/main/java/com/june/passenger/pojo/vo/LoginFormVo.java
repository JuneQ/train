package com.june.passenger.pojo.vo;

public record LoginFormVo(String phone, String code) {
}
