package com.june.passenger.pojo.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.june.passenger.pojo.dao.entity.Passenger;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 乘车人 Mapper 接口
 * </p>
 *
 * @author
 */
@Mapper
public interface PassengerMapper extends BaseMapper<Passenger> {

}
