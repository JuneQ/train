package com.june.passenger.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.june.passenger.pojo.dao.entity.Passenger;
import com.june.passenger.pojo.vo.AddOrUpdatePassengerVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 乘车人 服务类
 * </p>
 *
 * @author
 */
public interface IPassengerService extends IService<Passenger> {

    String serviceReturn(String param);

    Long addPassenger(AddOrUpdatePassengerVo addOrUpdatePassengerVo);

    void updatePassenger(AddOrUpdatePassengerVo addOrUpdatePassengerVo);

    void deletePassenger(Long id);

    Passenger getPassenger(Long id);

    String uploadImage(MultipartFile file);

    List<Passenger> getPassengers();
}
