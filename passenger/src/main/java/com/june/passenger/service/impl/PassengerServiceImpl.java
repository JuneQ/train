package com.june.passenger.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.june.common.utils.UserHolder;
import com.june.passenger.pojo.dao.entity.Passenger;
import com.june.passenger.pojo.dao.mapper.PassengerMapper;
import com.june.passenger.pojo.vo.AddOrUpdatePassengerVo;
import com.june.passenger.service.IPassengerService;
import com.june.passenger.utils.COSClientUtil;
import jakarta.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 乘车人 服务实现类
 * </p>
 *
 * @author june
 */
@Service
public class PassengerServiceImpl extends ServiceImpl<PassengerMapper, Passenger> implements IPassengerService {
    @Resource
    private PassengerMapper passengerMapper;

    @Override
    @Cacheable(key = "#param", cacheNames = {"ABC", "BCD"})
    public String serviceReturn(String param) {
        return "success";
    }

    @Override
    public Long addPassenger(AddOrUpdatePassengerVo vo) {
        Passenger p = new Passenger();
        BeanUtils.copyProperties(vo, p);
        p.setUserId(UserHolder.getCurrentUser().id());
        if (passengerMapper.insert(p) == 1) {
            return p.getId();
        }
        throw new RuntimeException("addPassenger 插入失败");

    }

    @Override
    public void updatePassenger(AddOrUpdatePassengerVo vo) {
        Passenger p = new Passenger();
        BeanUtils.copyProperties(vo, p);
        if (passengerMapper.updateById(p) != 1) {
            throw new RuntimeException("updatePassenger 更新失败");
        }
    }

    @Override
    public void deletePassenger(Long id) {
        baseMapper.deleteById(id);
    }

    @Override
    public Passenger getPassenger(Long id) {
        return baseMapper.selectById(id);
    }

    @Override
    public String uploadImage(MultipartFile file) {
        return COSClientUtil.upload(file);
    }

    @Override
    public List<Passenger> getPassengers() {
        return passengerMapper.selectList(new LambdaQueryWrapper<>(Passenger.class)
                .eq(Passenger::getUserId, UserHolder.getCurrentUser().id())
                .orderBy(true, true, List.of(Passenger::getVerifyStatus))
        );
    }
}
