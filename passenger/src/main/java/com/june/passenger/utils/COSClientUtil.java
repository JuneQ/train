package com.june.passenger.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.IdUtil;
import com.june.common.exception.BusinessException;
import com.june.common.exception.ErrorCodeEnum;
import com.june.passenger.config.COSConfig;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.UploadResult;
import com.qcloud.cos.region.Region;
import com.qcloud.cos.transfer.TransferManager;
import com.qcloud.cos.transfer.Upload;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import static cn.hutool.extra.spring.SpringUtil.getBean;

/**
 * 腾讯云COS文件上传工具类
 */
@Slf4j
@SuppressWarnings("all")
public class COSClientUtil {

    /**
     * 获取配置信息
     */
    private static final COSConfig COS_PROPERTIES = getBean(COSConfig.class);

    /**
     * 初始化用户身份信息
     */
    private static final COSCredentials CRED = new BasicCOSCredentials(COS_PROPERTIES.getSecretId(), COS_PROPERTIES.getSecretKey());

    /**
     * 设置bucket的区域
     */
    private static final ClientConfig CLIENT_CONFIG = new ClientConfig(new Region(COS_PROPERTIES.getRegion()));

    /**
     * 生成COS客户端
     */
    private static final COSClient COS_CLIENT = new COSClient(CRED, CLIENT_CONFIG);

    /**
     * 上传文件
     */
    public static String upload(MultipartFile file) {
        String date = DateUtil.format(new Date(), "yyyy-MM-dd");
        String originalFilename = file.getOriginalFilename();
        long nextId = IdUtil.getSnowflake().nextId();
        Assert.notBlank(originalFilename);

        String fileName = nextId + originalFilename.substring(originalFilename.lastIndexOf("."));
        String fullPrefix = COS_PROPERTIES.getFolder() + date + "/" + IdUtil.randomUUID() + "-";
        String fullPath = fullPrefix + fileName;
        File localFile = null;
        try {
            localFile = transferToFile(file);

            String filePath = uploadFileToCOS(localFile, fullPath);
            log.debug("upload COS successful: {}", filePath);
            return filePath;
        } catch (Exception e) {
            throw new BusinessException(ErrorCodeEnum.FILE_UPLOAD_ERROR);
        } finally {
            localFile.delete();
        }
    }

    /**
     * 上传文件到COS
     */
    private static String uploadFileToCOS(File localFile, String key) throws InterruptedException {
        PutObjectRequest putObjectRequest = new PutObjectRequest(COS_PROPERTIES.getBucket(), key, localFile);
        TransferManager transferManager = new TransferManager(COS_CLIENT);
        // 返回一个异步结果Upload, 可同步的调用waitForUploadResult等待upload结束, 成功返回UploadResult, 失败抛出异常
        Upload upload = transferManager.upload(putObjectRequest);
        UploadResult uploadResult = upload.waitForUploadResult();
        return COS_PROPERTIES.getBaseURL() + uploadResult.getKey();
    }

    /**
     * 用缓冲区来实现这个转换, 即创建临时文件
     * 使用 MultipartFile.transferTo()
     */
    private static File transferToFile(MultipartFile multipartFile) throws IOException {
        String originalFilename = multipartFile.getOriginalFilename();
        String prefix = originalFilename.split("\\.")[0];
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        File file = File.createTempFile(prefix, suffix);
        multipartFile.transferTo(file);
        return file;
    }

}
