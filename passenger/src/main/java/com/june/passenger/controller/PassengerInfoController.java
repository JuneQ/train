package com.june.passenger.controller;


import com.june.common.entity.R;
import com.june.passenger.pojo.dao.entity.Passenger;
import com.june.passenger.pojo.vo.AddOrUpdatePassengerVo;
import com.june.passenger.pojo.vo.group.Add;
import com.june.passenger.pojo.vo.group.Update;
import com.june.passenger.service.IPassengerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author june
 */
@RestController
public class PassengerInfoController {
    @Resource
    IPassengerService passengerServiceImpl;


    @Operation(description = "新增乘客")
    @PostMapping("passenger")
    public R<Long> addPassenger(@Validated(Add.class) @RequestBody AddOrUpdatePassengerVo addOrUpdatePassengerVo) {
        System.out.println(addOrUpdatePassengerVo);
        return R.ok(passengerServiceImpl.addPassenger(addOrUpdatePassengerVo));
    }

    @Operation(description = "更新乘客")
    @PutMapping("passenger")
    public R<Void> updatePassenger(@Validated(Update.class) @RequestBody AddOrUpdatePassengerVo addOrUpdatePassengerVo) {
        passengerServiceImpl.updatePassenger(addOrUpdatePassengerVo);
        return R.ok();
    }


    @Operation(description = "删除乘客")
    @DeleteMapping("passenger")
    public R<Void> deletePassenger(@RequestParam Long id) {
        passengerServiceImpl.deletePassenger(id);
        return R.ok();
    }

    @GetMapping("passenger")
    public R<Passenger> getPassenger(@RequestParam Long id) {
        return R.ok(passengerServiceImpl.getPassenger(id));
    }

    @Schema(description = "获取当前登录用户添加的所有乘客信息")
    @GetMapping("passenger/all")
    public R<List<Passenger>> getPassenger() {
        return R.ok(passengerServiceImpl.getPassengers());
    }

    @Operation(description = "用户头像上传")
    @PostMapping("upload/image")
    public R<String> uploadImage(@RequestParam("file") MultipartFile file) {
        return R.ok(passengerServiceImpl.uploadImage(file));
    }

}
