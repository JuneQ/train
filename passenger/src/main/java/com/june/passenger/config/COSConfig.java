package com.june.passenger.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@SuppressWarnings("All")
@ConfigurationProperties(prefix = "tencent.cloud.cos")
public class COSConfig {
    private String secretId;
    private String secretKey;
    private String region;
    private String bucket;
    private String folder;
    private String baseURL;
}
