package com.june.common.constant;

public class TicketConstant {
    public static final String MQ_TOPIC = "TICKET_TOPIC";
    public static final String MQ_TOPIC_DELAYED = "TICKET_TOPIC_DELAYED";
    public static final String MQ_REFRESH_REDIS_TICKET_TAG = "refresh-redis-ticket";
    public static final String REDIS_TICKET_SYS_PREFIX = "ticket:";
    public static final String REDIS_REMAINING_TICKETS_BITMAP = REDIS_TICKET_SYS_PREFIX + "tickets-bitmap:";
    public static final String REDIS_REMAINING_TICKETS_SEARCH = REDIS_TICKET_SYS_PREFIX + "tickets-search:";
    public static final String REDIS_REMAINING_TICKETS_WITH_CARRIAGE_ID = REDIS_TICKET_SYS_PREFIX + "ticket-count";

}
