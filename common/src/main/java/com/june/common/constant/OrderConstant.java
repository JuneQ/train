package com.june.common.constant;

public class OrderConstant {
    public static final String MQ_CONSUMER_GROUP = "ORDER_GROUP";
    public static final String MQ_TOPIC = "ORDER_TOPIC";
    public static final String MQ_TOPIC_DELAYED = "ORDER_TOPIC_DELAYED";
    public static final String MQ_ROLLBACK_ORDER_TAG = "rollback-order";
    public static final String MQ_PLACE_ORDER_TAG = "place-order";

    public static final String REDIS_ORDER_SYS_PREFIX = "ticket:";
    public static final String LOCK_PLACE_ORDER = REDIS_ORDER_SYS_PREFIX + "place-order-lock";
}
