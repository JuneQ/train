package com.june.common.enums;

import com.baomidou.mybatisplus.annotation.IEnum;

/**
 * @author june
 */

public enum VerifyStatus implements IEnum<String> {
    //
    PASSED,
    UNDER_REVIEW,
    FAILED;


    @Override
    public String toString() {
        return String.valueOf(super.ordinal());
    }

    @Override
    public java.lang.String getValue() {
        return String.valueOf(this.ordinal());
    }
}
