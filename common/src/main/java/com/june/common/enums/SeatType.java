package com.june.common.enums;

import com.baomidou.mybatisplus.annotation.IEnum;

/**
 * @author june
 */

public enum SeatType implements IEnum<String> {
    // 无座
    NO_SEAT,
    // 硬座
    HARD_SEAT,
    // 硬卧
    HARD_SLEEPER,
    // 软卧
    SOFT_SLEEPER,
    // 二等座
    SECOND_CLASS,
    // 一等座
    FIRST_CLASS,
    // 商务座
    BUSINESS_CLASS;

    @Override
    public String getValue() {
        return String.valueOf(this.ordinal());
    }

    @Override
    public String toString() {
        return String.valueOf(super.ordinal());
    }
}
