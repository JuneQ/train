package com.june.common.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author june
 */

public enum IdType implements IEnum<String> {
    //
    @Schema(description = "身份证")
    IDENTITY_CARD,
    @Schema(description = "护照")
    PASSPORT,
    @Schema(description = "港澳通行证")
    MAINLAND_TRAVEL_PERMIT_FOR_HONG_KONG_AND_MACAO_RESIDENTS,
    @Schema(description = "台湾通行证")
    MAINLAND_TRAVEL_PERMIT_FOR_TAIWAN_RESIDENTS,
    @Schema(description = "军官证")
    MILITARY_OFFICER_ID;

    @Override
    public String getValue() {
        return String.valueOf(this.ordinal());
    }

    @Override
    public String toString() {
        return String.valueOf(super.ordinal());
    }
}
