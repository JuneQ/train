package com.june.common.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author june
 */

@Schema(description = "优惠类型/旅客类型")
public enum PassengerType implements IEnum<String> {
    //
    CHILD,
    STUDENT,
    ADULT,
    ;

    @Override
    public String getValue() {
        return String.valueOf(this.ordinal());
    }

    @Override
    public String toString() {
        return String.valueOf(super.ordinal());
    }
}
