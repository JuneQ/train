package com.june.common.enums;

import com.baomidou.mybatisplus.annotation.IEnum;

public enum OrderStatus implements IEnum<String> {
    PLACE_ORDER,
    PAID,
    COMPLETED,
    HAS_REFUND,
    CANCELED;

    @Override
    public String toString() {
        return String.valueOf(super.ordinal());
    }

    @Override
    public String getValue() {
        return String.valueOf(super.ordinal());
    }
}
