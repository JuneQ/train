package com.june.common.enums;

import com.baomidou.mybatisplus.annotation.IEnum;

public enum PayType implements IEnum<String> {
    ALIPAY,
    WECHAT,
    BANK_CARD,
    CASH,
    OTHER,
    NOT_PAID;


    @Override
    public String getValue() {
        return String.valueOf(this.ordinal());
    }

    @Override
    public String toString() {
        return String.valueOf(super.ordinal());
    }
}
