package com.june.common.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author june
 */

@Schema(description = "车厢类型：0硬座车厢 1软座车厢  2硬卧车厢 3软卧车厢 4高级软卧车厢 5商务座车厢 6特等座车厢")
public enum CarriageType implements IEnum<String> {
    // ...
    HARD_SEAT_CARRIAGE,
    SOFT_SEAT_CARRIAGE,
    HARD_SLEEPER_CARRIAGE,
    SOFT_SLEEPER_CARRIAGE,
    DELUXE_SOFT_SLEEPER_CARRIAGE,
    BUSINESS_SEAT_CARRIAGE,
    FIRST_CLASS_SEAT_CARRIAGE;

    @Override
    public String getValue() {
        return String.valueOf(this.ordinal());
    }

    @Override
    public String toString() {
        return String.valueOf(super.ordinal());
    }
}
