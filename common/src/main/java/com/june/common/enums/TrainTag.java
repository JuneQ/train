package com.june.common.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author june
 */

@Schema(description = "列车标签 0复兴号 1智能动车组 2静音车厢 3支持选铺")
public enum TrainTag implements IEnum<String> {
    //
    FUXING,
    INTELLIGENT_MULTIPLE_UNIT,
    SILENT_CARRIAGE,
    SUPPORTS_SHOP_SELECTION,
    ;

    @Override
    public String getValue() {
        return String.valueOf(this.ordinal());
    }

    @Override
    public String toString() {
        return String.valueOf(super.ordinal());
    }
}
