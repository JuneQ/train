package com.june.common.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author june
 */
@Schema(description =
        """
                0 高速列车 High-Speed Train
                1 城际列车 Intercity Train
                2 动车组 Multiple Unit Train
                3 直达列车 Through Train
                4 特快列车 Express Train
                5 快速列车 Rapid Train
                6 普通列车 Ordinary Train
                7 货物列车 Freight Train
                8 旅游列车 Tourist Train
                """)
public enum TrainType implements IEnum<String> {
    // 高铁 D
    HIGH_SPEED_TRAINS,

    // 城际动车 C
    INTERCITY_TRAIN,

    // 动车组列 D
    MULTIPLE_UNIT_TRAIN,

    // 直达列车 Z
    THROUGH_TRAIN,

    // 特快列车 T
    EXPRESS_TRAIN,

    // 快速列车 K
    RAPID_TRAIN,

    // 普通列车 none
    ORDINARY_TRAIN,

    // 货运列车 S
    FREIGHT_TRAIN,

    // 旅游列车 Y
    TOURIST_TRAIN,
    ;

    @Override
    public String getValue() {
        return String.valueOf(this.ordinal());
    }

    @Override
    public String toString() {
        return String.valueOf(super.ordinal());
    }
}
