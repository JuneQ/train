package com.june.common.exception;


import com.june.common.entity.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

/**
 * @author june
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = BusinessException.class)
    public R<Void> conditionExceptionHandler(BusinessException e) {
        log.debug("BusinessException:::" + e.getMessage());
        return R.fail(e.getErrorCodeEnum());
    }


    /**
     * 自定义验证异常(参数传值)
     */
    @ExceptionHandler(BindException.class)
    public R<Void> validatedBindException(BindException e) {
        String message = e.getAllErrors().get(0).getDefaultMessage();
        log.debug("BindException:::" + e.getMessage());
        return R.fail(ErrorCodeEnum.PARAM_ILLEGAL);
    }

    /**
     * 自定义验证异常(json传值)
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public R<String> methodArgumentNotValidException(MethodArgumentNotValidException e) {

        return R.fail(e.getAllErrors().get(0).getDefaultMessage());
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public R<Void> methodArgumentTypeMismatchExceptionException(MethodArgumentTypeMismatchException e) {
        return R.fail(ErrorCodeEnum.PARAM_ILLEGAL);
    }

    @ExceptionHandler(NumberFormatException.class)
    public R<Void> numberFormatExceptionException(NumberFormatException e) {
        return R.fail(ErrorCodeEnum.PARAM_ILLEGAL);
    }


    @ExceptionHandler(value = Exception.class)
    public R<Void> unknowException(Exception e) {
        e.printStackTrace();
        log.error("!!!!未知异常：：" + e.getMessage());
        return R.fail(ErrorCodeEnum.UNKNOW_ERROR);
    }
}
