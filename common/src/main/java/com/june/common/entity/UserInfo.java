package com.june.common.entity;

import java.time.LocalDateTime;

/**
 * 服务间通信的DTO-UserInfo
 * 原对象在user服务中
 */
public record UserInfo(
        Long id,
        String phone,
        String nickname,
        String avatar,
        String email,
        String region,
        String address,
        LocalDateTime createTime
) {
}
