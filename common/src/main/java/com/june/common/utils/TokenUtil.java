package com.june.common.utils;

import cn.hutool.core.convert.NumberWithFormat;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.jwt.JWT;
import com.june.common.entity.UserInfo;
import com.june.common.exception.BusinessException;
import com.june.common.exception.ErrorCodeEnum;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static cn.hutool.extra.spring.SpringUtil.getBean;
import static com.june.common.constant.UserConstant.REDIS_TOKEN_BLACKLIST;

/**
 * @author june
 */
public class TokenUtil {
    public static final String TOKEN_NAME = "token";
    private static final Long REDIS_TTL_NEGATIVE = -2L;

    private static String getTokenFromCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (TOKEN_NAME.equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

    public static UserInfo parseUserInfoWithException(String token) {
        JWT jwt = checkTokenWithException(token);

        return new UserInfo(
                ((NumberWithFormat) jwt.getPayload("id")).longValue(),
                (String) jwt.getPayload("phone"),
                (String) jwt.getPayload("nickname"),
                (String) jwt.getPayload("avatar"),
                (String) jwt.getPayload("email"),
                (String) jwt.getPayload("region"),
                (String) jwt.getPayload("address"),
                LocalDateTimeUtil.of(1707409471L * 1000, ZoneId.of("Asia/Shanghai")));
    }

    /**
     * 此方法依赖异常来表明错误
     *
     * @param token token
     * @return 成功验证的JWT对象
     */
    public static JWT checkTokenWithException(String token) {
        StringRedisTemplate srt = getBean(StringRedisTemplate.class);
        String secretKey = getBean(Environment.class).getProperty("jwt.secret-key");
        if (StrUtil.isBlank(token)) {
            throw new BusinessException(ErrorCodeEnum.NOT_LOGIN);
        }

        assert secretKey != null;
        JWT jwt;
        try {
            jwt = JWT.of(token).setKey(secretKey.getBytes());
        } catch (Exception e) {
            throw new BusinessException(ErrorCodeEnum.TOKEN_OR_SECRET_KEY_ERROR);
        }

        if (!jwt.verify()) {
            throw new BusinessException(ErrorCodeEnum.TOKEN_OR_SECRET_KEY_ERROR);
        }

        if (!jwt.validate(0)) {
            throw new BusinessException(ErrorCodeEnum.TOKEN_HAS_EXPIRED);
        }

        if (!REDIS_TTL_NEGATIVE.equals(srt.getExpire(REDIS_TOKEN_BLACKLIST + token))) {
            throw new BusinessException(ErrorCodeEnum.NOT_LOGIN);
        }

        return jwt;
    }

    public static boolean isTokenValid(String token) {
        try {
            checkTokenWithException(token);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 此方法依赖机器时钟，若系统时钟不同于MySQL时钟(UTC+8)，会出现时间错误
     */
    private static LocalDateTime convertDate(NumberWithFormat date) {
        return LocalDateTimeUtil.of(date.longValue() * 1000, ZoneId.systemDefault());
    }

    public static String getToken(HttpServletRequest request) {
        String token = StrUtil.blankToDefault(request.getHeader(TOKEN_NAME), getTokenFromCookie(request));
        if (StrUtil.isBlank(token)) {
            throw new BusinessException(ErrorCodeEnum.TOKEN_OR_SECRET_KEY_ERROR);
        }
        return token;
    }

    public static Long getUserId(String token) {
        return ((NumberWithFormat) JWT.of(token).getPayload("id")).longValue();
    }

    public static LocalDateTime getTokenExpireAt(String token) {
        return convertDate((NumberWithFormat) JWT.of(token).getPayload("exp"));
    }

    public static LocalDateTime getTokenExpireAt(HttpServletRequest request) {
        return convertDate((NumberWithFormat) JWT.of(getToken(request)).getPayload("exp"));
    }


}
