package com.june.common.utils;


import com.june.common.entity.UserInfo;

/**
 * 多线程环境请谨慎使用！
 */
public class UserHolder {
    private static final ThreadLocal<UserInfo> USER_THREAD_LOCAL = new ThreadLocal<>();

    public static UserInfo getCurrentUser() {
        return USER_THREAD_LOCAL.get();
    }

    public static void setCurrentUser(UserInfo user) {
        USER_THREAD_LOCAL.set(user);
    }

    public static void clearCurrentUser() {
        USER_THREAD_LOCAL.remove();
    }
}
