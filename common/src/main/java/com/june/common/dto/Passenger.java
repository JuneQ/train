package com.june.common.dto;

import com.june.common.enums.IdType;
import com.june.common.enums.PassengerType;
import com.june.common.enums.VerifyStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 乘车人
 * </p>
 *
 * @author june
 */
@Data
public class Passenger implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "id")
    private Long id;

    @Schema(description = "会员id")
    private Long userId;

    @Schema(description = "姓名")
    private String name;

    @Schema(description = "身份证")
    private String idCard;

    @Schema(description = "证件类型")
    private IdType idType;

    @Schema(description = "旅客类型")
    private PassengerType passengerType;


    @Schema(description = "校验状态")
    private VerifyStatus verifyStatus;

    @Schema(description = "手机号")
    private String phone;

    @Schema(description = "新增时间")
    private LocalDateTime createTime;

    @Schema(description = "修改时间")
    private LocalDateTime updateTime;

    @Schema(description = "删除标识")
    private Boolean delFlag;
}
