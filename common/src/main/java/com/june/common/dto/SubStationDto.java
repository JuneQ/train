package com.june.common.dto;

import java.time.LocalDateTime;

public record SubStationDto(
        // 按到站顺序排列
        Long stationId,
        LocalDateTime arrivalDatetime
) {
}
