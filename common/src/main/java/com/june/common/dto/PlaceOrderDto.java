package com.june.common.dto;

import com.june.common.enums.CarriageType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlaceOrderDto {
    public String orderSn;
    public Long userId;
    public CarriageType carriageType;
    public Long carriageId;
    public Long trainId;
    public Long departureStationId;
    public String departureStationName;
    public String arrivalStationName;
    public Long arrivalStationId;
    public Passenger[] passengers;
    public Byte startSequence;
    public Byte endSequence;
    public Byte[] seatNumbers;
    public LocalDateTime departureTime;
    public LocalDate firstStationDepartureDate;
    public LocalDateTime createOrderTime;
    public BigDecimal pricePerTicket; // 每张票的金额！
    public Integer orderVersion;
    public String trainNumber;
    public String carriageNumber;
}
