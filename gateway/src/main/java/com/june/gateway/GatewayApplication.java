package com.june.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.june.common.exception")
public class GatewayApplication {
    public static void main(String[] args) {
        new SpringApplication(GatewayApplication.class).run(args);
    }
}
