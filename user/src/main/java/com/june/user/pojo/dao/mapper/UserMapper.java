package com.june.user.pojo.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.june.user.pojo.dao.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 会员 Mapper 接口
 * </p>
 *
 * @author June
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
