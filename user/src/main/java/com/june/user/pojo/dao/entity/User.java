package com.june.user.pojo.dao.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 会员
 * </p>
 *
 * @author June
 */
@Data
@TableName("user")
@ToString
@Schema(name = "User", description = "$!{table.comment}")
public class User implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "id")
    @TableId("id")
    private Long id;

    @TableField("nickname")
    private String nickname;

    @TableField("avatar")
    private String avatar;


    @Schema(description = "手机号")
    @TableField("phone")
    private String phone;

    @Schema(description = "密码")
    @TableField("password")
    private String password;

    @Schema(description = "邮箱")
    @TableField("email")
    private String email;

    @Schema(description = "国家/地区")
    @TableField("region")
    private String region;

    @Schema(description = "地址")
    @TableField("address")
    private String address;

    @TableField(value = "create_time", updateStrategy = FieldStrategy.NOT_NULL)
    private LocalDateTime createTime;

    @TableField(value = "update_time", updateStrategy = FieldStrategy.NOT_NULL)
    private LocalDateTime updateTime;
}
