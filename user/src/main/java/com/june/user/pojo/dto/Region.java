package com.june.user.pojo.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 地区表
 * </p>
 *
 * @author June
 */
@Data
@TableName("region")
@Schema(name = "Region", description = "$!{table.comment}")
public class Region implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "ID")
    @TableId("id")
    private Long id;

    @Schema(description = "地区名称")
    @TableField("name")
    private String name;

    @Schema(description = "地区全名")
    @TableField("full_name")
    private String fullName;

    @Schema(description = "地区编码")
    @TableField("code")
    private String code;

    @Schema(description = "地区首字母")
    @TableField("initial")
    private String initial;

    @Schema(description = "拼音")
    @TableField("pinyin")
    private String pinyin;

    @Schema(description = "热门标识")
    @TableField("hot_flag")
    private Boolean hotFlag;

    @Schema(description = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @Schema(description = "修改时间")
    @TableField("update_time")
    private LocalDateTime updateTime;

    @Schema(description = "删除标识")
    @TableField("del_flag")
    private Boolean delFlag;
}
