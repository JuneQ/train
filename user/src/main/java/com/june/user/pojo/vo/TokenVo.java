package com.june.user.pojo.vo;

public record TokenVo(String token) {
}
