package com.june.user.feign;

import com.june.common.entity.R;
import com.june.user.pojo.dto.Station;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(value = "train")
public interface TestFeignClient {
    @GetMapping("/train/station/all")
    R<List<Station>> getStation();
}
