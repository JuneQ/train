package com.june.user.controller;

import com.june.common.entity.R;
import com.june.user.pojo.dao.entity.User;
import com.june.user.pojo.vo.LoginFormVo;
import com.june.user.pojo.vo.TokenVo;
import com.june.user.service.IUserService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.constraints.Min;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    @Resource
    public IUserService userServiceImpl;

    @GetMapping("test/{test}")
    public R<Long> test(@PathVariable @Min(1) Long test) {
        return R.ok(test);

    }

    @GetMapping("send-code")
    public R<String> sendCode(@RequestParam String phone) {
        return R.ok(userServiceImpl.sendCode(phone));
    }


    @PostMapping("login")
    public R<String> login(@Validated @RequestBody LoginFormVo vo) {
        return R.ok(userServiceImpl.login(vo));
    }

    @GetMapping("info")
    public R<User> getUserInfo(HttpServletRequest request) {
        return R.ok(userServiceImpl.getUserInfo(request));
    }

    /**
     * @param user 需要修改的非null字段
     * @return 新token
     */
    @PutMapping("info")
    public R<String> updateUserInfo(@RequestBody User user) {
        return R.ok(userServiceImpl.updateUserInfo(user));
    }


    @PostMapping("logout")
    public R<Void> logout(HttpServletRequest request) {
        userServiceImpl.logout(request);
        return R.ok();
    }

    @Operation(summary = "检查Token是否有效。Token可以来自请求头/体")
    @PostMapping("check-token")
    public R<Boolean> checkToken(@RequestBody(required = false) TokenVo tokenVo, HttpServletRequest request) {
        return R.ok(userServiceImpl.checkToken(tokenVo.token(), request));
    }

    @Operation(summary = "失效旧token并生成新token")
    @PostMapping("generate-new-token")
    public R<String> generateNewToken(@RequestBody TokenVo tokenVo) {
        return R.ok(userServiceImpl.generateNewToken(tokenVo.token()));
    }

    @PostMapping("invalid-token")
    public R<Void> invalidToken(@RequestBody TokenVo tokenVo) {
        userServiceImpl.invalidToken(tokenVo.token());
        return R.ok();
    }
}
