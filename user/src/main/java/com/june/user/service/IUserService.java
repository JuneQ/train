package com.june.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.june.user.pojo.dao.entity.User;
import com.june.user.pojo.vo.LoginFormVo;
import jakarta.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员 服务类
 * </p>
 *
 * @author June
 */
public interface IUserService extends IService<User> {
    String sendCode(String phone);

    String login(LoginFormVo vo);

    Long register(User user);

    User getUserInfo(HttpServletRequest request);

    User getUserInfo(String token);

    void logout(HttpServletRequest request);

    Boolean checkToken(String token, HttpServletRequest request);

    String updateUserInfo(User user);

    void invalidToken(String token);

    String generateNewToken(String token);
}
