package com.june.user;

import com.aliyun.sdk.service.dysmsapi20170525.AsyncClient;
import com.june.user.feign.TestFeignClient;
import com.june.user.pojo.dao.mapper.UserMapper;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.ReturnType;
import org.springframework.data.redis.connection.StringRedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Random;


@SpringBootTest
class UserApplicationTests {

    @Resource
    UserMapper userMapper;

    @Resource
    StringRedisTemplate stringRedisTemplate;

    @Resource
    StringRedisConnection stringRedisConnection;
    @Value("${jwt.secret-key}")
    String key;

    @Resource
    TestFeignClient testFeignClient;

    @Resource
    AsyncClient asyncClient;

    @Value("${aliyun.sms.template-id}")
    String smsTemplate;

    public static void main(String[] args) {
    }

    @Test
    void contextLoads() {
        // Lua脚本
        String luaScript = """
                    local cursor = 0
                    local departure_idx = tonumber(ARGV[1])
                    local arrival_idx = tonumber(ARGV[2])

                    repeat
                        local result = redis.call('SCAN', cursor, 'MATCH', KEYS[1], 'COUNT', 1)
                        cursor = tonumber(result[1])
                        local data = result[2]

                        for i, key in ipairs(data) do
                            if redis.call('BITCOUNT', key, departure_idx, arrival_idx, 'BIT') == 0 then
                                for i = departure_idx, arrival_idx, 1 do
                                    redis.call('SETBIT', key, i, 1)
                                end
                                
                                return key
                            end
                            
                        end
                    until cursor == 0

                    return '0'
                """;

        // 创建RedisScript对象
//        DefaultRedisScript<String> redisScript = new DefaultRedisScript<>();
//        redisScript.setScriptSource(new StaticScriptSource(luaScript));
//        redisScript.setResultType(String.class);

        // 执行脚本，传入参数
//        List<String> keys = Collections.singletonList("yourKeyPattern"); // 替换为你的键匹配模式
////        List<String> args = Arrays.asList("0", "1");
//        String execute = stringRedisTemplate.execute(redisScript, keys, "0", "1");
//        System.out.println(execute);

        byte[] yourKeyPattern = stringRedisConnection.eval(luaScript, ReturnType.VALUE, 1, "yourKeyPattern", "0", "1");
        System.out.println(new String(yourKeyPattern));
//        stringRedisConnection.eval()
    }

    @Test
    void testRedis() {
    }

    public void populateRedisWithRandomData() {
        Random random = new Random();
        stringRedisTemplate.executePipelined((RedisCallback<Object>) connection -> {
            for (int i = 0; i < 1000_0000; i++) {
                String key = "key-" + i;
                String value = "value-" + random.nextInt(100);
                connection.set(key.getBytes(), value.getBytes());
            }
            return null;
        });
    }
}
