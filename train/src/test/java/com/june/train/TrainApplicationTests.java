package com.june.train;


import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.TypeReference;
import com.june.train.pojo.dao.Train;
import com.june.train.pojo.dao.mapper.RegionMapper;
import com.june.train.pojo.dao.mapper.TrainMapper;
import com.june.train.pojo.vo.SimpleSelectVo;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.StringRedisConnection;
import org.springframework.data.redis.core.StringRedisTemplate;

@SpringBootTest
class TrainApplicationTests {


    @Resource
    TrainMapper trainMapper;

    @Resource
    RegionMapper regionMapper;

    @Resource
    StringRedisConnection stringRedisConnection;

    @Resource
    StringRedisTemplate stringRedisTemplate;


    @Test
    void contextLoads() {
    }

    @Test
    void contextLoads1() {
        String params = """
                {"keyword":""}
                """.trim();
        SimpleSelectVo<Train> vo = JSONObject.parseObject(params, new TypeReference<>() {
        });


        System.out.println(vo);

    }

}
