package com.june.train.pojo.dao;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.june.common.enums.CarriageType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 车厢表
 * </p>
 *
 * @author June
 */
@Data
@TableName("carriage")
@Schema(name = "Carriage", description = "$!{table.comment}")
public class Carriage implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "ID")
    @TableId("id")
    private Long id;

    @Schema(description = "列车ID")
    @TableField("train_id")
    private Long trainId;

    @Schema(description = "车厢号")
    @TableField("carriage_number")
    private String carriageNumber;

    @Schema(description = "车厢类型")
    @TableField("carriage_type")
    private CarriageType carriageType;

    @Schema(description = "座位数")
    @TableField("seat_count")
    private Short seatCount;

    @Schema(description = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @Schema(description = "修改时间")
    @TableField("update_time")
    private LocalDateTime updateTime;

    @Schema(description = "删除标识")
    @TableField("del_flag")
    private Boolean delFlag;

    @TableField(exist = false)
    private Train train;
}
