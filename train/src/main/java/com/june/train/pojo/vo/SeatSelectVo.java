package com.june.train.pojo.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.june.common.enums.SeatType;
import com.june.train.pojo.dao.Seat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SeatSelectVo extends Page<Seat> {
    Long id;
    Long trainId;
    String carriageNumber;
    String seatNumber;
    SeatType seatType;
}
