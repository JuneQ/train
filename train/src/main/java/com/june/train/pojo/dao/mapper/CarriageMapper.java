package com.june.train.pojo.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.june.train.pojo.dao.Carriage;
import com.june.train.pojo.vo.SimpleSelectVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 车厢表 Mapper 接口
 * </p>
 *
 * @author June
 */
@Mapper
public interface CarriageMapper extends BaseMapper<Carriage> {

    List<Carriage> selectCarriagePage(SimpleSelectVo<Carriage> vo);
}
