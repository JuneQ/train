package com.june.train.pojo.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.june.train.pojo.dao.TrainStationPrice;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 列车站点价格表 Mapper 接口
 * </p>
 *
 * @author June
 */
@Mapper
public interface TrainStationPriceMapper extends BaseMapper<TrainStationPrice> {

}
