package com.june.train.pojo.vo;

import com.june.common.enums.SeatType;
import jakarta.validation.constraints.NotNull;

public record SeatAddVo(
        Long id,
        @NotNull
        Long trainId,
        @NotNull
        String carriageNumber,
        @NotNull
        String seatNumber,
        @NotNull
        SeatType seatType
) {
}
