package com.june.train.pojo.vo;

import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;

public record StationPriceUpdateVo(
        @NotNull
        Long id,
        Long trainId,
        Long departureStationId,
        Long arrivalStationId,
        Long seatId,
        BigDecimal price

) {
}
