package com.june.train.pojo.dao.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.june.train.pojo.dao.Region;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 地区表 Mapper 接口
 * </p>
 *
 * @author June
 */
@Mapper
public interface RegionMapper extends BaseMapper<Region> {

}
