package com.june.train.pojo.vo;

import com.june.common.enums.CarriageType;
import jakarta.validation.constraints.NotNull;

public record CarriageAddVo(
        Long id,
        @NotNull
        CarriageType carriageType,

        @NotNull
        String carriageNumber,
        @NotNull
        Short seatCount,
        @NotNull
        Long trainId
) {
}
