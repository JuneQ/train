package com.june.train.pojo.vo;

import jakarta.validation.constraints.NotNull;

public record RegionAddVo(
        Long id,

        @NotNull
        String name,
        String fullName,
        @NotNull
        String code,


        String initial,

        String pinyin,

        Boolean hotFlag
) {
}
