package com.june.train.pojo.dao;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.june.common.enums.TrainType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * <p>
 * 列车信息表
 * </p>
 *
 * @author June
 */
@Data
@TableName("train")
@Schema(name = "Train", description = "$!{table.comment}")
public class Train implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "ID")
    @TableId("id")
    private Long id;

    @Schema(description = "列车车次")
    @TableField("train_number")
    private String trainNumber;

    @Schema(description = "0 高速列车 (High-Speed Train) | 1 城际列车 (Intercity Train) | 2 动车组 (Multiple Unit Train) | 3 直达列车 (Through Train) | 4 特快列车 (Express Train) | 5 快速列车 (Rapid Train) | 6 普通列车 (Ordinary Train) | 7 货物列车 (Freight Train) | 8 旅游列车 (Tourist Train)")
    @TableField("train_type")
    private TrainType trainType;


    @Schema(description = "起始站ID")
    @TableField("departure_station_id")
    private Long startStationId;

    @Schema(description = "终点站ID")
    @TableField("arrival_station_id")
    private Long endStationId;

    @Schema(description = "始发时间")
    @TableField("departure_time")
    private LocalTime departureTime;

    @Schema(description = "到达终点站时间")
    @TableField("arrival_time")
    private LocalTime arrivalTime;

    @Schema(description = "行驶所跨天数")
    @TableField("day_crossed")
    private Byte dayCrossed;

    @Schema(description = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;


    @Schema(description = "修改时间")
    @TableField("update_time")
    private LocalDateTime updateTime;

    @Schema(description = "列车可用标识")
    @TableField("available_flag")
    private Boolean availableFlag;

    @Schema(description = "删除标识")
    @TableField("del_flag")
    private Boolean delFlag;

    @TableField(exist = false)
    private Station startStation;

    @TableField(exist = false)
    private Station endStation;

}
