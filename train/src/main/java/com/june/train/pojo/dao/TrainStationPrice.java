package com.june.train.pojo.dao;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 列车站点价格表
 * </p>
 *
 * @author June
 */
@Data
@TableName("train_station_price")
@Schema(name = "TrainStationPrice", description = "$!{table.comment}")
public class TrainStationPrice implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "ID")
    @TableId("id")
    private Long id;

    @Schema(description = "列车ID")
    @TableField("train_id")
    private Long trainId;

    @Schema(description = "出发站点")
    @TableField("departure_station_id")
    private Long departureStationId;

    @Schema(description = "到达站点")
    @TableField("arrival_station_id")
    private Long arrivalStationId;

    @Schema(description = "座位ID")
    @TableField("seat_id")
    private Long seatId;

    @Schema(description = "车票价格")
    @TableField("price")
    private BigDecimal price;

    @Schema(description = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @Schema(description = "修改时间")
    @TableField("update_time")
    private LocalDateTime updateTime;

    @Schema(description = "删除标识")
    @TableField("del_flag")
    private Boolean delFlag;
}
