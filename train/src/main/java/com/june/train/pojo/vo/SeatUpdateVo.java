package com.june.train.pojo.vo;

import com.june.common.enums.SeatType;
import jakarta.validation.constraints.NotNull;

public record SeatUpdateVo(
        @NotNull
        Long id,
        Long trainId,
        String carriageNumber,
        String seatNumber,
        SeatType seatType
) {
}
