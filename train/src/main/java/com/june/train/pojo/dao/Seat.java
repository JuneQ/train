package com.june.train.pojo.dao;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.june.common.enums.SeatType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 座位表
 * </p>
 *
 * @author June
 */
@Data
@TableName("seat")
@Schema(name = "Seat", description = "$!{table.comment}")
public class Seat implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "ID")
    @TableId("id")
    private Long id;

    @Schema(description = "列车ID")
    @TableField("train_id")
    private Long trainId;

    @Schema(description = "车厢号")
    @TableField("carriage_number")
    private String carriageNumber;

    @Schema(description = "座位号")
    @TableField("seat_number")
    private String seatNumber;

    @Schema(description = "座位类型 0无座 1硬座 2硬卧 3软卧 4二等座 5一等座 6商务座")
    @TableField("seat_type")
    private SeatType seatType;

    @Schema(description = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @Schema(description = "修改时间")
    @TableField("update_time")
    private LocalDateTime updateTime;

    @Schema(description = "删除标识")
    @TableField("del_flag")
    private Boolean delFlag;
}
