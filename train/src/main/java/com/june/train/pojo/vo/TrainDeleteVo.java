package com.june.train.pojo.vo;

import jakarta.validation.constraints.NotNull;

public record TrainDeleteVo(
        @NotNull
        Long id
) {
}
