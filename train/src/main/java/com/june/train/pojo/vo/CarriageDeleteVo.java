package com.june.train.pojo.vo;

public record CarriageDeleteVo(
        Long id
) {
}
