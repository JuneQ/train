package com.june.train.pojo.dao.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.june.train.pojo.dao.Seat;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 座位表 Mapper 接口
 * </p>
 *
 * @author June
 */
@Mapper
public interface SeatMapper extends BaseMapper<Seat> {

}
