package com.june.train.pojo.vo;

import jakarta.validation.constraints.NotNull;

import java.time.LocalTime;

public record StationArrivalAddVo(
        Long id,

        @NotNull
        Long trainId,
        @NotNull
        Long stationId,

        @NotNull
        Byte sequence,

        @NotNull
        Long departureStationId,

        @NotNull
        Byte kthFromDepartureDay,

        @NotNull
        Long arrivalStationId,

        @NotNull
        LocalTime arrivalTime,

        @NotNull
        Byte stopoverTime
) {
}
