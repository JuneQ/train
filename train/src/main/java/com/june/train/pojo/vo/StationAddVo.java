package com.june.train.pojo.vo;

import jakarta.validation.constraints.NotNull;

public record StationAddVo(
        Long id,

        @NotNull
        String name,

        @NotNull
        String code,
        @NotNull
        Long regionId
) {
}
