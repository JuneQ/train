package com.june.train.pojo.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.june.train.pojo.dao.TrainStationArrival;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 列车到站表 Mapper 接口
 * </p>
 *
 * @author June
 */
@Mapper
public interface TrainStationArrivalMapper extends BaseMapper<TrainStationArrival> {

    List<TrainStationArrival> selectTrainStationArrivalById(Long id);
}
