package com.june.train.pojo.dao;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 车站表
 * </p>
 *
 * @author June
 */
@Data
@TableName("station")
@Schema(name = "Station", description = "$!{table.comment}")
public class Station implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "ID")
    @TableId("id")
    private Long id;

    @Schema(description = "车站编号")
    @TableField("code")
    private String code;

    @Schema(description = "车站名称")
    @TableField("name")
    private String name;

    @Schema(description = "拼音")
    @TableField("name_pinyin")
    private String namePinyin;

    @Schema(description = "车站地区")
    @TableField("region_id")
    private Long regionId;

    @Schema(description = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @Schema(description = "修改时间")
    @TableField("update_time")
    private LocalDateTime updateTime;


    @TableField(exist = false)
    private Region region;

    @Schema(description = "删除标识")
    @TableField("del_flag")
    private Boolean delFlag;

}
