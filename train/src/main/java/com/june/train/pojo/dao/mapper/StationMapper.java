package com.june.train.pojo.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.june.train.pojo.dao.Station;
import com.june.train.pojo.vo.SimpleSelectVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 车站表 Mapper 接口
 * </p>
 *
 * @author June
 */
@Mapper
public interface StationMapper extends BaseMapper<Station> {

    List<Station> selectPage(SimpleSelectVo<Station> vo);
}
