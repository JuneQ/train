package com.june.train.pojo.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.june.common.enums.TrainType;
import com.june.train.pojo.dao.Train;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TrainSelectVo extends Page<Train> {
    public Long id;
    public TrainType trainType;
}
