package com.june.train.pojo.vo;

import jakarta.validation.constraints.NotNull;

import java.time.LocalTime;

public record StationArrivalUpdateVo(
        @NotNull
        Long id,
        Long trainId,
        Long stationId,
        Byte sequence,
        Long departureStationId,
        Long arrivalStationId,
        LocalTime arrivalTime,

        Byte kthFromDepartureDay,
        LocalTime departureTime,
        Byte stopoverTime
) {
}
