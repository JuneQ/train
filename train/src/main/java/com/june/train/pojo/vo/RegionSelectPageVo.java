package com.june.train.pojo.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.june.train.pojo.dao.Region;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author june
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegionSelectPageVo extends Page<Region> {
    Long id;
    String name;
    String fullName;
}
