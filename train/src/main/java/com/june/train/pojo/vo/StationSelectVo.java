package com.june.train.pojo.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.june.train.pojo.dao.Region;
import com.june.train.pojo.dao.Station;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author june
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StationSelectVo extends Page<Station> {
    private Long id;
    private String code;
    private String name;
    private String namePinyin;
    private Region region;
}
