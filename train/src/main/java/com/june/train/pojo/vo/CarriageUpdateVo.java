package com.june.train.pojo.vo;

import com.june.common.enums.CarriageType;
import jakarta.validation.constraints.NotNull;

public record CarriageUpdateVo(
        @NotNull
        Long id,
        String trainId,
        String carriageNumber,
        CarriageType carriageType,
        Short seatCount
) {
}
