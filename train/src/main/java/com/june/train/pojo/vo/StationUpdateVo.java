package com.june.train.pojo.vo;

import jakarta.validation.constraints.NotNull;

public record StationUpdateVo(
        @NotNull
        Long id,
        String code,
        String name,
        String namePinyin,
        String regionId
) {
}
