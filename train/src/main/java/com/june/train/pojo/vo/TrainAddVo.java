package com.june.train.pojo.vo;

import com.june.common.enums.TrainType;
import jakarta.validation.constraints.NotNull;

import java.time.LocalTime;

public record TrainAddVo(
        Long id,
        @NotNull
        String trainNumber,
        @NotNull
        TrainType trainType,

        @NotNull
        Byte dayCrossed,
        @NotNull
        Long startStationId,
        @NotNull
        Long endStationId,
        @NotNull
        LocalTime departureTime,
        @NotNull
        LocalTime arrivalTime,
        @NotNull
        Boolean availableFlag
) {
}
