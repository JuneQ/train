package com.june.train.pojo.vo;

public record StationPriceDeleteVo(
        Long id
) {
}
