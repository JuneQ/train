package com.june.train.pojo.vo;

import jakarta.validation.constraints.NotNull;

public record RegionUpdateVo(
        @NotNull
        Long id,
        String name,
        String fullName,
        String code,
        String initial,
        String pinyin,
        Boolean hotFlag,
        Boolean delFlag
) {
}
