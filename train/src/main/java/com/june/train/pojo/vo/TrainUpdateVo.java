package com.june.train.pojo.vo;

import com.june.common.enums.TrainType;
import jakarta.validation.constraints.NotNull;

import java.time.LocalTime;

public record TrainUpdateVo(
        @NotNull
        Long id,
        String trainNumber,
        TrainType trainType,
        Long startStationId,
        Long endStationId,
        Byte dayCrossed,
        LocalTime departureTime,
        LocalTime arrivalTime,
        Boolean availableFlag
) {
}
