package com.june.train.pojo.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.june.train.pojo.dao.Train;
import com.june.train.pojo.vo.SimpleSelectVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 列车信息表 Mapper 接口
 * </p>
 *
 * @author June
 */
@Mapper
public interface TrainMapper extends BaseMapper<Train> {


    List<Train> selectTrainWithCondition(SimpleSelectVo<Train> vo);
}
