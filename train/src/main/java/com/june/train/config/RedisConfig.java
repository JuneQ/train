package com.june.train.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.DefaultStringRedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.StringRedisConnection;

@Configuration
public class RedisConfig {
    @Bean
    StringRedisConnection getStringRedisConnection(RedisConnectionFactory redisConnectionFactory) {
        return new DefaultStringRedisConnection(redisConnectionFactory.getConnection());
    }
}
