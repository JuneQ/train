package com.june.train.controller;

import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.TypeReference;
import com.june.common.entity.R;
import com.june.train.pojo.dao.Carriage;
import com.june.train.pojo.vo.CarriageAddVo;
import com.june.train.pojo.vo.CarriageUpdateVo;
import com.june.train.pojo.vo.SimpleSelectVo;
import com.june.train.service.ICarriageService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public class CarriageController {
    @Resource
    private ICarriageService carriageServiceImpl;

    @Operation(description = "查询carriage,分页")
    @GetMapping("carriage/page")
    @SuppressWarnings("all")
    public R<List<Carriage>> getCarriagePage(@RequestParam String params) {
        log.debug("经常报错？参数: {}", params);

        SimpleSelectVo<Carriage> vo = JSONObject.parseObject(params, new TypeReference<SimpleSelectVo<Carriage>>() {
        });

        return R.ok(carriageServiceImpl.getCarriagePage(vo));
    }

    @Operation(description = "查询所有carriage")
    @GetMapping("carriage/all")
    public R<List<Carriage>> getCarriage() {
        return R.ok(carriageServiceImpl.getCarriages());
    }

    @Operation(description = "逻辑删除对应Id的记录")
    @DeleteMapping("carriage")
    public R<Void> deleteCarriage(@RequestParam Long id) {
        carriageServiceImpl.deleteCarriageById(id);
        return R.ok();
    }

    @Operation(description = "添加Carriage记录")
    @PostMapping("carriage")
    public R<Long> addCarriage(@RequestBody @Validated CarriageAddVo vo) {
        Long id = carriageServiceImpl.addCarriage(vo);
        return R.ok(id);
    }

    @Operation(description = "更新Carriage记录")
    @PutMapping("carriage")
    public R<Void> updateCarriage(@RequestBody @Validated CarriageUpdateVo vo) {
        carriageServiceImpl.updateCarriage(vo);
        return R.ok();
    }

}
