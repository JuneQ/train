package com.june.train.controller;

import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.TypeReference;
import com.june.common.entity.R;
import com.june.train.pojo.dao.Station;
import com.june.train.pojo.vo.SimpleSelectVo;
import com.june.train.pojo.vo.StationAddVo;
import com.june.train.pojo.vo.StationUpdateVo;
import com.june.train.service.IStationService;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author june
 */
@RestController
public class StationController {
    @Resource
    private IStationService stationServiceImpl;

    @GetMapping("station/all")
    public R<List<Station>> getStation() {
        return R.ok(stationServiceImpl.getAllStation());
    }

    @GetMapping("station")
    public R<List<Station>> getStation(@RequestParam String params) {
        SimpleSelectVo<Station> vo = JSONObject.parseObject(params, new TypeReference<SimpleSelectVo<Station>>() {
        });
        return R.ok(stationServiceImpl.getStation(vo));
    }

    @PutMapping("station")
    public R<Void> updateStation(@RequestBody @Validated StationUpdateVo vo) {
        stationServiceImpl.updateStation(vo);
        return R.ok();
    }

    @PostMapping("station")
    public R<Long> addStation(@RequestBody @Validated StationAddVo vo) {
        return R.ok(stationServiceImpl.addStation(vo));
    }

    @DeleteMapping("station")
    public R<Void> deleteStation(@RequestParam Long id) {
        stationServiceImpl.deleteStation(id);
        return R.ok();
    }
}
