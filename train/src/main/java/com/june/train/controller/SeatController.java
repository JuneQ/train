package com.june.train.controller;

import com.june.common.entity.R;
import com.june.train.pojo.dao.Seat;
import com.june.train.pojo.vo.SeatAddVo;
import com.june.train.pojo.vo.SeatSelectVo;
import com.june.train.pojo.vo.SeatUpdateVo;
import com.june.train.service.ISeatService;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SeatController {
    @Resource
    private ISeatService seatServiceImpl;

    @GetMapping("Seat")
    public R<List<Seat>> getSeat(@RequestBody SeatSelectVo vo) {
        return R.ok(seatServiceImpl.getSeat(vo));
    }

    @DeleteMapping("Seat")
    public R<Void> deleteSeat(@RequestParam Long id) {
        seatServiceImpl.deleteSeat(id);
        return R.ok();
    }

    @PutMapping("Seat")
    public R<Void> updateSeat(@RequestBody @Validated SeatUpdateVo vo) {
        seatServiceImpl.updateSeat(vo);
        return R.ok();
    }

    @PostMapping("Seat")
    public R<Long> addSeat(@RequestBody @Validated SeatAddVo vo) {
        return R.ok(seatServiceImpl.addSeat(vo));
    }
}
