package com.june.train.controller;

import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.TypeReference;
import com.june.common.dto.SubStationDto;
import com.june.common.entity.R;
import com.june.train.pojo.dao.Train;
import com.june.train.pojo.dao.TrainStationArrival;
import com.june.train.pojo.vo.*;
import com.june.train.service.ITrainService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author june
 */
@RestController
@Slf4j
public class TrainController {
    @Resource
    private ITrainService trainServiceImpl;

    @Operation(description = "获取最近的一次发车表（已经发过的）")
    @GetMapping("sub-station/{trainId}")
    R<List<SubStationDto>> getPastAndNearestSubStationIdAndSequenceAndTime(@PathVariable("trainId") Long trainId) {
        return R.ok(trainServiceImpl.getPastAndNearestSubStationIdAndSequenceAndTime(trainId));
    }

    @GetMapping("train")
    @SuppressWarnings("all")
    public R<List<Train>> getTrain(@RequestParam String params) {
        log.debug("经常报错？params：{}", params);

        SimpleSelectVo<Train> vo = JSONObject.parseObject(params, new TypeReference<SimpleSelectVo<Train>>() {
        });

        return R.ok(trainServiceImpl.getTrain(vo));
    }


    @DeleteMapping("train")
    public R<Void> deleteTrain(@RequestParam Long id) {
        trainServiceImpl.deleteTrain(id);
        return R.ok();
    }

    @PutMapping("train")
    public R<Void> updateTrain(@RequestBody @Validated TrainUpdateVo vo) {
        trainServiceImpl.updateTrain(vo);
        return R.ok();
    }

    @PostMapping("train")
    public R<Long> addTrain(@RequestBody @Validated TrainAddVo vo) {
        return R.ok(trainServiceImpl.addTrain(vo));
    }


    @PostMapping("station-arrival")
    public R<Long> addStationArrival(@RequestBody StationArrivalAddVo vo) {
        return R.ok(trainServiceImpl.addTrainStationArrival(vo));
    }

    @GetMapping("station-crossed/{trainId}")
    R<Byte> getStationCrossed(@PathVariable("trainId") Long trainId) {
        return R.ok(trainServiceImpl.getStationCrossed(trainId));
    }


    @DeleteMapping("station-arrival")
    public R<Void> deleteStationArrival(@RequestParam Long id) {
        trainServiceImpl.deleteTrainStationArrival(id);
        return R.ok();
    }

    @PutMapping("station-arrival")
    public R<Void> updateStationArrival(@RequestBody StationArrivalUpdateVo vo) {
        trainServiceImpl.updateTrainStationArrival(vo);
        return R.ok();
    }

    @GetMapping("station-arrival")
    public R<List<TrainStationArrival>> getStationArrival(@RequestParam Long id) {
        return R.ok(trainServiceImpl.getTrainStationArrival(id));
    }

    @Schema(description = "后台上票调用的接口")
    @GetMapping("station-arrival/{current}/{size}")
    public R<List<TrainStationArrival>> getStationArrivalPageFromSys(@PathVariable("current") Integer current,
                                                                     @PathVariable("size") Integer size
    ) {
        return R.ok(trainServiceImpl.getTrainStationArrivalPage(current, size));
    }

    @GetMapping("station-arrival/sequence/{stationId}/{trainId}")
    public R<Byte> getStationArrivalSequence(@PathVariable("stationId") Long stationId, @PathVariable("trainId") Long trainId) {
        return R.ok(trainServiceImpl.getStationArrivalSequence(stationId, trainId));
    }

    @GetMapping("station-arrival/arrival-datetime/{stationId}/{trainId}/{departureDate}")
    R<LocalDateTime> getTrainStationArrivalDateTime(@PathVariable("stationId") Long stationId,
                                                    @PathVariable("trainId") Long trainId,
                                                    @PathVariable("departureDate") String departureDate
    ) {
        return R.ok(trainServiceImpl.getTrainStationArrivalDateTime(stationId, trainId, departureDate));
    }
}
