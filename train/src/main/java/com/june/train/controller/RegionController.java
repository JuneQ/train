package com.june.train.controller;

import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.TypeReference;
import com.june.common.entity.R;
import com.june.train.pojo.dao.Region;
import com.june.train.pojo.vo.RegionAddVo;
import com.june.train.pojo.vo.RegionUpdateVo;
import com.june.train.pojo.vo.SimpleSelectVo;
import com.june.train.service.IRegionService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RegionController {
    @Resource
    private IRegionService regionServiceImpl;

    @Operation(description = "查询region,分页")
    @GetMapping("region/page")
    public R<List<Region>> getRegions(@RequestParam String params) {
        SimpleSelectVo<Region> vo = JSONObject.parseObject(params, new TypeReference<SimpleSelectVo<Region>>() {
        });
        return R.ok(regionServiceImpl.getRegionPage(vo));
    }

    @Operation(description = "查询所有region,不分页")
    @GetMapping("region/all")
    public R<List<Region>> getRegions() {
        return R.ok(regionServiceImpl.getRegions());
    }

    @Operation(description = "逻辑删除对应Id的记录")
    @DeleteMapping("region")
    public R<Void> deleteRegion(@RequestParam Long id) {
        regionServiceImpl.deleteRegionById(id);
        return R.ok();
    }

    @Operation(description = "添加Region记录")
    @PostMapping("region")
    public R<Long> addRegion(@RequestBody @Validated RegionAddVo vo) {
        Long id = regionServiceImpl.addRegion(vo);
        return R.ok(id);
    }

    @Operation(description = "更新Region记录")
    @PutMapping("region")
    public R<Void> updateRegion(@RequestBody @Validated RegionUpdateVo vo) {
        regionServiceImpl.updateRegion(vo);
        return R.ok();
    }
}
