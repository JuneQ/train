package com.june.train.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.june.train.pojo.dao.Carriage;
import com.june.train.pojo.dao.mapper.CarriageMapper;
import com.june.train.pojo.vo.CarriageAddVo;
import com.june.train.pojo.vo.CarriageUpdateVo;
import com.june.train.pojo.vo.SimpleSelectVo;
import com.june.train.service.ICarriageService;
import jakarta.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 车厢表 服务实现类
 * </p>
 *
 * @author June
 */
@Service
public class CarriageServiceImpl extends ServiceImpl<CarriageMapper, Carriage> implements ICarriageService {

    @Resource
    private CarriageMapper carriageMapper;

    @Override
    public List<Carriage> getCarriagePage(SimpleSelectVo<Carriage> vo) {

        return carriageMapper.selectCarriagePage(vo);
    }

    @Override
    public void deleteCarriageById(Long id) {
        carriageMapper.deleteById(id);
    }

    @Override
    public Long addCarriage(CarriageAddVo vo) {
        Carriage c = new Carriage();
        BeanUtils.copyProperties(vo, c);
        carriageMapper.insert(c);
        return c.getId();
    }

    @Override
    public void updateCarriage(CarriageUpdateVo vo) {
        Carriage c = new Carriage();
        BeanUtils.copyProperties(vo, c);
        carriageMapper.updateById(c);
    }

    @Override
    public List<Carriage> getCarriages() {
        var vo = new SimpleSelectVo<Carriage>();
        vo.setCurrent(1);
        vo.setSize(Long.MAX_VALUE);
        return carriageMapper.selectCarriagePage(vo);
    }
}
