package com.june.train.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.june.train.pojo.dao.Carriage;
import com.june.train.pojo.vo.CarriageAddVo;
import com.june.train.pojo.vo.CarriageUpdateVo;
import com.june.train.pojo.vo.SimpleSelectVo;

import java.util.List;

/**
 * <p>
 * 车厢表 服务类
 * </p>
 *
 * @author June
 */
public interface ICarriageService extends IService<Carriage> {


    List<Carriage> getCarriagePage(SimpleSelectVo<Carriage> vo);

    void deleteCarriageById(Long id);

    Long addCarriage(CarriageAddVo vo);

    void updateCarriage(CarriageUpdateVo vo);

    List<Carriage> getCarriages();
}
