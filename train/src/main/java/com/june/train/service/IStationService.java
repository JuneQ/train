package com.june.train.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.june.train.pojo.dao.Station;
import com.june.train.pojo.vo.SimpleSelectVo;
import com.june.train.pojo.vo.StationAddVo;
import com.june.train.pojo.vo.StationUpdateVo;

import java.util.List;

/**
 * <p>
 * 车站表 服务类
 * </p>
 *
 * @author June
 */
public interface IStationService extends IService<Station> {


    List<Station> getStation(SimpleSelectVo<Station> vo);

    void updateStation(StationUpdateVo vo);

    void deleteStation(Long id);

    Long addStation(StationAddVo vo);

    List<Station> getAllStation();
}
