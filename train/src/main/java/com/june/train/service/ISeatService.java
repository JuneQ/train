package com.june.train.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.june.train.pojo.dao.Seat;
import com.june.train.pojo.vo.SeatAddVo;
import com.june.train.pojo.vo.SeatUpdateVo;

/**
 * <p>
 * 座位表 服务类
 * </p>
 *
 * @author June
 */
public interface ISeatService extends IService<Seat> {

    void deleteSeat(Long id);

    void updateSeat(SeatUpdateVo vo);

    Long addSeat(SeatAddVo vo);
}
