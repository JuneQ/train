package com.june.train.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.june.train.pojo.dao.Seat;
import com.june.train.pojo.dao.mapper.SeatMapper;
import com.june.train.pojo.vo.SeatAddVo;
import com.june.train.pojo.vo.SeatUpdateVo;
import com.june.train.service.ISeatService;
import jakarta.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 座位表 服务实现类
 * </p>
 *
 * @author June
 */
@Service
public class SeatServiceImpl extends ServiceImpl<SeatMapper, Seat> implements ISeatService {
    @Resource
    SeatMapper seatMapper;


    @Override
    public void deleteSeat(Long id) {
        seatMapper.deleteById(id);
    }

    @Override
    public void updateSeat(SeatUpdateVo vo) {
        Seat seat = new Seat();
        BeanUtils.copyProperties(vo, seat);
        seatMapper.updateById(seat);
    }

    @Override
    public Long addSeat(SeatAddVo vo) {
        Seat seat = new Seat();
        BeanUtils.copyProperties(vo, seat);
        return seat.getId();
    }
}
