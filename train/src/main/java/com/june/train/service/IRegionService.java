package com.june.train.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.june.train.pojo.dao.Region;
import com.june.train.pojo.vo.RegionAddVo;
import com.june.train.pojo.vo.RegionUpdateVo;
import com.june.train.pojo.vo.SimpleSelectVo;

import java.util.List;

/**
 * <p>
 * 地区表 服务类
 * </p>
 *
 * @author June
 */
public interface IRegionService extends IService<Region> {

    List<Region> getRegionPage(SimpleSelectVo<Region> vo);

    void deleteRegionById(Long id);

    Long addRegion(RegionAddVo region);

    void updateRegion(RegionUpdateVo vo);

    List<Region> getRegions();
}
