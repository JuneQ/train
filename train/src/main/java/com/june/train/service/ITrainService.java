package com.june.train.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.june.common.dto.SubStationDto;
import com.june.train.pojo.dao.Train;
import com.june.train.pojo.dao.TrainStationArrival;
import com.june.train.pojo.vo.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 列车信息表 服务类
 * </p>
 *
 * @author June
 */
public interface ITrainService extends IService<Train> {
    List<Train> getTrain(SimpleSelectVo<Train> vo);

    void deleteTrain(Long id);

    void updateTrain(TrainUpdateVo vo);

    Long addTrain(TrainAddVo vo);

    Long addTrainStationArrival(StationArrivalAddVo vo);

    void deleteTrainStationArrival(Long id);

    void updateTrainStationArrival(StationArrivalUpdateVo vo);

    List<TrainStationArrival> getTrainStationArrival(Long id);


    Byte getStationArrivalSequence(Long stationId, Long trainId);

    LocalDateTime getTrainStationArrivalDateTime(Long stationId, Long trainId, String departureDateTime);

    List<TrainStationArrival> getTrainStationArrivalPage(Integer current, Integer size);

    Byte getStationCrossed(Long trainId);

    List<SubStationDto> getPastAndNearestSubStationIdAndSequenceAndTime(Long trainId);
}
