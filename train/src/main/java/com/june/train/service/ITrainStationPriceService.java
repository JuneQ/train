package com.june.train.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.june.train.pojo.dao.TrainStationPrice;

/**
 * <p>
 * 列车站点价格表 服务类
 * </p>
 *
 * @author June
 */
public interface ITrainStationPriceService extends IService<TrainStationPrice> {

}
