package com.june.train.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.june.train.pojo.dao.TrainStationArrival;

/**
 * <p>
 * 列车到站表 服务类
 * </p>
 *
 * @author June
 */
public interface ITrainStationArrivalService extends IService<TrainStationArrival> {

}
