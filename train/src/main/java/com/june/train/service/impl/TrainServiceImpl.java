package com.june.train.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.june.common.constant.TicketConstant;
import com.june.common.dto.SubStationDto;
import com.june.common.exception.BusinessException;
import com.june.common.exception.ErrorCodeEnum;
import com.june.train.pojo.dao.Carriage;
import com.june.train.pojo.dao.Train;
import com.june.train.pojo.dao.TrainStationArrival;
import com.june.train.pojo.dao.mapper.CarriageMapper;
import com.june.train.pojo.dao.mapper.TrainMapper;
import com.june.train.pojo.dao.mapper.TrainStationArrivalMapper;
import com.june.train.pojo.vo.*;
import com.june.train.service.ITrainService;
import jakarta.annotation.Resource;
import lombok.val;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.connection.StringRedisConnection;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

/**
 * <p>
 * 列车信息表 服务实现类
 * </p>
 *
 * @author June
 */
@Service
public class TrainServiceImpl extends ServiceImpl<TrainMapper, Train> implements ITrainService {
    private static final int[] backoffArray = new int[]{5, 3, 2, 1};
    @Resource
    private TrainMapper trainMapper;
    @Resource
    private TrainStationArrivalMapper trainStationArrivalMapper;
    @Resource
    private CarriageMapper carriageMapper;
    @Resource
    private StringRedisConnection stringRedisConnection;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public List<Train> getTrain(SimpleSelectVo<Train> vo) {
        return trainMapper.selectTrainWithCondition(vo);
    }

    @Override
    @Transactional
    public void deleteTrain(Long id) {
        List<Carriage> carriages = carriageMapper.selectList(new LambdaQueryWrapper<>(Carriage.class)
                .eq(Carriage::getTrainId, id)
        );
        List<Long> cids = carriages.stream().map(Carriage::getId).toList();
        if (!cids.isEmpty()) carriageMapper.deleteBatchIds(cids);
        trainMapper.deleteById(id);
    }

    @Override
    public void updateTrain(TrainUpdateVo vo) {
        Train train = new Train();
        BeanUtils.copyProperties(vo, train);
        trainMapper.updateById(train);
    }

    @Override
    @Transactional
    public Long addTrain(TrainAddVo vo) {
        Train train = new Train();
        BeanUtils.copyProperties(vo, train);

        trainMapper.insert(train);

        TrainStationArrival tsa1 = new TrainStationArrival();
        tsa1.setTrainId(train.getId());
        tsa1.setArrivalTime(train.getDepartureTime());
        tsa1.setStationId(train.getStartStationId());
        tsa1.setStopoverTime((byte) 0);
        tsa1.setKthFromDepartureDay((byte) 1);
        tsa1.setSequence((byte) 1);

        TrainStationArrival tsa2 = new TrainStationArrival();
        tsa2.setTrainId(train.getId());
        tsa2.setArrivalTime(train.getArrivalTime());
        tsa2.setStationId(train.getEndStationId());
        tsa2.setStopoverTime((byte) 0);
        tsa2.setKthFromDepartureDay((byte) (1 + vo.dayCrossed()));
        tsa2.setSequence((byte) 2);

        trainStationArrivalMapper.insert(tsa1);
        trainStationArrivalMapper.insert(tsa2);

        return train.getId();
    }

    private boolean checkInsertDateValid(LocalTime departureTime, LocalTime arrivalTime, int dayCrossed, LocalTime insertTime, int kthDay) {
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = startDate.plusDays(dayCrossed);

        LocalDateTime departureDateTime = LocalDateTime.of(startDate, departureTime);
        LocalDateTime arrivalDateTime = LocalDateTime.of(endDate, arrivalTime);

        LocalDateTime intermediateDateTimeToCheck = LocalDateTime.of(startDate.plusDays(kthDay - 1), insertTime);

        // [)
        return !intermediateDateTimeToCheck.isBefore(departureDateTime) && intermediateDateTimeToCheck.isBefore(arrivalDateTime);
    }

    @Override
    public Long addTrainStationArrival(StationArrivalAddVo vo) {
        Train train = trainMapper.selectOne(new LambdaQueryWrapper<>(Train.class)
                .eq(Train::getId, vo.trainId())
        );

        if (!checkInsertDateValid(train.getDepartureTime(), train.getArrivalTime(), train.getDayCrossed(), vo.arrivalTime(), vo.kthFromDepartureDay()))
            throw new BusinessException(ErrorCodeEnum.INSERT_TIME_NOT_IN_NORMAL_INTERVAL);

        var arrival = new TrainStationArrival();
        BeanUtils.copyProperties(vo, arrival);
        trainStationArrivalMapper.insert(arrival);


        resortTrainStationArrivalSequence(vo.trainId());

        return arrival.getId();
    }

    @Override
    public void deleteTrainStationArrival(Long id) {
        TrainStationArrival arrival = trainStationArrivalMapper.selectById(id);

        trainStationArrivalMapper.deleteById(id);

        resortTrainStationArrivalSequence(arrival.getTrainId());
    }

    @Override
    public void updateTrainStationArrival(StationArrivalUpdateVo vo) {
        var arrival = new TrainStationArrival();
        BeanUtils.copyProperties(vo, arrival);
        trainStationArrivalMapper.updateById(arrival);

        resortTrainStationArrivalSequence(vo.trainId());
    }

    private void resortTrainStationArrivalSequence(Long trainId) {
        List<TrainStationArrival> list = trainStationArrivalMapper.selectList(new LambdaQueryWrapper<>(TrainStationArrival.class).eq(TrainStationArrival::getTrainId, trainId));
        list.sort((a, b) -> {
            if (a.getArrivalTime().isBefore(b.getArrivalTime())) return -1;
            if (a.getArrivalTime().isAfter(b.getArrivalTime())) return 1;
            return 0;
        });
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setSequence((byte) (i + 1));
            trainStationArrivalMapper.updateById(list.get(i));
        }
    }

    @Override
    public List<TrainStationArrival> getTrainStationArrival(Long id) {
        return trainStationArrivalMapper.selectTrainStationArrivalById(id);
    }

    @Override
    public Byte getStationArrivalSequence(Long stationId, Long trainId) {
        TrainStationArrival arrival = trainStationArrivalMapper.selectOne(new LambdaQueryWrapper<>(TrainStationArrival.class)
                .eq(TrainStationArrival::getStationId, stationId)
                .eq(TrainStationArrival::getTrainId, trainId)
                .select(TrainStationArrival::getSequence)
        );
        return arrival.getSequence();
    }

    @Override
    public LocalDateTime getTrainStationArrivalDateTime(Long stationId, Long trainId, String departureDate) {

        TrainStationArrival arrival = trainStationArrivalMapper.selectOne(
                Wrappers.<TrainStationArrival>lambdaQuery()
                        .eq(TrainStationArrival::getTrainId, trainId)
                        .eq(TrainStationArrival::getStationId, stationId)
        );

        return LocalDateTime.of(
                LocalDate.parse(departureDate).plus(Duration.ofDays(arrival.getKthFromDepartureDay() - 1)),
                arrival.getArrivalTime()
        );

    }

    @Override
    public List<TrainStationArrival> getTrainStationArrivalPage(Integer current, Integer size) {
        List<Train> trains = trainMapper.selectList(null);
        for (Train train : trains) {
            TrainStationArrival endTSA = trainStationArrivalMapper.selectOne(Wrappers.<TrainStationArrival>lambdaQuery()
                    .eq(TrainStationArrival::getStationId, train.getEndStationId())
            );
            List<Carriage> carriages = carriageMapper.selectList(
                    Wrappers.lambdaQuery(Carriage.class).eq(Carriage::getTrain, train.getId())
            );

            for (Carriage carriage : carriages) {
                //  格式：： ticket:tickets-bitmap:12312:123412:2312:12  (trainId:carriageTypeOrdinal:carriageId:seatNumber)
                for (int i = 1; i <= carriage.getSeatCount(); i++) {
                    stringRedisTemplate.opsForValue().setIfAbsent("", "");
                    stringRedisConnection.setBit(
                            String.format("%s%s:%s:%s:%s",
                                    TicketConstant.REDIS_REMAINING_TICKETS_BITMAP,
                                    train.getId(),
                                    carriage.getCarriageType().ordinal(),
                                    carriage.getId(),
                                    i
                            ),
                            (endTSA.getSequence() - 1L),
                            false
                    );
                }
            }


        }
        return trainStationArrivalMapper.selectPage(new Page<>(current, size), null).getRecords();
    }

    @Override
    public Byte getStationCrossed(Long trainId) {
        List<TrainStationArrival> list = trainStationArrivalMapper.selectList(Wrappers.lambdaQuery(TrainStationArrival.class)
                .eq(TrainStationArrival::getTrainId, trainId)
                .orderByAsc(TrainStationArrival::getSequence)
                .select(TrainStationArrival::getSequence)
        );

        return (byte) (list.get(list.size() - 1).getSequence() - list.get(0).getSequence() + 1);
    }

    @Override
    public List<SubStationDto> getPastAndNearestSubStationIdAndSequenceAndTime(Long trainId) {
        List<TrainStationArrival> list = trainStationArrivalMapper.selectList(
                Wrappers.lambdaQuery(TrainStationArrival.class)
                        .eq(TrainStationArrival::getTrainId, trainId)
                        .select(TrainStationArrival::getArrivalTime, TrainStationArrival::getStationId, TrainStationArrival::getKthFromDepartureDay)
                        .orderByAsc(TrainStationArrival::getSequence)
        );
        // 作为所有列车始发日期
        val initDate = "2024-01-01";

        // 计算offsetDays：最近的过去的发过的一次车 与 初始发车日期的偏移天数
        TrainStationArrival tsa = list.get(list.size() - 1);
        int offsetDays = 0;
        val cycle = tsa.getKthFromDepartureDay();
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime pastAndNearestDatetime = LocalDateTime.of(LocalDate.parse(initDate), tsa.getArrivalTime());
        for (int backoff : backoffArray) {
            LocalDateTime test = pastAndNearestDatetime.plusDays((long) backoff * cycle);
            while (now.isAfter(test)) {
                offsetDays += cycle * backoff;
                pastAndNearestDatetime = test;
                test = pastAndNearestDatetime.plusDays((long) backoff * cycle);
            }
        }

        int finalOffsetDays = offsetDays;
        return list.stream().map(t -> new SubStationDto(
                t.getStationId(),
                // 以此日期作为所有列车始发日期
                LocalDateTime.of(LocalDate.parse(initDate).plusDays(finalOffsetDays + t.getKthFromDepartureDay() - 1), t.getArrivalTime())
        )).toList();
    }
}
