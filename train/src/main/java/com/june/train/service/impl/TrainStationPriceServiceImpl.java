package com.june.train.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.june.train.pojo.dao.TrainStationPrice;
import com.june.train.pojo.dao.mapper.TrainStationPriceMapper;
import com.june.train.service.ITrainStationPriceService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 列车站点价格表 服务实现类
 * </p>
 *
 * @author June
 */
@Service
public class TrainStationPriceServiceImpl extends ServiceImpl<TrainStationPriceMapper, TrainStationPrice> implements ITrainStationPriceService {

}
