package com.june.train.service.impl;

import cn.hutool.extra.pinyin.PinyinUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.june.train.pojo.dao.Station;
import com.june.train.pojo.dao.mapper.StationMapper;
import com.june.train.pojo.vo.SimpleSelectVo;
import com.june.train.pojo.vo.StationAddVo;
import com.june.train.pojo.vo.StationUpdateVo;
import com.june.train.service.IStationService;
import jakarta.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 车站表 服务实现类
 * </p>
 *
 * @author June
 */
@Service
public class StationServiceImpl extends ServiceImpl<StationMapper, Station> implements IStationService {
    @Resource
    private StationMapper stationMapper;

    @Override
    public List<Station> getStation(SimpleSelectVo<Station> vo) {
        var wrapper = new LambdaQueryWrapper<Station>();
        Optional.ofNullable(vo.getId()).ifPresentOrElse(
                e -> stationMapper.selectPage(vo, wrapper.eq(Station::getId, e)),
                () -> vo.setRecords(stationMapper.selectPage(vo))
        );
        return vo.getRecords();
    }

    @Override
    public void updateStation(StationUpdateVo vo) {
        Station station = new Station();
        BeanUtils.copyProperties(vo, station);
        stationMapper.updateById(station);
    }

    @Override
    public void deleteStation(Long id) {
        stationMapper.deleteById(id);
    }

    @Override
    public Long addStation(StationAddVo vo) {
        Station station = new Station();
        BeanUtils.copyProperties(vo, station);
        station.setNamePinyin(PinyinUtil.getPinyin(vo.name(), ""));
        stationMapper.insert(station);
        return station.getId();
    }

    @Override
    public List<Station> getAllStation() {
        return stationMapper.selectList(null);
    }
}
