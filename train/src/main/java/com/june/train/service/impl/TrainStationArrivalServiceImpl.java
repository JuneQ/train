package com.june.train.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.june.train.pojo.dao.TrainStationArrival;
import com.june.train.pojo.dao.mapper.TrainStationArrivalMapper;
import com.june.train.service.ITrainStationArrivalService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 列车到站表 服务实现类
 * </p>
 *
 * @author June
 */
@Service
public class TrainStationArrivalServiceImpl extends ServiceImpl<TrainStationArrivalMapper, TrainStationArrival> implements ITrainStationArrivalService {

}
