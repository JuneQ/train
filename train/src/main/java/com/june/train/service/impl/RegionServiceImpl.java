package com.june.train.service.impl;

import cn.hutool.extra.pinyin.PinyinUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.june.train.pojo.dao.Region;
import com.june.train.pojo.dao.mapper.RegionMapper;
import com.june.train.pojo.vo.RegionAddVo;
import com.june.train.pojo.vo.RegionUpdateVo;
import com.june.train.pojo.vo.SimpleSelectVo;
import com.june.train.service.IRegionService;
import jakarta.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 地区表 服务实现类
 * </p>
 *
 * @author June
 */
@Service
public class RegionServiceImpl extends ServiceImpl<RegionMapper, Region> implements IRegionService {
    @Resource
    private RegionMapper regionMapper;

    @Override
    public List<Region> getRegionPage(SimpleSelectVo<Region> vo) {
        var wrapper = new LambdaQueryWrapper<>(Region.class);
        Optional.ofNullable(vo.getId())
                .ifPresentOrElse(
                        e -> wrapper.eq(Region::getId, e),
                        () -> {
                            Optional.ofNullable(vo.getKeyword()).ifPresent(n ->
                                    wrapper.like(Region::getName, n)
                            );
                        }
                );

        return regionMapper.selectPage(vo, wrapper).getRecords();
    }

    @Override
    public void deleteRegionById(Long id) {
        regionMapper.deleteById(id);
    }

    @Override
    public Long addRegion(RegionAddVo region) {
        Region dbRegion = new Region();
        BeanUtils.copyProperties(region, dbRegion);
        dbRegion.setInitial(PinyinUtil.getFirstLetter(dbRegion.getName().charAt(0)) + "");
        dbRegion.setPinyin(PinyinUtil.getPinyin(dbRegion.getName(), ""));
        dbRegion.setFullName(dbRegion.getName());

        regionMapper.insert(dbRegion);
        return dbRegion.getId();
    }

    @Override
    public void updateRegion(RegionUpdateVo vo) {
        Region dbRegion = new Region();
        BeanUtils.copyProperties(vo, dbRegion);
        regionMapper.updateById(dbRegion);
    }

    @Override
    public List<Region> getRegions() {
        return regionMapper.selectList(null);
    }

}
